using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;

namespace UserModule_CISCO_C90___C60_CALL_CONTROL_V2_5
{
    public class UserModuleClass_CISCO_C90___C60_CALL_CONTROL_V2_5 : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        Crestron.Logos.SplusObjects.BufferInput DEVICE_RX__DOLLAR__;
        Crestron.Logos.SplusObjects.StringOutput DEVICE_TX__DOLLAR__;
        Crestron.Logos.SplusObjects.AnalogInput ENTRIESPERPAGE;
        Crestron.Logos.SplusObjects.AnalogInput DTMFSENDCALLSLOT;
        Crestron.Logos.SplusObjects.StringInput DTMFSEND;
        Crestron.Logos.SplusObjects.DigitalInput GETLOCALPHONEBOOK;
        Crestron.Logos.SplusObjects.DigitalInput ENABLERECENTCALLS;
        Crestron.Logos.SplusObjects.DigitalInput GETRECENTCALLS;
        Crestron.Logos.SplusObjects.AnalogInput SELECTEDENTRYNUMBER;
        Crestron.Logos.SplusObjects.AnalogInput PAGESSELECTEDENTRYNUMBER;
        Crestron.Logos.SplusObjects.DigitalInput CLEARSELECTEDENTRYNUMBER;
        Crestron.Logos.SplusObjects.DigitalInput DIALSELECTEDPHONEBOOKENTRYNUMBER;
        Crestron.Logos.SplusObjects.AnalogInput PHONEBOOKSOURCEID;
        Crestron.Logos.SplusObjects.DigitalInput TOPLEVEL;
        Crestron.Logos.SplusObjects.DigitalInput UPONELEVEL;
        Crestron.Logos.SplusObjects.DigitalInput PAGESPHONEBOOKFIRSTPAGE;
        Crestron.Logos.SplusObjects.DigitalInput PAGESPHONEBOOKNEXTPAGE;
        Crestron.Logos.SplusObjects.DigitalInput PAGESPHONEBOOKPREVIOUSPAGE;
        Crestron.Logos.SplusObjects.DigitalInput SEARCHPHONEBOOK;
        Crestron.Logos.SplusObjects.StringInput PHONEBOOKSEARCHSTRING;
        Crestron.Logos.SplusObjects.DigitalInput CALLCONTROLACCEPTINCOMINGCALL;
        Crestron.Logos.SplusObjects.DigitalInput CALLCONTROLREJECTINCOMINGCALL;
        Crestron.Logos.SplusObjects.AnalogInput DISCONNECTCALLWITHID;
        Crestron.Logos.SplusObjects.DigitalInput DISCONNECTALLCALLS;
        Crestron.Logos.SplusObjects.StringInput DIALNUMBER;
        Crestron.Logos.SplusObjects.StringInput DIALCALLRATE;
        Crestron.Logos.SplusObjects.StringInput DIALCALLPROTOCOL;
        Crestron.Logos.SplusObjects.DigitalInput DIALCALL;
        Crestron.Logos.SplusObjects.AnalogInput FARENDCONTROLCALLSLOT;
        Crestron.Logos.SplusObjects.DigitalInput FARENDCONTROLCAMERAPANLEFT;
        Crestron.Logos.SplusObjects.DigitalInput FARENDCONTROLCAMERAPANRIGHT;
        Crestron.Logos.SplusObjects.DigitalInput FARENDCONTROLCAMERATILTUP;
        Crestron.Logos.SplusObjects.DigitalInput FARENDCONTROLCAMERATILTDOWN;
        Crestron.Logos.SplusObjects.DigitalInput FARENDCONTROLCAMERAZOOMIN;
        Crestron.Logos.SplusObjects.DigitalInput FARENDCONTROLCAMERAZOOMOUT;
        Crestron.Logos.SplusObjects.DigitalInput FARENDCONTROLCAMERAFOCUSIN;
        Crestron.Logos.SplusObjects.DigitalInput FARENDCONTROLCAMERAFOCUSOUT;
        Crestron.Logos.SplusObjects.DigitalInput FARENDCONTROLCAMERASTOP;
        Crestron.Logos.SplusObjects.AnalogInput FARENDCONTROLPRESENTATIONSOURCEID;
        Crestron.Logos.SplusObjects.AnalogInput FARENDCONTROLPRESETACTIVATEID;
        Crestron.Logos.SplusObjects.AnalogInput SETVOLUMELEVEL;
        Crestron.Logos.SplusObjects.DigitalInput RESYNCRONIZECALLSTATUSLIST;
        Crestron.Logos.SplusObjects.StringOutput TSTRING_RECEIVE_TEXT;
        Crestron.Logos.SplusObjects.StringInput TSTRING_SEND_TEXT;
        Crestron.Logos.SplusObjects.StringOutput SSTRING_RECEIVE_TEXT;
        Crestron.Logos.SplusObjects.StringInput SSTRING_SEND_TEXT;
        Crestron.Logos.SplusObjects.DigitalInput DATATOTRACE;
        Crestron.Logos.SplusObjects.AnalogInput SEARCHDELAYSTART;
        Crestron.Logos.SplusObjects.AnalogInput SEARCHDELAYMOD;
        Crestron.Logos.SplusObjects.AnalogInput SEARCHDELAY;
        Crestron.Logos.SplusObjects.DigitalInput DEFAULTCALLTYPESIP;
        Crestron.Logos.SplusObjects.StringOutput SELECTEDPHONEBOOK_NAME;
        Crestron.Logos.SplusObjects.StringOutput SELECTEDPHONEBOOK_NUMBER;
        Crestron.Logos.SplusObjects.StringOutput SELECTEDPHONEBOOK_CALLRATE;
        Crestron.Logos.SplusObjects.StringOutput PHONEBOOK_SEARCH_TEXT;
        Crestron.Logos.SplusObjects.AnalogOutput GETVOLUMELEVEL;
        Crestron.Logos.SplusObjects.StringOutput GETSYSTEMNAME;
        Crestron.Logos.SplusObjects.StringOutput GETSYSTEMNETWORKADDRESS;
        Crestron.Logos.SplusObjects.StringOutput GETGATEKEEPERSTATUS;
        Crestron.Logos.SplusObjects.StringOutput GETGATEKEEPERREGISTRATIONMODE;
        Crestron.Logos.SplusObjects.StringOutput GETGATEKEEPERNETWORKADDRESS;
        Crestron.Logos.SplusObjects.StringOutput GETSYSTEMH323ID;
        Crestron.Logos.SplusObjects.StringOutput GETSYSTEME164ALIAS;
        Crestron.Logos.SplusObjects.StringOutput GETSYSTEMSIPURI;
        Crestron.Logos.SplusObjects.StringOutput GETSYSTEMSOFTWAREVERSION;
        Crestron.Logos.SplusObjects.StringOutput FROM_DEVICE_CONFIGURATIONCHANGES;
        Crestron.Logos.SplusObjects.StringOutput FROM_DEVICE_STATUSCHANGES;
        Crestron.Logos.SplusObjects.StringOutput MESSAGE_ALERT_TEXT;
        Crestron.Logos.SplusObjects.DigitalOutput MESSAGE_ALERT_ISACTIVE;
        Crestron.Logos.SplusObjects.DigitalOutput MESSAGE_PROMPT_ISACTIVE;
        Crestron.Logos.SplusObjects.StringOutput MESSAGE_PROMPT_TITLE;
        Crestron.Logos.SplusObjects.StringOutput MESSAGE_PROMPT_TEXT;
        Crestron.Logos.SplusObjects.StringOutput MESSAGE_PROMPT_OPTION1_TEXT;
        Crestron.Logos.SplusObjects.StringOutput MESSAGE_PROMPT_OPTION2_TEXT;
        Crestron.Logos.SplusObjects.StringOutput MESSAGE_PROMPT_OPTION3_TEXT;
        Crestron.Logos.SplusObjects.StringOutput MESSAGE_PROMPT_OPTION4_TEXT;
        Crestron.Logos.SplusObjects.StringOutput MESSAGE_PROMPT_OPTION5_TEXT;
        Crestron.Logos.SplusObjects.DigitalOutput MESSAGE_PROMPT_OPTION1_ISACTIVE;
        Crestron.Logos.SplusObjects.DigitalOutput MESSAGE_PROMPT_OPTION2_ISACTIVE;
        Crestron.Logos.SplusObjects.DigitalOutput MESSAGE_PROMPT_OPTION3_ISACTIVE;
        Crestron.Logos.SplusObjects.DigitalOutput MESSAGE_PROMPT_OPTION4_ISACTIVE;
        Crestron.Logos.SplusObjects.DigitalOutput MESSAGE_PROMPT_OPTION5_ISACTIVE;
        Crestron.Logos.SplusObjects.DigitalOutput MESSAGE_PROMPT_OPTION1_WASSELECTED;
        Crestron.Logos.SplusObjects.DigitalOutput MESSAGE_PROMPT_OPTION2_WASSELECTED;
        Crestron.Logos.SplusObjects.DigitalOutput MESSAGE_PROMPT_OPTION3_WASSELECTED;
        Crestron.Logos.SplusObjects.DigitalOutput MESSAGE_PROMPT_OPTION4_WASSELECTED;
        Crestron.Logos.SplusObjects.DigitalOutput MESSAGE_PROMPT_OPTION5_WASSELECTED;
        Crestron.Logos.SplusObjects.DigitalOutput PHONEBOOK_SEARCHRESULT_IS_FROM_SUBFOLDER;
        Crestron.Logos.SplusObjects.AnalogOutput CURRENTFIRSTPHONEBOOKITEMNUMBER;
        Crestron.Logos.SplusObjects.AnalogOutput CURRENTLASTPHONEBOOKITEMNUMBER;
        Crestron.Logos.SplusObjects.StringOutput PHONEBOOK_SELECTED_FOLDER_NAME;
        Crestron.Logos.SplusObjects.DigitalOutput DIRECTORYDOWNLOADBUSY;
        Crestron.Logos.SplusObjects.DigitalOutput RECENTCALLSDOWNLOADBUSY;
        Crestron.Logos.SplusObjects.AnalogOutput TOTALROWSFORSEARCH;
        Crestron.Logos.SplusObjects.DigitalOutput SELECTEDISFOLDER;
        Crestron.Logos.SplusObjects.DigitalOutput SELECTEDISCONTACT;
        Crestron.Logos.SplusObjects.StringOutput CALLHISTORYNAME;
        Crestron.Logos.SplusObjects.StringOutput CALLHISTORYNUMBER;
        Crestron.Logos.SplusObjects.DigitalOutput SEARCH_IS_ACTIVE;
        Crestron.Logos.SplusObjects.DigitalOutput GETREBOOTCODEC_IS_IN_PROGRESS;
        Crestron.Logos.SplusObjects.DigitalOutput SIGNALINCOMINGCALL;
        Crestron.Logos.SplusObjects.DigitalOutput DEVICE_ONLINE;
        Crestron.Logos.SplusObjects.StringOutput INCOMINGCALLNUMBER;
        Crestron.Logos.SplusObjects.AnalogOutput NUMBEROFACTIVECALLS;
        Crestron.Logos.SplusObjects.AnalogOutput PHONEBOOKPAGESCURRENTPAGE;
        Crestron.Logos.SplusObjects.AnalogOutput PHONEBOOKPAGESTOTALPAGES;
        Crestron.Logos.SplusObjects.DigitalOutput DIRECTORYONTOPLEVEL;
        InOutArray<Crestron.Logos.SplusObjects.DigitalOutput> CALLSTATUS_ISCONNECTED;
        InOutArray<Crestron.Logos.SplusObjects.DigitalOutput> CALLSTATUS_SUPPORTSPRESENTATION;
        InOutArray<Crestron.Logos.SplusObjects.DigitalOutput> CALLSTATUS_ISACTIVE;
        InOutArray<Crestron.Logos.SplusObjects.StringOutput> INPUT_SOURCE_NAME;
        InOutArray<Crestron.Logos.SplusObjects.StringOutput> PHONEBOOK_NAME;
        InOutArray<Crestron.Logos.SplusObjects.StringOutput> PHONEBOOK_PAGES_NAME;
        InOutArray<Crestron.Logos.SplusObjects.StringOutput> CALLSTATUS_STATUS;
        InOutArray<Crestron.Logos.SplusObjects.StringOutput> CALLSTATUS_CALLTYPE;
        InOutArray<Crestron.Logos.SplusObjects.StringOutput> CALLSTATUS_REMOTESITE;
        InOutArray<Crestron.Logos.SplusObjects.StringOutput> CALLSTATUS_DURATION;
        InOutArray<Crestron.Logos.SplusObjects.StringOutput> CALLSTATUS_TRANSMITCALLRATE;
        InOutArray<Crestron.Logos.SplusObjects.StringOutput> CALLSTATUS_RECEIVECALLRATE;
        InOutArray<Crestron.Logos.SplusObjects.StringOutput> CALLSTATUS_DIRECTION;
        InOutArray<Crestron.Logos.SplusObjects.AnalogOutput> CALLSTATUS_CALLID;
        ushort SEMAPHORE = 0;
        ushort DIALSEMAPHORE = 0;
        ushort IPROCESS = 0;
        CrestronString TEMPSTRING__DOLLAR__;
        ushort CALLID = 0;
        CrestronString CALLRESPONSETYPE;
        CrestronString CALLSTATUS;
        ushort TMP = 0;
        CrestronString REMOTENUMBER;
        ushort CALLSLOT = 0;
        CrestronString GTEMPDISPLAYNAME;
        CrestronString GTEMPVALUE;
        CrestronString GTEMPSEGMENT;
        ushort INPUTSOURCEITEM = 0;
        ushort BGETRECENTSONLY = 0;
        ushort CONTACTITEM = 0;
        ushort ENTITYID = 0;
        CrestronString PHONEBOOKENTRYTYPE;
        CrestronString PHONEBOOKTYPE;
        ushort PREVIOUSFOUNDCONTACTITEM = 0;
        ushort NEXTAVAILABLEDIRENTRY = 0;
        ushort NEXTAVAILABLECONTACTMETHOD = 0;
        ushort INTERNALENTRYOFFSET = 0;
        ushort INTERNALPAGESENTRYOFFSET = 0;
        CrestronString MESSAGE_PROMPT_FEEDBACKID;
        CrestronString SSEARCHTEXT;
        ushort NEXTFOLDERENTRY = 0;
        ushort PHONEBOOKPARSEFAILURE = 0;
        ushort RECEIVINGPHONEBOOK = 0;
        ushort PHONEBOOKLEVELTOTALROWS = 0;
        ushort PREVIOUSFOUNDCONTACTMETHOD = 0;
        ushort CONTACTMETHOD = 0;
        ushort SELECTEDFOLDER = 0;
        ushort [] SEARCHRESULT;
        ushort SEARCHACTIVE = 0;
        ushort SELECTEDENTRY = 0;
        ushort SELECTEDENTRYMETHOD = 0;
        ushort SEARCHINDEX = 0;
        ushort GLOBALSEARCHCOUNT = 0;
        ushort ICURRENTPAGE = 0;
        ushort ITOTALPAGES = 0;
        ushort IENTRIESPERPAGE = 0;
        PHONEBOOK [] PHONEBOOKENTRY;
        CONTACTLIST [] CONTACT;
        private void PRINTSAVEDDATA (  SplusExecutionContext __context__ ) 
            { 
            ushort I = 0;
            
            
            __context__.SourceCodeLine = 274;
            /* Trace( "Contacts") */ ; 
            __context__.SourceCodeLine = 275;
            ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)200; 
            int __FN_FORSTEP_VAL__1 = (int)1; 
            for ( I  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (I  >= __FN_FORSTART_VAL__1) && (I  <= __FN_FOREND_VAL__1) ) : ( (I  <= __FN_FORSTART_VAL__1) && (I  >= __FN_FOREND_VAL__1) ) ; I  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                __context__.SourceCodeLine = 277;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( CONTACT[ I ].CONTACTNUMBER ) > 0 ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 279;
                    /* Trace( "i = {0:d}; {1} \t {2:d}", (short)I, CONTACT [ I] . CONTACTNUMBER , (short)CONTACT[ I ].CALLTYPE) */ ; 
                    } 
                
                else 
                    { 
                    __context__.SourceCodeLine = 283;
                    break ; 
                    } 
                
                __context__.SourceCodeLine = 275;
                } 
            
            __context__.SourceCodeLine = 285;
            /* Trace( "Phonebook Entries") */ ; 
            __context__.SourceCodeLine = 286;
            ushort __FN_FORSTART_VAL__2 = (ushort) ( 0 ) ;
            ushort __FN_FOREND_VAL__2 = (ushort)170; 
            int __FN_FORSTEP_VAL__2 = (int)1; 
            for ( I  = __FN_FORSTART_VAL__2; (__FN_FORSTEP_VAL__2 > 0)  ? ( (I  >= __FN_FORSTART_VAL__2) && (I  <= __FN_FOREND_VAL__2) ) : ( (I  <= __FN_FORSTART_VAL__2) && (I  >= __FN_FOREND_VAL__2) ) ; I  += (ushort)__FN_FORSTEP_VAL__2) 
                { 
                __context__.SourceCodeLine = 288;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( Functions.Length( PHONEBOOKENTRY[ I ].ENTRYNAME ) > 0 ) ) || Functions.TestForTrue ( Functions.BoolToInt (I == 0) )) ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 290;
                    /* Trace( "i = {0:d}; {1} \t {2:d} \t {3:d}", (short)I, PHONEBOOKENTRY [ I] . ENTRYNAME , (short)PHONEBOOKENTRY[ I ].FIRSTCHILD, (short)PHONEBOOKENTRY[ I ].CHILDCOUNT) */ ; 
                    } 
                
                else 
                    { 
                    __context__.SourceCodeLine = 294;
                    break ; 
                    } 
                
                __context__.SourceCodeLine = 286;
                } 
            
            
            }
            
        private void FDISPLAYNEWPAGE (  SplusExecutionContext __context__ ) 
            { 
            ushort I = 0;
            ushort A = 0;
            ushort B = 0;
            ushort FOLDERTEMP = 0;
            
            
            __context__.SourceCodeLine = 302;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (RECENTCALLSDOWNLOADBUSY  .Value == 0))  ) ) 
                { 
                __context__.SourceCodeLine = 304;
                FOLDERTEMP = (ushort) ( SELECTEDFOLDER ) ; 
                __context__.SourceCodeLine = 305;
                ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
                ushort __FN_FOREND_VAL__1 = (ushort)IENTRIESPERPAGE; 
                int __FN_FORSTEP_VAL__1 = (int)1; 
                for ( I  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (I  >= __FN_FORSTART_VAL__1) && (I  <= __FN_FOREND_VAL__1) ) : ( (I  <= __FN_FORSTART_VAL__1) && (I  >= __FN_FOREND_VAL__1) ) ; I  += (ushort)__FN_FORSTEP_VAL__1) 
                    { 
                    __context__.SourceCodeLine = 307;
                    PHONEBOOK_PAGES_NAME [ I]  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 305;
                    } 
                
                __context__.SourceCodeLine = 310;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (SEARCHACTIVE == 1))  ) ) 
                    { 
                    __context__.SourceCodeLine = 312;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (ICURRENTPAGE == 0))  ) ) 
                        { 
                        __context__.SourceCodeLine = 314;
                        ICURRENTPAGE = (ushort) ( 1 ) ; 
                        __context__.SourceCodeLine = 315;
                        PHONEBOOKPAGESCURRENTPAGE  .Value = (ushort) ( ICURRENTPAGE ) ; 
                        } 
                    
                    __context__.SourceCodeLine = 317;
                    ushort __FN_FORSTART_VAL__2 = (ushort) ( 1 ) ;
                    ushort __FN_FOREND_VAL__2 = (ushort)IENTRIESPERPAGE; 
                    int __FN_FORSTEP_VAL__2 = (int)1; 
                    for ( A  = __FN_FORSTART_VAL__2; (__FN_FORSTEP_VAL__2 > 0)  ? ( (A  >= __FN_FORSTART_VAL__2) && (A  <= __FN_FOREND_VAL__2) ) : ( (A  <= __FN_FORSTART_VAL__2) && (A  >= __FN_FOREND_VAL__2) ) ; A  += (ushort)__FN_FORSTEP_VAL__2) 
                        { 
                        __context__.SourceCodeLine = 319;
                        B = (ushort) ( (A + ((ICURRENTPAGE - 1) * IENTRIESPERPAGE)) ) ; 
                        __context__.SourceCodeLine = 320;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( B < SEARCHINDEX ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 322;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( PHONEBOOKENTRY[ SEARCHRESULT[ B ] ].FOLDERID ) > 0 ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 324;
                                PHONEBOOK_PAGES_NAME [ A]  .UpdateValue ( "[+]  " + PHONEBOOKENTRY [ SEARCHRESULT[ B ]] . ENTRYNAME  ) ; 
                                } 
                            
                            else 
                                { 
                                __context__.SourceCodeLine = 328;
                                PHONEBOOK_PAGES_NAME [ A]  .UpdateValue ( PHONEBOOKENTRY [ SEARCHRESULT[ B ]] . ENTRYNAME  ) ; 
                                } 
                            
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 331;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (B == SEARCHINDEX) ) && Functions.TestForTrue ( Functions.BoolToInt (SEARCHINDEX == 50) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt (A == IENTRIESPERPAGE) )) ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 333;
                                PHONEBOOK_PAGES_NAME [ A]  .UpdateValue ( "More Entries Available. Refine Search."  ) ; 
                                __context__.SourceCodeLine = 334;
                                break ; 
                                } 
                            
                            else 
                                {
                                __context__.SourceCodeLine = 336;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (B == SEARCHINDEX) ) && Functions.TestForTrue ( Functions.BoolToInt (SEARCHINDEX == 50) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( A < IENTRIESPERPAGE ) )) ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 338;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( PHONEBOOKENTRY[ SEARCHRESULT[ B ] ].FOLDERID ) > 0 ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 340;
                                        PHONEBOOK_PAGES_NAME [ A]  .UpdateValue ( "[+]  " + PHONEBOOKENTRY [ SEARCHRESULT[ B ]] . ENTRYNAME  ) ; 
                                        } 
                                    
                                    else 
                                        { 
                                        __context__.SourceCodeLine = 344;
                                        PHONEBOOK_PAGES_NAME [ A]  .UpdateValue ( PHONEBOOKENTRY [ SEARCHRESULT[ B ]] . ENTRYNAME  ) ; 
                                        } 
                                    
                                    __context__.SourceCodeLine = 346;
                                    PHONEBOOK_PAGES_NAME [ (A + 1)]  .UpdateValue ( "More Entries Available. Refine Search."  ) ; 
                                    __context__.SourceCodeLine = 347;
                                    break ; 
                                    } 
                                
                                else 
                                    { 
                                    __context__.SourceCodeLine = 351;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( PHONEBOOKENTRY[ SEARCHRESULT[ B ] ].FOLDERID ) > 0 ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 353;
                                        PHONEBOOK_PAGES_NAME [ A]  .UpdateValue ( "[+]  " + PHONEBOOKENTRY [ SEARCHRESULT[ B ]] . ENTRYNAME  ) ; 
                                        } 
                                    
                                    else 
                                        { 
                                        __context__.SourceCodeLine = 357;
                                        PHONEBOOK_PAGES_NAME [ A]  .UpdateValue ( PHONEBOOKENTRY [ SEARCHRESULT[ B ]] . ENTRYNAME  ) ; 
                                        } 
                                    
                                    } 
                                
                                }
                            
                            }
                        
                        __context__.SourceCodeLine = 317;
                        } 
                    
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 362;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (PHONEBOOKENTRY[ FOLDERTEMP ].FIRSTCHILD == (2000 + 1)) ) || Functions.TestForTrue ( Functions.BoolToInt (NEXTAVAILABLEDIRENTRY == 0) )) ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 364;
                        ICURRENTPAGE = (ushort) ( 1 ) ; 
                        __context__.SourceCodeLine = 365;
                        ITOTALPAGES = (ushort) ( 1 ) ; 
                        __context__.SourceCodeLine = 366;
                        PHONEBOOKPAGESCURRENTPAGE  .Value = (ushort) ( ICURRENTPAGE ) ; 
                        __context__.SourceCodeLine = 367;
                        PHONEBOOKPAGESTOTALPAGES  .Value = (ushort) ( ITOTALPAGES ) ; 
                        __context__.SourceCodeLine = 368;
                        PHONEBOOK_PAGES_NAME [ 1]  .UpdateValue ( "No Contacts"  ) ; 
                        } 
                    
                    else 
                        { 
                        __context__.SourceCodeLine = 372;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (ICURRENTPAGE == 0))  ) ) 
                            { 
                            __context__.SourceCodeLine = 374;
                            ICURRENTPAGE = (ushort) ( 1 ) ; 
                            __context__.SourceCodeLine = 375;
                            PHONEBOOKPAGESCURRENTPAGE  .Value = (ushort) ( ICURRENTPAGE ) ; 
                            } 
                        
                        __context__.SourceCodeLine = 377;
                        ushort __FN_FORSTART_VAL__3 = (ushort) ( 1 ) ;
                        ushort __FN_FOREND_VAL__3 = (ushort)IENTRIESPERPAGE; 
                        int __FN_FORSTEP_VAL__3 = (int)1; 
                        for ( A  = __FN_FORSTART_VAL__3; (__FN_FORSTEP_VAL__3 > 0)  ? ( (A  >= __FN_FORSTART_VAL__3) && (A  <= __FN_FOREND_VAL__3) ) : ( (A  <= __FN_FORSTART_VAL__3) && (A  >= __FN_FOREND_VAL__3) ) ; A  += (ushort)__FN_FORSTEP_VAL__3) 
                            { 
                            __context__.SourceCodeLine = 379;
                            B = (ushort) ( (A + ((ICURRENTPAGE - 1) * IENTRIESPERPAGE)) ) ; 
                            __context__.SourceCodeLine = 380;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( B <= PHONEBOOKENTRY[ FOLDERTEMP ].CHILDCOUNT ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 382;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( PHONEBOOKENTRY[ ((PHONEBOOKENTRY[ FOLDERTEMP ].FIRSTCHILD + B) - 1) ].FOLDERID ) > 0 ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 384;
                                    PHONEBOOK_PAGES_NAME [ A]  .UpdateValue ( "[+]  " + PHONEBOOKENTRY [ ((PHONEBOOKENTRY[ FOLDERTEMP ].FIRSTCHILD + B) - 1)] . ENTRYNAME  ) ; 
                                    } 
                                
                                else 
                                    { 
                                    __context__.SourceCodeLine = 388;
                                    PHONEBOOK_PAGES_NAME [ A]  .UpdateValue ( PHONEBOOKENTRY [ ((PHONEBOOKENTRY[ FOLDERTEMP ].FIRSTCHILD + B) - 1)] . ENTRYNAME  ) ; 
                                    } 
                                
                                } 
                            
                            __context__.SourceCodeLine = 377;
                            } 
                        
                        } 
                    
                    }
                
                } 
            
            
            }
            
        private void CONTACTSEARCH (  SplusExecutionContext __context__, ushort INDEXCONTACT , ushort SEARCHCOUNT ) 
            { 
            ushort I = 0;
            
            
            __context__.SourceCodeLine = 399;
            ushort __FN_FORSTART_VAL__1 = (ushort) ( PHONEBOOKENTRY[ INDEXCONTACT ].FIRSTCHILD ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)((PHONEBOOKENTRY[ INDEXCONTACT ].FIRSTCHILD + PHONEBOOKENTRY[ INDEXCONTACT ].CHILDCOUNT) - 1); 
            int __FN_FORSTEP_VAL__1 = (int)1; 
            for ( I  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (I  >= __FN_FORSTART_VAL__1) && (I  <= __FN_FOREND_VAL__1) ) : ( (I  <= __FN_FORSTART_VAL__1) && (I  >= __FN_FOREND_VAL__1) ) ; I  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                __context__.SourceCodeLine = 401;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (SEARCHCOUNT != GLOBALSEARCHCOUNT) ) || Functions.TestForTrue ( Functions.BoolToInt ( SEARCHINDEX >= 50 ) )) ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 403;
                    return ; 
                    } 
                
                __context__.SourceCodeLine = 405;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( PHONEBOOKENTRY[ I ].FOLDERID ) > 0 ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 407;
                    CONTACTSEARCH (  __context__ , (ushort)( I ), (ushort)( SEARCHCOUNT )) ; 
                    } 
                
                else 
                    { 
                    __context__.SourceCodeLine = 411;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Find( Functions.Upper( PHONEBOOKSEARCHSTRING ) , Functions.Upper( PHONEBOOKENTRY[ I ].ENTRYNAME ) ) > 0 ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 413;
                        SEARCHINDEX = (ushort) ( (SEARCHINDEX + 1) ) ; 
                        __context__.SourceCodeLine = 414;
                        SEARCHRESULT [ SEARCHINDEX] = (ushort) ( I ) ; 
                        } 
                    
                    } 
                
                __context__.SourceCodeLine = 399;
                } 
            
            
            }
            
        private void FOLDERSEARCH (  SplusExecutionContext __context__, ushort INDEXFOLDER , ushort SEARCHCOUNT ) 
            { 
            ushort I = 0;
            
            
            __context__.SourceCodeLine = 423;
            ushort __FN_FORSTART_VAL__1 = (ushort) ( PHONEBOOKENTRY[ INDEXFOLDER ].FIRSTCHILD ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)((PHONEBOOKENTRY[ INDEXFOLDER ].FIRSTCHILD + PHONEBOOKENTRY[ INDEXFOLDER ].CHILDCOUNT) - 1); 
            int __FN_FORSTEP_VAL__1 = (int)1; 
            for ( I  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (I  >= __FN_FORSTART_VAL__1) && (I  <= __FN_FOREND_VAL__1) ) : ( (I  <= __FN_FORSTART_VAL__1) && (I  >= __FN_FOREND_VAL__1) ) ; I  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                __context__.SourceCodeLine = 425;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (SEARCHCOUNT != GLOBALSEARCHCOUNT) ) || Functions.TestForTrue ( Functions.BoolToInt ( SEARCHINDEX >= 50 ) )) ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 427;
                    return ; 
                    } 
                
                __context__.SourceCodeLine = 429;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( PHONEBOOKENTRY[ I ].FOLDERID ) > 0 ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 431;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Find( Functions.Upper( PHONEBOOKSEARCHSTRING ) , Functions.Upper( PHONEBOOKENTRY[ I ].ENTRYNAME ) ) > 0 ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 433;
                        SEARCHINDEX = (ushort) ( (SEARCHINDEX + 1) ) ; 
                        __context__.SourceCodeLine = 434;
                        SEARCHRESULT [ SEARCHINDEX] = (ushort) ( I ) ; 
                        } 
                    
                    __context__.SourceCodeLine = 436;
                    FOLDERSEARCH (  __context__ , (ushort)( I ), (ushort)( SEARCHCOUNT )) ; 
                    } 
                
                __context__.SourceCodeLine = 423;
                } 
            
            __context__.SourceCodeLine = 439;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (INDEXFOLDER == SELECTEDFOLDER) ) && Functions.TestForTrue ( Functions.BoolToInt ( SEARCHINDEX < 50 ) )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 441;
                CONTACTSEARCH (  __context__ , (ushort)( INDEXFOLDER ), (ushort)( SEARCHCOUNT )) ; 
                } 
            
            
            }
            
        private void PHONEBOOKSEARCH (  SplusExecutionContext __context__, ushort SEARCHCOUNT ) 
            { 
            ushort INDEXRESET = 0;
            ushort A = 0;
            
            ushort INDEXLIST = 0;
            
            
            __context__.SourceCodeLine = 450;
            ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)50; 
            int __FN_FORSTEP_VAL__1 = (int)1; 
            for ( INDEXRESET  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (INDEXRESET  >= __FN_FORSTART_VAL__1) && (INDEXRESET  <= __FN_FOREND_VAL__1) ) : ( (INDEXRESET  <= __FN_FORSTART_VAL__1) && (INDEXRESET  >= __FN_FOREND_VAL__1) ) ; INDEXRESET  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                __context__.SourceCodeLine = 452;
                SEARCHRESULT [ INDEXRESET] = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 453;
                SEARCHRESULT [ INDEXRESET] = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 450;
                } 
            
            __context__.SourceCodeLine = 455;
            SEARCHINDEX = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 457;
            FOLDERSEARCH (  __context__ , (ushort)( SELECTEDFOLDER ), (ushort)( SEARCHCOUNT )) ; 
            __context__.SourceCodeLine = 459;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (SEARCHCOUNT == GLOBALSEARCHCOUNT))  ) ) 
                { 
                __context__.SourceCodeLine = 461;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (SEARCHINDEX == 0))  ) ) 
                    { 
                    __context__.SourceCodeLine = 463;
                    TOTALROWSFORSEARCH  .Value = (ushort) ( 1 ) ; 
                    __context__.SourceCodeLine = 464;
                    PHONEBOOK_NAME [ 1]  .UpdateValue ( "No Results"  ) ; 
                    } 
                
                else 
                    { 
                    __context__.SourceCodeLine = 468;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (SEARCHINDEX == 50))  ) ) 
                        { 
                        __context__.SourceCodeLine = 470;
                        TOTALROWSFORSEARCH  .Value = (ushort) ( (50 + 1) ) ; 
                        __context__.SourceCodeLine = 471;
                        PHONEBOOK_NAME [ (50 + 1)]  .UpdateValue ( "More Entries Available. Refine Search."  ) ; 
                        } 
                    
                    else 
                        { 
                        __context__.SourceCodeLine = 475;
                        TOTALROWSFORSEARCH  .Value = (ushort) ( SEARCHINDEX ) ; 
                        } 
                    
                    __context__.SourceCodeLine = 477;
                    INTERNALPAGESENTRYOFFSET = (ushort) ( 0 ) ; 
                    __context__.SourceCodeLine = 478;
                    ushort __FN_FORSTART_VAL__2 = (ushort) ( 1 ) ;
                    ushort __FN_FOREND_VAL__2 = (ushort)SEARCHINDEX; 
                    int __FN_FORSTEP_VAL__2 = (int)1; 
                    for ( INDEXLIST  = __FN_FORSTART_VAL__2; (__FN_FORSTEP_VAL__2 > 0)  ? ( (INDEXLIST  >= __FN_FORSTART_VAL__2) && (INDEXLIST  <= __FN_FOREND_VAL__2) ) : ( (INDEXLIST  <= __FN_FORSTART_VAL__2) && (INDEXLIST  >= __FN_FOREND_VAL__2) ) ; INDEXLIST  += (ushort)__FN_FORSTEP_VAL__2) 
                        { 
                        __context__.SourceCodeLine = 480;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (SEARCHCOUNT != GLOBALSEARCHCOUNT))  ) ) 
                            { 
                            __context__.SourceCodeLine = 482;
                            break ; 
                            } 
                        
                        __context__.SourceCodeLine = 484;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( PHONEBOOKENTRY[ SEARCHRESULT[ INDEXLIST ] ].FOLDERID ) > 0 ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 486;
                            PHONEBOOK_NAME [ INDEXLIST]  .UpdateValue ( "[+]  " + PHONEBOOKENTRY [ SEARCHRESULT[ INDEXLIST ]] . ENTRYNAME  ) ; 
                            } 
                        
                        else 
                            { 
                            __context__.SourceCodeLine = 490;
                            PHONEBOOK_NAME [ INDEXLIST]  .UpdateValue ( PHONEBOOKENTRY [ SEARCHRESULT[ INDEXLIST ]] . ENTRYNAME  ) ; 
                            } 
                        
                        __context__.SourceCodeLine = 478;
                        } 
                    
                    __context__.SourceCodeLine = 493;
                    ICURRENTPAGE = (ushort) ( 1 ) ; 
                    __context__.SourceCodeLine = 494;
                    PHONEBOOKPAGESCURRENTPAGE  .Value = (ushort) ( ICURRENTPAGE ) ; 
                    __context__.SourceCodeLine = 495;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Mod( SEARCHINDEX , IENTRIESPERPAGE ) > 0 ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 497;
                        ITOTALPAGES = (ushort) ( ((SEARCHINDEX / IENTRIESPERPAGE) + 1) ) ; 
                        } 
                    
                    else 
                        { 
                        __context__.SourceCodeLine = 501;
                        ITOTALPAGES = (ushort) ( (SEARCHINDEX / IENTRIESPERPAGE) ) ; 
                        } 
                    
                    __context__.SourceCodeLine = 503;
                    PHONEBOOKPAGESTOTALPAGES  .Value = (ushort) ( ITOTALPAGES ) ; 
                    __context__.SourceCodeLine = 504;
                    FDISPLAYNEWPAGE (  __context__  ) ; 
                    } 
                
                } 
            
            
            }
            
        private CrestronString SEARCHREPLACE (  SplusExecutionContext __context__, CrestronString DATA , CrestronString SEARCH , CrestronString REPLACE ) 
            { 
            ushort POS = 0;
            
            
            __context__.SourceCodeLine = 512;
            POS = (ushort) ( Functions.Find( SEARCH , DATA ) ) ; 
            __context__.SourceCodeLine = 513;
            while ( Functions.TestForTrue  ( ( Functions.BoolToInt ( POS > 0 ))  ) ) 
                { 
                __context__.SourceCodeLine = 515;
                SetString ( REPLACE , (int)POS, DATA ) ; 
                __context__.SourceCodeLine = 516;
                DATA  .UpdateValue ( Functions.Mid ( DATA ,  (int) ( 1 ) ,  (int) ( POS ) ) + Functions.Mid ( DATA ,  (int) ( (POS + 2) ) ,  (int) ( Functions.Length( DATA ) ) )  ) ; 
                __context__.SourceCodeLine = 517;
                POS = (ushort) ( Functions.Find( SEARCH , DATA ) ) ; 
                __context__.SourceCodeLine = 513;
                } 
            
            __context__.SourceCodeLine = 519;
            if ( Functions.TestForTrue  ( ( 0)  ) ) 
                {
                __context__.SourceCodeLine = 519;
                Print( "searchReplace\r\n") ; 
                }
            
            __context__.SourceCodeLine = 520;
            return ( DATA ) ; 
            
            }
            
        private CrestronString UTF8TOISO8859 (  SplusExecutionContext __context__, CrestronString DATA ) 
            { 
            
            __context__.SourceCodeLine = 525;
            DATA  .UpdateValue ( SEARCHREPLACE (  __context__ , DATA, "\u00C3\u00A6", "�")  ) ; 
            __context__.SourceCodeLine = 526;
            DATA  .UpdateValue ( SEARCHREPLACE (  __context__ , DATA, "\u00C3\u00B8", "�")  ) ; 
            __context__.SourceCodeLine = 527;
            DATA  .UpdateValue ( SEARCHREPLACE (  __context__ , DATA, "\u00C3\u00A5", "�")  ) ; 
            __context__.SourceCodeLine = 528;
            DATA  .UpdateValue ( SEARCHREPLACE (  __context__ , DATA, "\u00C3\u0086", "�")  ) ; 
            __context__.SourceCodeLine = 529;
            DATA  .UpdateValue ( SEARCHREPLACE (  __context__ , DATA, "\u00C3\u0098", "�")  ) ; 
            __context__.SourceCodeLine = 530;
            DATA  .UpdateValue ( SEARCHREPLACE (  __context__ , DATA, "\u00C3\u0085", "�")  ) ; 
            __context__.SourceCodeLine = 531;
            DATA  .UpdateValue ( SEARCHREPLACE (  __context__ , DATA, "\u00C3\u00A9", "�")  ) ; 
            __context__.SourceCodeLine = 532;
            return ( DATA ) ; 
            
            }
            
        private void SETSELECTEDMESSAGEPROMPTRESPONSE (  SplusExecutionContext __context__, ushort OPTIONID ) 
            { 
            
            __context__.SourceCodeLine = 537;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (OPTIONID == 1))  ) ) 
                {
                __context__.SourceCodeLine = 537;
                MESSAGE_PROMPT_OPTION1_WASSELECTED  .Value = (ushort) ( 1 ) ; 
                }
            
            else 
                {
                __context__.SourceCodeLine = 537;
                MESSAGE_PROMPT_OPTION1_WASSELECTED  .Value = (ushort) ( 0 ) ; 
                }
            
            __context__.SourceCodeLine = 538;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (OPTIONID == 2))  ) ) 
                {
                __context__.SourceCodeLine = 538;
                MESSAGE_PROMPT_OPTION2_WASSELECTED  .Value = (ushort) ( 1 ) ; 
                }
            
            else 
                {
                __context__.SourceCodeLine = 538;
                MESSAGE_PROMPT_OPTION2_WASSELECTED  .Value = (ushort) ( 0 ) ; 
                }
            
            __context__.SourceCodeLine = 539;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (OPTIONID == 3))  ) ) 
                {
                __context__.SourceCodeLine = 539;
                MESSAGE_PROMPT_OPTION3_WASSELECTED  .Value = (ushort) ( 1 ) ; 
                }
            
            else 
                {
                __context__.SourceCodeLine = 539;
                MESSAGE_PROMPT_OPTION3_WASSELECTED  .Value = (ushort) ( 0 ) ; 
                }
            
            __context__.SourceCodeLine = 540;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (OPTIONID == 4))  ) ) 
                {
                __context__.SourceCodeLine = 540;
                MESSAGE_PROMPT_OPTION4_WASSELECTED  .Value = (ushort) ( 1 ) ; 
                }
            
            else 
                {
                __context__.SourceCodeLine = 540;
                MESSAGE_PROMPT_OPTION4_WASSELECTED  .Value = (ushort) ( 0 ) ; 
                }
            
            __context__.SourceCodeLine = 541;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (OPTIONID == 5))  ) ) 
                {
                __context__.SourceCodeLine = 541;
                MESSAGE_PROMPT_OPTION5_WASSELECTED  .Value = (ushort) ( 1 ) ; 
                }
            
            else 
                {
                __context__.SourceCodeLine = 541;
                MESSAGE_PROMPT_OPTION5_WASSELECTED  .Value = (ushort) ( 0 ) ; 
                }
            
            
            }
            
        private void RESETMESSAGEPROMPT (  SplusExecutionContext __context__ ) 
            { 
            
            __context__.SourceCodeLine = 546;
            MESSAGE_PROMPT_ISACTIVE  .Value = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 547;
            MESSAGE_PROMPT_FEEDBACKID  .UpdateValue ( ""  ) ; 
            __context__.SourceCodeLine = 548;
            MESSAGE_PROMPT_TITLE  .UpdateValue ( ""  ) ; 
            __context__.SourceCodeLine = 549;
            MESSAGE_PROMPT_TEXT  .UpdateValue ( ""  ) ; 
            __context__.SourceCodeLine = 550;
            MESSAGE_PROMPT_OPTION1_TEXT  .UpdateValue ( ""  ) ; 
            __context__.SourceCodeLine = 551;
            MESSAGE_PROMPT_OPTION2_TEXT  .UpdateValue ( ""  ) ; 
            __context__.SourceCodeLine = 552;
            MESSAGE_PROMPT_OPTION3_TEXT  .UpdateValue ( ""  ) ; 
            __context__.SourceCodeLine = 553;
            MESSAGE_PROMPT_OPTION4_TEXT  .UpdateValue ( ""  ) ; 
            __context__.SourceCodeLine = 554;
            MESSAGE_PROMPT_OPTION5_TEXT  .UpdateValue ( ""  ) ; 
            __context__.SourceCodeLine = 555;
            MESSAGE_PROMPT_OPTION1_ISACTIVE  .Value = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 556;
            MESSAGE_PROMPT_OPTION2_ISACTIVE  .Value = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 557;
            MESSAGE_PROMPT_OPTION3_ISACTIVE  .Value = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 558;
            MESSAGE_PROMPT_OPTION4_ISACTIVE  .Value = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 559;
            MESSAGE_PROMPT_OPTION5_ISACTIVE  .Value = (ushort) ( 0 ) ; 
            
            }
            
        private CrestronString GETSTRINGAFTERTOKEN (  SplusExecutionContext __context__, CrestronString TEXT , CrestronString TOKEN ) 
            { 
            ushort MARKER1 = 0;
            ushort MARKER2 = 0;
            
            CrestronString RETVAL;
            RETVAL  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
            
            
            __context__.SourceCodeLine = 566;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Find( "\u000D\u000A" , TEXT ) > 0 ))  ) ) 
                { 
                __context__.SourceCodeLine = 568;
                TEXT  .UpdateValue ( Functions.Mid ( TEXT ,  (int) ( 1 ) ,  (int) ( (Functions.Find( "\u000D\u000A" , TEXT ) - 1) ) )  ) ; 
                } 
            
            __context__.SourceCodeLine = 570;
            MARKER1 = (ushort) ( (Functions.Find( TOKEN , TEXT ) + Functions.Length( TOKEN )) ) ; 
            __context__.SourceCodeLine = 571;
            MARKER2 = (ushort) ( (Functions.Length( TEXT ) - (MARKER1 - 1)) ) ; 
            __context__.SourceCodeLine = 572;
            RETVAL  .UpdateValue ( Functions.Mid ( TEXT ,  (int) ( MARKER1 ) ,  (int) ( MARKER2 ) )  ) ; 
            __context__.SourceCodeLine = 573;
            return ( RETVAL ) ; 
            
            }
            
        private CrestronString GETTAGVALUEREMOVEAMPERSANDS (  SplusExecutionContext __context__, ref CrestronString DATA ) 
            { 
            ushort MARKER1 = 0;
            ushort MARKER2 = 0;
            
            CrestronString VALUE;
            VALUE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 500, this );
            
            
            __context__.SourceCodeLine = 582;
            MARKER1 = (ushort) ( (Functions.Find( ": \u0022" , DATA ) + 3) ) ; 
            __context__.SourceCodeLine = 583;
            MARKER2 = (ushort) ( Functions.Find( "\u0022" , DATA , MARKER1 ) ) ; 
            __context__.SourceCodeLine = 584;
            VALUE  .UpdateValue ( Functions.Mid ( DATA ,  (int) ( MARKER1 ) ,  (int) ( (MARKER2 - MARKER1) ) )  ) ; 
            __context__.SourceCodeLine = 585;
            return ( VALUE ) ; 
            
            }
            
        private CrestronString GETTAGVALUE (  SplusExecutionContext __context__, ref CrestronString DATA ) 
            { 
            ushort MARKER1 = 0;
            ushort MARKER2 = 0;
            
            CrestronString VALUE;
            VALUE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 500, this );
            
            
            __context__.SourceCodeLine = 594;
            if ( Functions.TestForTrue  ( ( Functions.Find( "(ghost=True): /" , DATA ))  ) ) 
                { 
                __context__.SourceCodeLine = 596;
                return ( "" ) ; 
                } 
            
            __context__.SourceCodeLine = 598;
            MARKER1 = (ushort) ( (Functions.Find( ": " , DATA ) + 2) ) ; 
            __context__.SourceCodeLine = 599;
            MARKER2 = (ushort) ( Functions.Find( "\u000D\u000A" , DATA ) ) ; 
            __context__.SourceCodeLine = 600;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (MARKER1 == 0) ) || Functions.TestForTrue ( Functions.BoolToInt (MARKER2 == 0) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt ( MARKER2 <= MARKER1 ) )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 602;
                return ( "" ) ; 
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 606;
                VALUE  .UpdateValue ( Functions.Mid ( DATA ,  (int) ( MARKER1 ) ,  (int) ( (MARKER2 - MARKER1) ) )  ) ; 
                } 
            
            __context__.SourceCodeLine = 608;
            return ( VALUE ) ; 
            
            }
            
        private CrestronString REMOVEAMPERSANDS (  SplusExecutionContext __context__, CrestronString STR ) 
            { 
            ushort MARKER1 = 0;
            ushort MARKER2 = 0;
            
            CrestronString VALUE;
            VALUE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 500, this );
            
            
            __context__.SourceCodeLine = 616;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.Left( STR , (int)( 2 ) ) == "\u0022\u0022"))  ) ) 
                { 
                __context__.SourceCodeLine = 618;
                return ( "" ) ; 
                } 
            
            __context__.SourceCodeLine = 620;
            MARKER1 = (ushort) ( (Functions.Find( "\u0022" , STR ) + 1) ) ; 
            __context__.SourceCodeLine = 621;
            MARKER2 = (ushort) ( (Functions.Find( "\u0022" , STR , MARKER1 ) - MARKER1) ) ; 
            __context__.SourceCodeLine = 622;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( MARKER2 > MARKER1 ))  ) ) 
                { 
                __context__.SourceCodeLine = 624;
                return ( Functions.Mid ( STR ,  (int) ( MARKER1 ) ,  (int) ( MARKER2 ) ) ) ; 
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 628;
                return ( STR ) ; 
                } 
            
            
            return ""; // default return value (none specified in module)
            }
            
        private CrestronString GETSTRINGTOKENVALUE (  SplusExecutionContext __context__, CrestronString TEXT , CrestronString TOKEN ) 
            { 
            ushort MARKER1 = 0;
            ushort MARKER2 = 0;
            
            CrestronString VALUE;
            VALUE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 500, this );
            
            ushort ENDOFTOKEN = 0;
            ushort STARTOFTEXT = 0;
            ushort ENDOFTEXT = 0;
            
            CrestronString RETVAL;
            RETVAL  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
            
            
            __context__.SourceCodeLine = 639;
            ENDOFTOKEN = (ushort) ( (Functions.Find( TOKEN , TEXT ) + Functions.Length( TOKEN )) ) ; 
            __context__.SourceCodeLine = 640;
            STARTOFTEXT = (ushort) ( (Functions.Find( "\u0022" , TEXT , (ENDOFTOKEN + 1) ) + 1) ) ; 
            __context__.SourceCodeLine = 641;
            ENDOFTEXT = (ushort) ( Functions.Find( "\u0022" , TEXT , STARTOFTEXT ) ) ; 
            __context__.SourceCodeLine = 642;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( ENDOFTEXT > STARTOFTEXT ))  ) ) 
                { 
                __context__.SourceCodeLine = 644;
                RETVAL  .UpdateValue ( Functions.Mid ( TEXT ,  (int) ( STARTOFTEXT ) ,  (int) ( (ENDOFTEXT - STARTOFTEXT) ) )  ) ; 
                } 
            
            __context__.SourceCodeLine = 646;
            return ( RETVAL ) ; 
            
            }
            
        private CrestronString GETCALLRATEFROMTEXT (  SplusExecutionContext __context__, CrestronString TEXT ) 
            { 
            CrestronString RETVAL;
            RETVAL  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 20, this );
            
            
            __context__.SourceCodeLine = 652;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (TEXT == ""))  ) ) 
                { 
                __context__.SourceCodeLine = 654;
                return ( TEXT ) ; 
                } 
            
            __context__.SourceCodeLine = 656;
            MakeString ( RETVAL , "{0} Kbps", TEXT ) ; 
            __context__.SourceCodeLine = 657;
            return ( RETVAL ) ; 
            
            }
            
        private CrestronString GETDURATIONFROMTEXT (  SplusExecutionContext __context__, CrestronString TEXT ) 
            { 
            CrestronString RETVAL;
            RETVAL  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 20, this );
            
            
            __context__.SourceCodeLine = 663;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (TEXT == ""))  ) ) 
                { 
                __context__.SourceCodeLine = 665;
                return ( TEXT ) ; 
                } 
            
            __context__.SourceCodeLine = 667;
            MakeString ( RETVAL , "{0} s", TEXT ) ; 
            __context__.SourceCodeLine = 668;
            return ( RETVAL ) ; 
            
            }
            
        private ushort GETCALLSLOTFROMCALLID (  SplusExecutionContext __context__, ushort CALLID ) 
            { 
            ushort CALLNUMBER = 0;
            
            ushort I = 0;
            
            
            __context__.SourceCodeLine = 676;
            CALLNUMBER = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 677;
            ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)3; 
            int __FN_FORSTEP_VAL__1 = (int)1; 
            for ( I  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (I  >= __FN_FORSTART_VAL__1) && (I  <= __FN_FOREND_VAL__1) ) : ( (I  <= __FN_FORSTART_VAL__1) && (I  >= __FN_FOREND_VAL__1) ) ; I  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                __context__.SourceCodeLine = 679;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (CALLSTATUS_CALLID[ I ] .Value == CALLID))  ) ) 
                    { 
                    __context__.SourceCodeLine = 681;
                    CALLNUMBER = (ushort) ( I ) ; 
                    __context__.SourceCodeLine = 682;
                    break ; 
                    } 
                
                else 
                    { 
                    __context__.SourceCodeLine = 686;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (CALLSTATUS_CALLID[ I ] .Value == 0) ) && Functions.TestForTrue ( Functions.BoolToInt (CALLNUMBER == 0) )) ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 688;
                        CALLNUMBER = (ushort) ( I ) ; 
                        } 
                    
                    } 
                
                __context__.SourceCodeLine = 677;
                } 
            
            __context__.SourceCodeLine = 692;
            return (ushort)( CALLNUMBER) ; 
            
            }
            
        private void CLEARCALL (  SplusExecutionContext __context__, ushort CALLSLOT ) 
            { 
            
            __context__.SourceCodeLine = 697;
            CALLSTATUS_CALLID [ CALLSLOT]  .Value = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 698;
            if ( Functions.TestForTrue  ( ( IsSignalDefined( CALLSTATUS_STATUS[ CALLSLOT ] ))  ) ) 
                { 
                __context__.SourceCodeLine = 700;
                CALLSTATUS_STATUS [ CALLSLOT]  .UpdateValue ( ""  ) ; 
                } 
            
            __context__.SourceCodeLine = 702;
            if ( Functions.TestForTrue  ( ( IsSignalDefined( CALLSTATUS_DIRECTION[ CALLSLOT ] ))  ) ) 
                { 
                __context__.SourceCodeLine = 704;
                CALLSTATUS_DIRECTION [ CALLSLOT]  .UpdateValue ( ""  ) ; 
                } 
            
            __context__.SourceCodeLine = 706;
            if ( Functions.TestForTrue  ( ( IsSignalDefined( CALLSTATUS_CALLTYPE[ CALLSLOT ] ))  ) ) 
                { 
                __context__.SourceCodeLine = 708;
                CALLSTATUS_CALLTYPE [ CALLSLOT]  .UpdateValue ( ""  ) ; 
                } 
            
            __context__.SourceCodeLine = 710;
            if ( Functions.TestForTrue  ( ( IsSignalDefined( CALLSTATUS_TRANSMITCALLRATE[ CALLSLOT ] ))  ) ) 
                { 
                __context__.SourceCodeLine = 712;
                CALLSTATUS_TRANSMITCALLRATE [ CALLSLOT]  .UpdateValue ( ""  ) ; 
                } 
            
            __context__.SourceCodeLine = 714;
            if ( Functions.TestForTrue  ( ( IsSignalDefined( CALLSTATUS_RECEIVECALLRATE[ CALLSLOT ] ))  ) ) 
                { 
                __context__.SourceCodeLine = 716;
                CALLSTATUS_RECEIVECALLRATE [ CALLSLOT]  .UpdateValue ( ""  ) ; 
                } 
            
            __context__.SourceCodeLine = 718;
            if ( Functions.TestForTrue  ( ( IsSignalDefined( CALLSTATUS_REMOTESITE[ CALLSLOT ] ))  ) ) 
                { 
                __context__.SourceCodeLine = 720;
                CALLSTATUS_REMOTESITE [ CALLSLOT]  .UpdateValue ( ""  ) ; 
                } 
            
            __context__.SourceCodeLine = 722;
            if ( Functions.TestForTrue  ( ( IsSignalDefined( CALLSTATUS_DURATION[ CALLSLOT ] ))  ) ) 
                { 
                __context__.SourceCodeLine = 724;
                CALLSTATUS_DURATION [ CALLSLOT]  .UpdateValue ( ""  ) ; 
                } 
            
            __context__.SourceCodeLine = 726;
            CALLSTATUS_ISACTIVE [ CALLSLOT]  .Value = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 727;
            CALLSTATUS_ISCONNECTED [ CALLSLOT]  .Value = (ushort) ( 0 ) ; 
            
            }
            
        private void RESETCALLLIST (  SplusExecutionContext __context__ ) 
            { 
            ushort I = 0;
            
            
            __context__.SourceCodeLine = 735;
            ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)3; 
            int __FN_FORSTEP_VAL__1 = (int)1; 
            for ( I  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (I  >= __FN_FORSTART_VAL__1) && (I  <= __FN_FOREND_VAL__1) ) : ( (I  <= __FN_FORSTART_VAL__1) && (I  >= __FN_FOREND_VAL__1) ) ; I  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                __context__.SourceCodeLine = 737;
                CLEARCALL (  __context__ , (ushort)( I )) ; 
                __context__.SourceCodeLine = 735;
                } 
            
            __context__.SourceCodeLine = 739;
            MakeString ( DEVICE_TX__DOLLAR__ , "xStatus Call\r\n") ; 
            
            }
            
        object RESYNCRONIZECALLSTATUSLIST_OnPush_0 ( Object __EventInfo__ )
        
            { 
            Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
            try
            {
                SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
                ushort I = 0;
                
                
                __context__.SourceCodeLine = 746;
                ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
                ushort __FN_FOREND_VAL__1 = (ushort)3; 
                int __FN_FORSTEP_VAL__1 = (int)1; 
                for ( I  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (I  >= __FN_FORSTART_VAL__1) && (I  <= __FN_FOREND_VAL__1) ) : ( (I  <= __FN_FORSTART_VAL__1) && (I  >= __FN_FOREND_VAL__1) ) ; I  += (ushort)__FN_FORSTEP_VAL__1) 
                    { 
                    __context__.SourceCodeLine = 748;
                    CLEARCALL (  __context__ , (ushort)( I )) ; 
                    __context__.SourceCodeLine = 746;
                    } 
                
                __context__.SourceCodeLine = 750;
                MakeString ( DEVICE_TX__DOLLAR__ , "xStatus Call\r\n") ; 
                
                
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler( __SignalEventArg__ ); }
            return this;
            
        }
        
    private void RESETPHONEBOOKLIST (  SplusExecutionContext __context__ ) 
        { 
        ushort I = 0;
        
        
        __context__.SourceCodeLine = 758;
        TOTALROWSFORSEARCH  .Value = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 760;
        ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
        ushort __FN_FOREND_VAL__1 = (ushort)255; 
        int __FN_FORSTEP_VAL__1 = (int)1; 
        for ( I  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (I  >= __FN_FORSTART_VAL__1) && (I  <= __FN_FOREND_VAL__1) ) : ( (I  <= __FN_FORSTART_VAL__1) && (I  >= __FN_FOREND_VAL__1) ) ; I  += (ushort)__FN_FORSTEP_VAL__1) 
            { 
            __context__.SourceCodeLine = 762;
            if ( Functions.TestForTrue  ( ( IsSignalDefined( PHONEBOOK_NAME[ I ] ))  ) ) 
                { 
                __context__.SourceCodeLine = 764;
                PHONEBOOK_NAME [ I]  .UpdateValue ( ""  ) ; 
                } 
            
            __context__.SourceCodeLine = 760;
            } 
        
        
        }
        
    private void RESETSELECTEDPHONEBOOKITEM (  SplusExecutionContext __context__ ) 
        { 
        
        __context__.SourceCodeLine = 772;
        SELECTEDPHONEBOOK_NAME  .UpdateValue ( ""  ) ; 
        __context__.SourceCodeLine = 773;
        SELECTEDPHONEBOOK_NUMBER  .UpdateValue ( ""  ) ; 
        __context__.SourceCodeLine = 774;
        SELECTEDPHONEBOOK_CALLRATE  .UpdateValue ( ""  ) ; 
        __context__.SourceCodeLine = 775;
        SELECTEDISFOLDER  .Value = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 776;
        SELECTEDISCONTACT  .Value = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 777;
        SELECTEDENTRYMETHOD = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 778;
        SELECTEDENTRY = (ushort) ( 0 ) ; 
        
        }
        
    private void DISCONNECTCALL (  SplusExecutionContext __context__, ushort CALLSLOT ) 
        { 
        
        __context__.SourceCodeLine = 783;
        MakeString ( DEVICE_TX__DOLLAR__ , "xCommand Call Disconnect CallId: {0:d}\r\n", (short)CALLSTATUS_CALLID[ CALLSLOT ] .Value) ; 
        
        }
        
    private void VALIDRESPONSEFOUND (  SplusExecutionContext __context__ ) 
        { 
        
        __context__.SourceCodeLine = 788;
        if ( Functions.TestForTrue  ( ( Functions.Not( DEVICE_ONLINE  .Value ))  ) ) 
            { 
            __context__.SourceCodeLine = 790;
            DEVICE_ONLINE  .Value = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 792;
            CreateWait ( "COMMTIMEOUT" , 2500 , COMMTIMEOUT_Callback ) ;
            } 
        
        else 
            { 
            __context__.SourceCodeLine = 799;
            RetimeWait ( (int)2500, "COMMTIMEOUT" ) ; 
            } 
        
        
        }
        
    public void COMMTIMEOUT_CallbackFn( object stateInfo )
    {
    
        try
        {
            Wait __LocalWait__ = (Wait)stateInfo;
            SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
            __LocalWait__.RemoveFromList();
            
            
            __context__.SourceCodeLine = 794;
            DEVICE_ONLINE  .Value = (ushort) ( 0 ) ; 
            
        
        
        }
        catch(Exception e) { ObjectCatchHandler(e); }
        finally { ObjectFinallyHandler(); }
        
    }
    
private void PARSECALLFEEDBACK (  SplusExecutionContext __context__ ) 
    { 
    
    __context__.SourceCodeLine = 805;
    CALLID = (ushort) ( Functions.Atoi( Functions.Remove( " " , TEMPSTRING__DOLLAR__ ) ) ) ; 
    __context__.SourceCodeLine = 806;
    CALLSLOT = (ushort) ( GETCALLSLOTFROMCALLID( __context__ , (ushort)( CALLID ) ) ) ; 
    __context__.SourceCodeLine = 807;
    CALLSTATUS_CALLID [ CALLSLOT]  .Value = (ushort) ( CALLID ) ; 
    __context__.SourceCodeLine = 808;
    CALLRESPONSETYPE  .UpdateValue ( Functions.Remove ( ":" , TEMPSTRING__DOLLAR__ )  ) ; 
    __context__.SourceCodeLine = 809;
    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (CALLRESPONSETYPE == "(ghost=True):"))  ) ) 
        { 
        __context__.SourceCodeLine = 811;
        CLEARCALL (  __context__ , (ushort)( CALLSLOT )) ; 
        } 
    
    else 
        {
        __context__.SourceCodeLine = 813;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (CALLRESPONSETYPE == "Status:"))  ) ) 
            { 
            __context__.SourceCodeLine = 815;
            CALLSTATUS_ISACTIVE [ CALLSLOT]  .Value = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 816;
            CALLSTATUS  .UpdateValue ( GETTAGVALUE (  __context__ ,   ref  TEMPSTRING__DOLLAR__ )  ) ; 
            __context__.SourceCodeLine = 817;
            if ( Functions.TestForTrue  ( ( IsSignalDefined( CALLSTATUS_STATUS[ CALLSLOT ] ))  ) ) 
                { 
                __context__.SourceCodeLine = 819;
                CALLSTATUS_STATUS [ CALLSLOT]  .UpdateValue ( CALLSTATUS  ) ; 
                } 
            
            __context__.SourceCodeLine = 821;
            if ( Functions.TestForTrue  ( ( IsSignalDefined( CALLSTATUS_ISCONNECTED[ CALLSLOT ] ))  ) ) 
                { 
                __context__.SourceCodeLine = 823;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (CALLSTATUS == "Connected"))  ) ) 
                    { 
                    __context__.SourceCodeLine = 825;
                    CALLSTATUS_ISCONNECTED [ CALLSLOT]  .Value = (ushort) ( 1 ) ; 
                    } 
                
                else 
                    { 
                    __context__.SourceCodeLine = 829;
                    CALLSTATUS_ISCONNECTED [ CALLSLOT]  .Value = (ushort) ( 0 ) ; 
                    } 
                
                } 
            
            } 
        
        else 
            {
            __context__.SourceCodeLine = 833;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (CALLRESPONSETYPE == "RemoteNumber:"))  ) ) 
                { 
                __context__.SourceCodeLine = 835;
                if ( Functions.TestForTrue  ( ( IsSignalDefined( CALLSTATUS_REMOTESITE[ CALLSLOT ] ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 837;
                    CALLSTATUS_REMOTESITE [ CALLSLOT]  .UpdateValue ( REMOVEAMPERSANDS (  __context__ , GETTAGVALUE( __context__ , ref TEMPSTRING__DOLLAR__ ))  ) ; 
                    __context__.SourceCodeLine = 837;
                    ; 
                    } 
                
                } 
            
            else 
                {
                __context__.SourceCodeLine = 841;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (CALLRESPONSETYPE == "Duration:"))  ) ) 
                    { 
                    __context__.SourceCodeLine = 843;
                    if ( Functions.TestForTrue  ( ( IsSignalDefined( CALLSTATUS_DURATION[ CALLSLOT ] ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 845;
                        CALLSTATUS_DURATION [ CALLSLOT]  .UpdateValue ( GETDURATIONFROMTEXT (  __context__ , GETTAGVALUE( __context__ , ref TEMPSTRING__DOLLAR__ ))  ) ; 
                        } 
                    
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 849;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (CALLRESPONSETYPE == "CallType:"))  ) ) 
                        { 
                        __context__.SourceCodeLine = 851;
                        if ( Functions.TestForTrue  ( ( IsSignalDefined( CALLSTATUS_CALLTYPE[ CALLSLOT ] ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 853;
                            CALLSTATUS_CALLTYPE [ CALLSLOT]  .UpdateValue ( GETTAGVALUE (  __context__ ,   ref  TEMPSTRING__DOLLAR__ )  ) ; 
                            } 
                        
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 857;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (CALLRESPONSETYPE == "Direction:"))  ) ) 
                            { 
                            __context__.SourceCodeLine = 859;
                            if ( Functions.TestForTrue  ( ( IsSignalDefined( CALLSTATUS_DIRECTION[ CALLSLOT ] ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 861;
                                CALLSTATUS_DIRECTION [ CALLSLOT]  .UpdateValue ( GETTAGVALUE (  __context__ ,   ref  TEMPSTRING__DOLLAR__ )  ) ; 
                                } 
                            
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 864;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (CALLRESPONSETYPE == "DisplayName:"))  ) ) 
                                { 
                                __context__.SourceCodeLine = 866;
                                if ( Functions.TestForTrue  ( ( IsSignalDefined( CALLSTATUS_REMOTESITE[ CALLSLOT ] ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 868;
                                    GTEMPDISPLAYNAME  .UpdateValue ( REMOVEAMPERSANDS (  __context__ , GETTAGVALUE( __context__ , ref TEMPSTRING__DOLLAR__ ))  ) ; 
                                    __context__.SourceCodeLine = 869;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPDISPLAYNAME == ""))  ) ) 
                                        { 
                                        } 
                                    
                                    else 
                                        { 
                                        __context__.SourceCodeLine = 875;
                                        CALLSTATUS_REMOTESITE [ CALLSLOT]  .UpdateValue ( GTEMPDISPLAYNAME  ) ; 
                                        } 
                                    
                                    } 
                                
                                } 
                            
                            else 
                                {
                                __context__.SourceCodeLine = 879;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (CALLRESPONSETYPE == "CallbackNumber:"))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 881;
                                    if ( Functions.TestForTrue  ( ( IsSignalDefined( CALLSTATUS_REMOTESITE[ CALLSLOT ] ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 883;
                                        CALLSTATUS_REMOTESITE [ CALLSLOT]  .UpdateValue ( REMOVEAMPERSANDS (  __context__ , GETTAGVALUE( __context__ , ref TEMPSTRING__DOLLAR__ ))  ) ; 
                                        } 
                                    
                                    } 
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 886;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (CALLRESPONSETYPE == "TransmitCallRate:"))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 888;
                                        if ( Functions.TestForTrue  ( ( IsSignalDefined( CALLSTATUS_TRANSMITCALLRATE[ CALLSLOT ] ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 890;
                                            CALLSTATUS_TRANSMITCALLRATE [ CALLSLOT]  .UpdateValue ( GETCALLRATEFROMTEXT (  __context__ , GETTAGVALUE( __context__ , ref TEMPSTRING__DOLLAR__ ))  ) ; 
                                            } 
                                        
                                        } 
                                    
                                    else 
                                        {
                                        __context__.SourceCodeLine = 893;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (CALLRESPONSETYPE == "ReceiveCallRate:"))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 895;
                                            if ( Functions.TestForTrue  ( ( IsSignalDefined( CALLSTATUS_RECEIVECALLRATE[ CALLSLOT ] ))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 897;
                                                CALLSTATUS_RECEIVECALLRATE [ CALLSLOT]  .UpdateValue ( GETCALLRATEFROMTEXT (  __context__ , GETTAGVALUE( __context__ , ref TEMPSTRING__DOLLAR__ ))  ) ; 
                                                } 
                                            
                                            } 
                                        
                                        }
                                    
                                    }
                                
                                }
                            
                            }
                        
                        }
                    
                    }
                
                }
            
            }
        
        }
    
    
    }
    
private void DISPLAYPHONEBOOK (  SplusExecutionContext __context__ ) 
    { 
    ushort I = 0;
    
    ushort FOLDERTEMP = 0;
    
    
    __context__.SourceCodeLine = 906;
    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (RECENTCALLSDOWNLOADBUSY  .Value == 0))  ) ) 
        { 
        __context__.SourceCodeLine = 909;
        FOLDERTEMP = (ushort) ( SELECTEDFOLDER ) ; 
        __context__.SourceCodeLine = 911;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (PHONEBOOKENTRY[ FOLDERTEMP ].FIRSTCHILD == (2000 + 1)) ) || Functions.TestForTrue ( Functions.BoolToInt (NEXTAVAILABLEDIRENTRY == 0) )) ))  ) ) 
            { 
            __context__.SourceCodeLine = 913;
            TOTALROWSFORSEARCH  .Value = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 914;
            PHONEBOOK_NAME [ 1]  .UpdateValue ( "No Contacts"  ) ; 
            } 
        
        else 
            { 
            __context__.SourceCodeLine = 918;
            TOTALROWSFORSEARCH  .Value = (ushort) ( Functions.Min( PHONEBOOKENTRY[ FOLDERTEMP ].CHILDCOUNT , 255 ) ) ; 
            __context__.SourceCodeLine = 919;
            ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)TOTALROWSFORSEARCH  .Value; 
            int __FN_FORSTEP_VAL__1 = (int)1; 
            for ( I  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (I  >= __FN_FORSTART_VAL__1) && (I  <= __FN_FOREND_VAL__1) ) : ( (I  <= __FN_FORSTART_VAL__1) && (I  >= __FN_FOREND_VAL__1) ) ; I  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                __context__.SourceCodeLine = 921;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( PHONEBOOKENTRY[ FOLDERTEMP ].CHILDCOUNT > SEARCHDELAYSTART  .UshortValue ) ) && Functions.TestForTrue ( Functions.BoolToInt (Mod( I , SEARCHDELAYMOD  .UshortValue ) == 0) )) ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 923;
                    Functions.Delay (  (int) ( SEARCHDELAY  .UshortValue ) ) ; 
                    } 
                
                __context__.SourceCodeLine = 925;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (FOLDERTEMP != SELECTEDFOLDER))  ) ) 
                    { 
                    __context__.SourceCodeLine = 927;
                    return ; 
                    } 
                
                __context__.SourceCodeLine = 930;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( PHONEBOOKENTRY[ ((I + PHONEBOOKENTRY[ FOLDERTEMP ].FIRSTCHILD) - 1) ].FOLDERID ) > 0 ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 932;
                    PHONEBOOK_NAME [ I]  .UpdateValue ( "[+]  " + PHONEBOOKENTRY [ ((I + PHONEBOOKENTRY[ FOLDERTEMP ].FIRSTCHILD) - 1)] . ENTRYNAME  ) ; 
                    } 
                
                else 
                    { 
                    __context__.SourceCodeLine = 936;
                    PHONEBOOK_NAME [ I]  .UpdateValue ( PHONEBOOKENTRY [ ((I + PHONEBOOKENTRY[ FOLDERTEMP ].FIRSTCHILD) - 1)] . ENTRYNAME  ) ; 
                    } 
                
                __context__.SourceCodeLine = 919;
                } 
            
            } 
        
        } 
    
    
    }
    
private void RUNSEARCHPHONEBOOK (  SplusExecutionContext __context__ ) 
    { 
    ushort I = 0;
    
    ushort NEXTENTRYTODISPLAY = 0;
    
    CrestronString PATH;
    PATH  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 125, this );
    
    CrestronString SEARCHSTRING;
    SEARCHSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 81, this );
    
    ushort FIRSTENTRYTODISPLAY = 0;
    
    
    __context__.SourceCodeLine = 952;
    FIRSTENTRYTODISPLAY = (ushort) ( (20 * INTERNALENTRYOFFSET) ) ; 
    __context__.SourceCodeLine = 953;
    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (NEXTFOLDERENTRY != 0) ) && Functions.TestForTrue ( Functions.BoolToInt ( NEXTFOLDERENTRY < 2000 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt (PHONEBOOKENTRY[ NEXTFOLDERENTRY ].FOLDERID != "crestron_0") )) ))  ) ) 
        { 
        __context__.SourceCodeLine = 955;
        PHONEBOOK_SEARCHRESULT_IS_FROM_SUBFOLDER  .Value = (ushort) ( 1 ) ; 
        __context__.SourceCodeLine = 956;
        MakeString ( PATH , " FolderId: \u0022{0}\u0022 ", PHONEBOOKENTRY [ NEXTFOLDERENTRY] . FOLDERID ) ; 
        } 
    
    else 
        { 
        __context__.SourceCodeLine = 960;
        PHONEBOOK_SEARCHRESULT_IS_FROM_SUBFOLDER  .Value = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 961;
        PATH  .UpdateValue ( ""  ) ; 
        } 
    
    __context__.SourceCodeLine = 965;
    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (RECEIVINGPHONEBOOK == 0))  ) ) 
        { 
        __context__.SourceCodeLine = 967;
        RECEIVINGPHONEBOOK = (ushort) ( 1 ) ; 
        __context__.SourceCodeLine = 968;
        PREVIOUSFOUNDCONTACTITEM = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 969;
        MakeString ( SEARCHSTRING , " SearchString: \"{0}\" ", SSEARCHTEXT ) ; 
        __context__.SourceCodeLine = 970;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (PHONEBOOKENTRY[ NEXTFOLDERENTRY ].FOLDERID == "crestron_0") ) && Functions.TestForTrue ( Functions.BoolToInt ( PHONEBOOKENTRY[ NEXTFOLDERENTRY ].CHILDCOUNT < 40 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (PHONEBOOKENTRY[ NEXTFOLDERENTRY ].CHILDCOUNT == 20) ) || Functions.TestForTrue ( Functions.BoolToInt (PHONEBOOKENTRY[ NEXTFOLDERENTRY ].CHILDCOUNT == 0) )) ) )) ))  ) ) 
            { 
            __context__.SourceCodeLine = 972;
            MakeString ( DEVICE_TX__DOLLAR__ , "xCommand CallHistory Recents Filter: All Offset: {0:d} Limit: {1:d} DetailLevel: Basic\r\n", (short)FIRSTENTRYTODISPLAY, (short)20) ; 
            } 
        
        else 
            { 
            __context__.SourceCodeLine = 976;
            MakeString ( DEVICE_TX__DOLLAR__ , "xcommand Phonebook Search PhonebookType:{0} Limit:{1:d} Offset: {2:d} {3}{4} Recursive:False\r\n", PHONEBOOKTYPE , (short)20, (short)FIRSTENTRYTODISPLAY, PATH , SEARCHSTRING ) ; 
            } 
        
        __context__.SourceCodeLine = 979;
        CreateWait ( "RECEIVEPHONEBOOK" , 1000 , RECEIVEPHONEBOOK_Callback ) ;
        } 
    
    
    }
    
public void RECEIVEPHONEBOOK_CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            
            __context__.SourceCodeLine = 981;
            RECEIVINGPHONEBOOK = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 982;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( PHONEBOOKPARSEFAILURE < 1 ))  ) ) 
                { 
                __context__.SourceCodeLine = 984;
                GenerateUserWarning ( "Directory Download Timed Out. (Attempt: {0:d}) (Folder Name: {1}) (FolderID {2})", (short)(PHONEBOOKPARSEFAILURE + 1), PHONEBOOKENTRY [ NEXTFOLDERENTRY] . ENTRYNAME , PHONEBOOKENTRY [ NEXTFOLDERENTRY] . FOLDERID ) ; 
                __context__.SourceCodeLine = 985;
                PHONEBOOKPARSEFAILURE = (ushort) ( (PHONEBOOKPARSEFAILURE + 1) ) ; 
                __context__.SourceCodeLine = 986;
                NEXTAVAILABLEDIRENTRY = (ushort) ( ((PHONEBOOKENTRY[ NEXTFOLDERENTRY ].FIRSTCHILD + (20 * INTERNALENTRYOFFSET)) - 1) ) ; 
                __context__.SourceCodeLine = 987;
                PHONEBOOKENTRY [ NEXTFOLDERENTRY] . CHILDCOUNT = (ushort) ( (20 * INTERNALENTRYOFFSET) ) ; 
                __context__.SourceCodeLine = 988;
                RUNSEARCHPHONEBOOK (  __context__  ) ; 
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 992;
                GenerateUserError ( "Directory Download Timed Out. (Folder Name: {0}) (FolderID: {1}) (Entries Expected: {2:d}) (Entries Receive: {3:d})", PHONEBOOKENTRY [ NEXTFOLDERENTRY] . ENTRYNAME , PHONEBOOKENTRY [ NEXTFOLDERENTRY] . FOLDERID , (short)PHONEBOOKLEVELTOTALROWS, (short)PHONEBOOKENTRY[ NEXTFOLDERENTRY ].CHILDCOUNT) ; 
                __context__.SourceCodeLine = 993;
                PHONEBOOKPARSEFAILURE = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 994;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (PHONEBOOKENTRY[ NEXTFOLDERENTRY ].CHILDCOUNT == 0))  ) ) 
                    { 
                    __context__.SourceCodeLine = 996;
                    PHONEBOOKENTRY [ NEXTFOLDERENTRY] . FIRSTCHILD = (ushort) ( (2000 + 1) ) ; 
                    } 
                
                __context__.SourceCodeLine = 998;
                INTERNALENTRYOFFSET = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 999;
                do 
                    { 
                    __context__.SourceCodeLine = 1001;
                    NEXTFOLDERENTRY = (ushort) ( (NEXTFOLDERENTRY + 1) ) ; 
                    } 
                while (false == ( Functions.TestForTrue  ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( NEXTFOLDERENTRY >= 2000 ) ) || Functions.TestForTrue ( Functions.BoolToInt ( Functions.Length( PHONEBOOKENTRY[ NEXTFOLDERENTRY ].FOLDERID ) > 0 ) )) )) )); 
                __context__.SourceCodeLine = 1004;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( NEXTFOLDERENTRY < 2000 ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 1006;
                    PHONEBOOKENTRY [ NEXTFOLDERENTRY] . FIRSTCHILD = (ushort) ( (NEXTAVAILABLEDIRENTRY + 1) ) ; 
                    __context__.SourceCodeLine = 1007;
                    RUNSEARCHPHONEBOOK (  __context__  ) ; 
                    } 
                
                else 
                    { 
                    __context__.SourceCodeLine = 1011;
                    SELECTEDFOLDER = (ushort) ( 0 ) ; 
                    __context__.SourceCodeLine = 1012;
                    DIRECTORYONTOPLEVEL  .Value = (ushort) ( 1 ) ; 
                    __context__.SourceCodeLine = 1013;
                    GenerateUserNotice ( "Directory Download Finished. (Entries: {0:d}) Last Folder Failed. (Folder Name: {1}) (FolderID: {2})", (short)NEXTAVAILABLEDIRENTRY, PHONEBOOKENTRY [ NEXTFOLDERENTRY] . ENTRYNAME , PHONEBOOKENTRY [ NEXTFOLDERENTRY] . FOLDERID ) ; 
                    __context__.SourceCodeLine = 1014;
                    ICURRENTPAGE = (ushort) ( 1 ) ; 
                    __context__.SourceCodeLine = 1015;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Mod( PHONEBOOKENTRY[ SELECTEDFOLDER ].CHILDCOUNT , IENTRIESPERPAGE ) > 0 ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 1017;
                        ITOTALPAGES = (ushort) ( ((PHONEBOOKENTRY[ SELECTEDFOLDER ].CHILDCOUNT / IENTRIESPERPAGE) + 1) ) ; 
                        } 
                    
                    else 
                        { 
                        __context__.SourceCodeLine = 1021;
                        ITOTALPAGES = (ushort) ( (PHONEBOOKENTRY[ SELECTEDFOLDER ].CHILDCOUNT / IENTRIESPERPAGE) ) ; 
                        } 
                    
                    __context__.SourceCodeLine = 1023;
                    PHONEBOOKPAGESCURRENTPAGE  .Value = (ushort) ( ICURRENTPAGE ) ; 
                    __context__.SourceCodeLine = 1024;
                    PHONEBOOKPAGESTOTALPAGES  .Value = (ushort) ( ITOTALPAGES ) ; 
                    __context__.SourceCodeLine = 1025;
                    FDISPLAYNEWPAGE (  __context__  ) ; 
                    __context__.SourceCodeLine = 1026;
                    DISPLAYPHONEBOOK (  __context__  ) ; 
                    __context__.SourceCodeLine = 1027;
                    DIRECTORYDOWNLOADBUSY  .Value = (ushort) ( 0 ) ; 
                    } 
                
                } 
            
            
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

private void PROCESSFEEDBACK (  SplusExecutionContext __context__ ) 
    { 
    
    __context__.SourceCodeLine = 1036;
    while ( Functions.TestForTrue  ( ( IPROCESS)  ) ) 
        { 
        __context__.SourceCodeLine = 1039;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (TEMPSTRING__DOLLAR__ == "OK\u000D\u000A") ) || Functions.TestForTrue ( Functions.BoolToInt (TEMPSTRING__DOLLAR__ == "\u000D\u000A") )) ))  ) ) 
            { 
            __context__.SourceCodeLine = 1041;
            VALIDRESPONSEFOUND (  __context__  ) ; 
            } 
        
        else 
            {
            __context__.SourceCodeLine = 1043;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (TEMPSTRING__DOLLAR__ == "** end\u000D\u000A"))  ) ) 
                { 
                __context__.SourceCodeLine = 1045;
                VALIDRESPONSEFOUND (  __context__  ) ; 
                __context__.SourceCodeLine = 1046;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (RECEIVINGPHONEBOOK == 1) ) && Functions.TestForTrue ( Functions.BoolToInt (BGETRECENTSONLY == 0) )) ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 1048;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (NEXTAVAILABLEDIRENTRY == (((PHONEBOOKENTRY[ NEXTFOLDERENTRY ].FIRSTCHILD + (20 * INTERNALENTRYOFFSET)) + 20) - 1)) ) || Functions.TestForTrue ( Functions.BoolToInt (NEXTAVAILABLEDIRENTRY == ((PHONEBOOKENTRY[ NEXTFOLDERENTRY ].FIRSTCHILD + PHONEBOOKLEVELTOTALROWS) - 1)) )) ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 1050;
                        CancelWait ( "RECEIVEPHONEBOOK" ) ; 
                        __context__.SourceCodeLine = 1051;
                        PHONEBOOKPARSEFAILURE = (ushort) ( 0 ) ; 
                        __context__.SourceCodeLine = 1052;
                        RECEIVINGPHONEBOOK = (ushort) ( 0 ) ; 
                        __context__.SourceCodeLine = 1053;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( NEXTAVAILABLEDIRENTRY >= 2000 ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 1055;
                            SELECTEDFOLDER = (ushort) ( 0 ) ; 
                            __context__.SourceCodeLine = 1056;
                            DIRECTORYONTOPLEVEL  .Value = (ushort) ( 1 ) ; 
                            __context__.SourceCodeLine = 1057;
                            GenerateUserNotice ( "Directory Download Finished. Found Max Entries. (Entries: {0:d})", (short)NEXTAVAILABLEDIRENTRY) ; 
                            __context__.SourceCodeLine = 1058;
                            ICURRENTPAGE = (ushort) ( 1 ) ; 
                            __context__.SourceCodeLine = 1059;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Mod( PHONEBOOKENTRY[ SELECTEDFOLDER ].CHILDCOUNT , IENTRIESPERPAGE ) > 0 ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 1061;
                                ITOTALPAGES = (ushort) ( ((PHONEBOOKENTRY[ SELECTEDFOLDER ].CHILDCOUNT / IENTRIESPERPAGE) + 1) ) ; 
                                } 
                            
                            else 
                                { 
                                __context__.SourceCodeLine = 1065;
                                ITOTALPAGES = (ushort) ( (PHONEBOOKENTRY[ SELECTEDFOLDER ].CHILDCOUNT / IENTRIESPERPAGE) ) ; 
                                } 
                            
                            __context__.SourceCodeLine = 1067;
                            PHONEBOOKPAGESCURRENTPAGE  .Value = (ushort) ( ICURRENTPAGE ) ; 
                            __context__.SourceCodeLine = 1068;
                            PHONEBOOKPAGESTOTALPAGES  .Value = (ushort) ( ITOTALPAGES ) ; 
                            __context__.SourceCodeLine = 1069;
                            FDISPLAYNEWPAGE (  __context__  ) ; 
                            __context__.SourceCodeLine = 1070;
                            DISPLAYPHONEBOOK (  __context__  ) ; 
                            __context__.SourceCodeLine = 1071;
                            DIRECTORYDOWNLOADBUSY  .Value = (ushort) ( 0 ) ; 
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 1073;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (ENABLERECENTCALLS  .Value == 1) ) && Functions.TestForTrue ( Functions.BoolToInt (PHONEBOOKENTRY[ NEXTFOLDERENTRY ].FOLDERID == "crestron_0") )) ) ) && Functions.TestForTrue ( Functions.BoolToInt (PHONEBOOKENTRY[ NEXTFOLDERENTRY ].CHILDCOUNT == 20) )) ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 1075;
                                INTERNALENTRYOFFSET = (ushort) ( (INTERNALENTRYOFFSET + 1) ) ; 
                                __context__.SourceCodeLine = 1076;
                                RUNSEARCHPHONEBOOK (  __context__  ) ; 
                                } 
                            
                            else 
                                {
                                __context__.SourceCodeLine = 1078;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( NEXTAVAILABLEDIRENTRY < ((PHONEBOOKENTRY[ NEXTFOLDERENTRY ].FIRSTCHILD + PHONEBOOKLEVELTOTALROWS) - 1) ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 1080;
                                    INTERNALENTRYOFFSET = (ushort) ( (INTERNALENTRYOFFSET + 1) ) ; 
                                    __context__.SourceCodeLine = 1081;
                                    RUNSEARCHPHONEBOOK (  __context__  ) ; 
                                    } 
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 1083;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( NEXTAVAILABLEDIRENTRY >= ((PHONEBOOKENTRY[ NEXTFOLDERENTRY ].FIRSTCHILD + PHONEBOOKLEVELTOTALROWS) - 1) ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 1085;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (PHONEBOOKLEVELTOTALROWS == 0) ) && Functions.TestForTrue ( Functions.BoolToInt (INTERNALENTRYOFFSET == 0) )) ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 1087;
                                            PHONEBOOKENTRY [ NEXTFOLDERENTRY] . FIRSTCHILD = (ushort) ( (2000 + 1) ) ; 
                                            } 
                                        
                                        __context__.SourceCodeLine = 1089;
                                        INTERNALENTRYOFFSET = (ushort) ( 0 ) ; 
                                        __context__.SourceCodeLine = 1090;
                                        do 
                                            { 
                                            __context__.SourceCodeLine = 1092;
                                            NEXTFOLDERENTRY = (ushort) ( (NEXTFOLDERENTRY + 1) ) ; 
                                            } 
                                        while (false == ( Functions.TestForTrue  ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (NEXTFOLDERENTRY == 2000) ) || Functions.TestForTrue ( Functions.BoolToInt ( Functions.Length( PHONEBOOKENTRY[ NEXTFOLDERENTRY ].FOLDERID ) > 0 ) )) )) )); 
                                        __context__.SourceCodeLine = 1095;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( NEXTFOLDERENTRY < 2000 ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 1097;
                                            PHONEBOOKENTRY [ NEXTFOLDERENTRY] . FIRSTCHILD = (ushort) ( (NEXTAVAILABLEDIRENTRY + 1) ) ; 
                                            __context__.SourceCodeLine = 1098;
                                            RUNSEARCHPHONEBOOK (  __context__  ) ; 
                                            } 
                                        
                                        else 
                                            {
                                            __context__.SourceCodeLine = 1100;
                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( NEXTFOLDERENTRY >= 2000 ))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 1102;
                                                SELECTEDFOLDER = (ushort) ( 0 ) ; 
                                                __context__.SourceCodeLine = 1103;
                                                DIRECTORYONTOPLEVEL  .Value = (ushort) ( 1 ) ; 
                                                __context__.SourceCodeLine = 1104;
                                                GenerateUserNotice ( "Directory Download Finished. (Entries: {0:d})", (short)NEXTAVAILABLEDIRENTRY) ; 
                                                __context__.SourceCodeLine = 1105;
                                                ICURRENTPAGE = (ushort) ( 1 ) ; 
                                                __context__.SourceCodeLine = 1106;
                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Mod( PHONEBOOKENTRY[ SELECTEDFOLDER ].CHILDCOUNT , IENTRIESPERPAGE ) > 0 ))  ) ) 
                                                    { 
                                                    __context__.SourceCodeLine = 1108;
                                                    ITOTALPAGES = (ushort) ( ((PHONEBOOKENTRY[ SELECTEDFOLDER ].CHILDCOUNT / IENTRIESPERPAGE) + 1) ) ; 
                                                    } 
                                                
                                                else 
                                                    { 
                                                    __context__.SourceCodeLine = 1112;
                                                    ITOTALPAGES = (ushort) ( (PHONEBOOKENTRY[ SELECTEDFOLDER ].CHILDCOUNT / IENTRIESPERPAGE) ) ; 
                                                    } 
                                                
                                                __context__.SourceCodeLine = 1114;
                                                PHONEBOOKPAGESCURRENTPAGE  .Value = (ushort) ( ICURRENTPAGE ) ; 
                                                __context__.SourceCodeLine = 1115;
                                                PHONEBOOKPAGESTOTALPAGES  .Value = (ushort) ( ITOTALPAGES ) ; 
                                                __context__.SourceCodeLine = 1116;
                                                FDISPLAYNEWPAGE (  __context__  ) ; 
                                                __context__.SourceCodeLine = 1117;
                                                DISPLAYPHONEBOOK (  __context__  ) ; 
                                                __context__.SourceCodeLine = 1118;
                                                DIRECTORYDOWNLOADBUSY  .Value = (ushort) ( 0 ) ; 
                                                } 
                                            
                                            }
                                        
                                        } 
                                    
                                    }
                                
                                }
                            
                            }
                        
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 1122;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (ENABLERECENTCALLS  .Value == 1) ) && Functions.TestForTrue ( Functions.BoolToInt (NEXTFOLDERENTRY == 0) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt (NEXTAVAILABLEDIRENTRY == (PHONEBOOKENTRY[ NEXTFOLDERENTRY ].FIRSTCHILD + PHONEBOOKLEVELTOTALROWS)) )) ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 1124;
                            CancelWait ( "RECEIVEPHONEBOOK" ) ; 
                            __context__.SourceCodeLine = 1125;
                            PHONEBOOKPARSEFAILURE = (ushort) ( 0 ) ; 
                            __context__.SourceCodeLine = 1126;
                            RECEIVINGPHONEBOOK = (ushort) ( 0 ) ; 
                            __context__.SourceCodeLine = 1127;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( NEXTAVAILABLEDIRENTRY >= 2000 ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 1129;
                                SELECTEDFOLDER = (ushort) ( 0 ) ; 
                                __context__.SourceCodeLine = 1130;
                                DIRECTORYONTOPLEVEL  .Value = (ushort) ( 1 ) ; 
                                __context__.SourceCodeLine = 1131;
                                GenerateUserNotice ( "Directory Download Finished. Found Max Entries. (Entries: {0:d})", (short)NEXTAVAILABLEDIRENTRY) ; 
                                __context__.SourceCodeLine = 1132;
                                ICURRENTPAGE = (ushort) ( 1 ) ; 
                                __context__.SourceCodeLine = 1133;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Mod( PHONEBOOKENTRY[ SELECTEDFOLDER ].CHILDCOUNT , IENTRIESPERPAGE ) > 0 ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 1135;
                                    ITOTALPAGES = (ushort) ( ((PHONEBOOKENTRY[ SELECTEDFOLDER ].CHILDCOUNT / IENTRIESPERPAGE) + 1) ) ; 
                                    } 
                                
                                else 
                                    { 
                                    __context__.SourceCodeLine = 1139;
                                    ITOTALPAGES = (ushort) ( (PHONEBOOKENTRY[ SELECTEDFOLDER ].CHILDCOUNT / IENTRIESPERPAGE) ) ; 
                                    } 
                                
                                __context__.SourceCodeLine = 1141;
                                PHONEBOOKPAGESCURRENTPAGE  .Value = (ushort) ( ICURRENTPAGE ) ; 
                                __context__.SourceCodeLine = 1142;
                                PHONEBOOKPAGESTOTALPAGES  .Value = (ushort) ( ITOTALPAGES ) ; 
                                __context__.SourceCodeLine = 1143;
                                FDISPLAYNEWPAGE (  __context__  ) ; 
                                __context__.SourceCodeLine = 1144;
                                DISPLAYPHONEBOOK (  __context__  ) ; 
                                __context__.SourceCodeLine = 1145;
                                DIRECTORYDOWNLOADBUSY  .Value = (ushort) ( 0 ) ; 
                                } 
                            
                            else 
                                {
                                __context__.SourceCodeLine = 1147;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( NEXTAVAILABLEDIRENTRY < ((PHONEBOOKENTRY[ NEXTFOLDERENTRY ].FIRSTCHILD + PHONEBOOKLEVELTOTALROWS) - 1) ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 1149;
                                    RECEIVINGPHONEBOOK = (ushort) ( 0 ) ; 
                                    __context__.SourceCodeLine = 1150;
                                    INTERNALENTRYOFFSET = (ushort) ( (INTERNALENTRYOFFSET + 1) ) ; 
                                    __context__.SourceCodeLine = 1151;
                                    RUNSEARCHPHONEBOOK (  __context__  ) ; 
                                    } 
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 1153;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( NEXTAVAILABLEDIRENTRY >= ((PHONEBOOKENTRY[ NEXTFOLDERENTRY ].FIRSTCHILD + PHONEBOOKLEVELTOTALROWS) - 1) ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 1155;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (PHONEBOOKLEVELTOTALROWS == 0) ) && Functions.TestForTrue ( Functions.BoolToInt (INTERNALENTRYOFFSET == 0) )) ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 1157;
                                            PHONEBOOKENTRY [ NEXTFOLDERENTRY] . FIRSTCHILD = (ushort) ( (2000 + 1) ) ; 
                                            } 
                                        
                                        __context__.SourceCodeLine = 1159;
                                        INTERNALENTRYOFFSET = (ushort) ( 0 ) ; 
                                        __context__.SourceCodeLine = 1160;
                                        do 
                                            { 
                                            __context__.SourceCodeLine = 1162;
                                            NEXTFOLDERENTRY = (ushort) ( (NEXTFOLDERENTRY + 1) ) ; 
                                            } 
                                        while (false == ( Functions.TestForTrue  ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (NEXTFOLDERENTRY == 2000) ) || Functions.TestForTrue ( Functions.BoolToInt ( Functions.Length( PHONEBOOKENTRY[ NEXTFOLDERENTRY ].FOLDERID ) > 0 ) )) )) )); 
                                        __context__.SourceCodeLine = 1165;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( NEXTFOLDERENTRY < 2000 ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 1167;
                                            RECEIVINGPHONEBOOK = (ushort) ( 0 ) ; 
                                            __context__.SourceCodeLine = 1168;
                                            PHONEBOOKENTRY [ NEXTFOLDERENTRY] . FIRSTCHILD = (ushort) ( (NEXTAVAILABLEDIRENTRY + 1) ) ; 
                                            __context__.SourceCodeLine = 1169;
                                            RUNSEARCHPHONEBOOK (  __context__  ) ; 
                                            } 
                                        
                                        else 
                                            {
                                            __context__.SourceCodeLine = 1171;
                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( NEXTFOLDERENTRY >= 2000 ))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 1173;
                                                SELECTEDFOLDER = (ushort) ( 0 ) ; 
                                                __context__.SourceCodeLine = 1174;
                                                DIRECTORYONTOPLEVEL  .Value = (ushort) ( 1 ) ; 
                                                __context__.SourceCodeLine = 1175;
                                                GenerateUserNotice ( "Directory Download Finished. (Entries: {0:d})", (short)NEXTAVAILABLEDIRENTRY) ; 
                                                __context__.SourceCodeLine = 1176;
                                                ICURRENTPAGE = (ushort) ( 1 ) ; 
                                                __context__.SourceCodeLine = 1177;
                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Mod( PHONEBOOKENTRY[ SELECTEDFOLDER ].CHILDCOUNT , IENTRIESPERPAGE ) > 0 ))  ) ) 
                                                    { 
                                                    __context__.SourceCodeLine = 1179;
                                                    ITOTALPAGES = (ushort) ( ((PHONEBOOKENTRY[ SELECTEDFOLDER ].CHILDCOUNT / IENTRIESPERPAGE) + 1) ) ; 
                                                    } 
                                                
                                                else 
                                                    { 
                                                    __context__.SourceCodeLine = 1183;
                                                    ITOTALPAGES = (ushort) ( (PHONEBOOKENTRY[ SELECTEDFOLDER ].CHILDCOUNT / IENTRIESPERPAGE) ) ; 
                                                    } 
                                                
                                                __context__.SourceCodeLine = 1185;
                                                PHONEBOOKPAGESCURRENTPAGE  .Value = (ushort) ( ICURRENTPAGE ) ; 
                                                __context__.SourceCodeLine = 1186;
                                                PHONEBOOKPAGESTOTALPAGES  .Value = (ushort) ( ITOTALPAGES ) ; 
                                                __context__.SourceCodeLine = 1187;
                                                FDISPLAYNEWPAGE (  __context__  ) ; 
                                                __context__.SourceCodeLine = 1188;
                                                DISPLAYPHONEBOOK (  __context__  ) ; 
                                                __context__.SourceCodeLine = 1189;
                                                DIRECTORYDOWNLOADBUSY  .Value = (ushort) ( 0 ) ; 
                                                } 
                                            
                                            }
                                        
                                        } 
                                    
                                    }
                                
                                }
                            
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 1193;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (ENABLERECENTCALLS  .Value == 1) ) && Functions.TestForTrue ( Functions.BoolToInt (NEXTAVAILABLEDIRENTRY == ((PHONEBOOKENTRY[ NEXTFOLDERENTRY ].FIRSTCHILD + (20 * INTERNALENTRYOFFSET)) + 20)) )) ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 1195;
                                CancelWait ( "RECEIVEPHONEBOOK" ) ; 
                                __context__.SourceCodeLine = 1196;
                                PHONEBOOKPARSEFAILURE = (ushort) ( 0 ) ; 
                                __context__.SourceCodeLine = 1197;
                                RECEIVINGPHONEBOOK = (ushort) ( 0 ) ; 
                                __context__.SourceCodeLine = 1198;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( NEXTAVAILABLEDIRENTRY >= 2000 ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 1200;
                                    SELECTEDFOLDER = (ushort) ( 0 ) ; 
                                    __context__.SourceCodeLine = 1201;
                                    DIRECTORYONTOPLEVEL  .Value = (ushort) ( 1 ) ; 
                                    __context__.SourceCodeLine = 1202;
                                    GenerateUserNotice ( "Directory Download Finished. Found Max Entries. (Entries: {0:d})", (short)NEXTAVAILABLEDIRENTRY) ; 
                                    __context__.SourceCodeLine = 1203;
                                    ICURRENTPAGE = (ushort) ( 1 ) ; 
                                    __context__.SourceCodeLine = 1204;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Mod( PHONEBOOKENTRY[ SELECTEDFOLDER ].CHILDCOUNT , IENTRIESPERPAGE ) > 0 ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 1206;
                                        ITOTALPAGES = (ushort) ( ((PHONEBOOKENTRY[ SELECTEDFOLDER ].CHILDCOUNT / IENTRIESPERPAGE) + 1) ) ; 
                                        } 
                                    
                                    else 
                                        { 
                                        __context__.SourceCodeLine = 1210;
                                        ITOTALPAGES = (ushort) ( (PHONEBOOKENTRY[ SELECTEDFOLDER ].CHILDCOUNT / IENTRIESPERPAGE) ) ; 
                                        } 
                                    
                                    __context__.SourceCodeLine = 1212;
                                    PHONEBOOKPAGESCURRENTPAGE  .Value = (ushort) ( ICURRENTPAGE ) ; 
                                    __context__.SourceCodeLine = 1213;
                                    PHONEBOOKPAGESTOTALPAGES  .Value = (ushort) ( ITOTALPAGES ) ; 
                                    __context__.SourceCodeLine = 1214;
                                    FDISPLAYNEWPAGE (  __context__  ) ; 
                                    __context__.SourceCodeLine = 1215;
                                    DISPLAYPHONEBOOK (  __context__  ) ; 
                                    __context__.SourceCodeLine = 1216;
                                    DIRECTORYDOWNLOADBUSY  .Value = (ushort) ( 0 ) ; 
                                    } 
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 1218;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( NEXTAVAILABLEDIRENTRY < ((PHONEBOOKENTRY[ NEXTFOLDERENTRY ].FIRSTCHILD + PHONEBOOKLEVELTOTALROWS) - 1) ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 1220;
                                        RECEIVINGPHONEBOOK = (ushort) ( 0 ) ; 
                                        __context__.SourceCodeLine = 1221;
                                        INTERNALENTRYOFFSET = (ushort) ( (INTERNALENTRYOFFSET + 1) ) ; 
                                        __context__.SourceCodeLine = 1222;
                                        RUNSEARCHPHONEBOOK (  __context__  ) ; 
                                        } 
                                    
                                    else 
                                        {
                                        __context__.SourceCodeLine = 1224;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( NEXTAVAILABLEDIRENTRY >= ((PHONEBOOKENTRY[ NEXTFOLDERENTRY ].FIRSTCHILD + PHONEBOOKLEVELTOTALROWS) - 1) ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 1226;
                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (PHONEBOOKLEVELTOTALROWS == 0) ) && Functions.TestForTrue ( Functions.BoolToInt (INTERNALENTRYOFFSET == 0) )) ))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 1228;
                                                PHONEBOOKENTRY [ NEXTFOLDERENTRY] . FIRSTCHILD = (ushort) ( (2000 + 1) ) ; 
                                                } 
                                            
                                            __context__.SourceCodeLine = 1230;
                                            INTERNALENTRYOFFSET = (ushort) ( 0 ) ; 
                                            __context__.SourceCodeLine = 1231;
                                            do 
                                                { 
                                                __context__.SourceCodeLine = 1233;
                                                NEXTFOLDERENTRY = (ushort) ( (NEXTFOLDERENTRY + 1) ) ; 
                                                } 
                                            while (false == ( Functions.TestForTrue  ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (NEXTFOLDERENTRY == 2000) ) || Functions.TestForTrue ( Functions.BoolToInt ( Functions.Length( PHONEBOOKENTRY[ NEXTFOLDERENTRY ].FOLDERID ) > 0 ) )) )) )); 
                                            __context__.SourceCodeLine = 1236;
                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( NEXTFOLDERENTRY < 2000 ))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 1238;
                                                RECEIVINGPHONEBOOK = (ushort) ( 0 ) ; 
                                                __context__.SourceCodeLine = 1239;
                                                PHONEBOOKENTRY [ NEXTFOLDERENTRY] . FIRSTCHILD = (ushort) ( (NEXTAVAILABLEDIRENTRY + 1) ) ; 
                                                __context__.SourceCodeLine = 1240;
                                                RUNSEARCHPHONEBOOK (  __context__  ) ; 
                                                } 
                                            
                                            else 
                                                {
                                                __context__.SourceCodeLine = 1242;
                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( NEXTFOLDERENTRY >= 2000 ))  ) ) 
                                                    { 
                                                    __context__.SourceCodeLine = 1244;
                                                    SELECTEDFOLDER = (ushort) ( 0 ) ; 
                                                    __context__.SourceCodeLine = 1245;
                                                    DIRECTORYONTOPLEVEL  .Value = (ushort) ( 1 ) ; 
                                                    __context__.SourceCodeLine = 1246;
                                                    GenerateUserNotice ( "Directory Download Finished. (Entries: {0:d})", (short)NEXTAVAILABLEDIRENTRY) ; 
                                                    __context__.SourceCodeLine = 1247;
                                                    ICURRENTPAGE = (ushort) ( 1 ) ; 
                                                    __context__.SourceCodeLine = 1248;
                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Mod( PHONEBOOKENTRY[ SELECTEDFOLDER ].CHILDCOUNT , IENTRIESPERPAGE ) > 0 ))  ) ) 
                                                        { 
                                                        __context__.SourceCodeLine = 1250;
                                                        ITOTALPAGES = (ushort) ( ((PHONEBOOKENTRY[ SELECTEDFOLDER ].CHILDCOUNT / IENTRIESPERPAGE) + 1) ) ; 
                                                        } 
                                                    
                                                    else 
                                                        { 
                                                        __context__.SourceCodeLine = 1254;
                                                        ITOTALPAGES = (ushort) ( (PHONEBOOKENTRY[ SELECTEDFOLDER ].CHILDCOUNT / IENTRIESPERPAGE) ) ; 
                                                        } 
                                                    
                                                    __context__.SourceCodeLine = 1256;
                                                    PHONEBOOKPAGESCURRENTPAGE  .Value = (ushort) ( ICURRENTPAGE ) ; 
                                                    __context__.SourceCodeLine = 1257;
                                                    PHONEBOOKPAGESTOTALPAGES  .Value = (ushort) ( ITOTALPAGES ) ; 
                                                    __context__.SourceCodeLine = 1258;
                                                    FDISPLAYNEWPAGE (  __context__  ) ; 
                                                    __context__.SourceCodeLine = 1259;
                                                    DISPLAYPHONEBOOK (  __context__  ) ; 
                                                    __context__.SourceCodeLine = 1260;
                                                    DIRECTORYDOWNLOADBUSY  .Value = (ushort) ( 0 ) ; 
                                                    } 
                                                
                                                }
                                            
                                            } 
                                        
                                        }
                                    
                                    }
                                
                                } 
                            
                            else 
                                {
                                __context__.SourceCodeLine = 1264;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (ENABLERECENTCALLS  .Value == 1) ) && Functions.TestForTrue ( Functions.BoolToInt ( NEXTAVAILABLEDIRENTRY <= (PHONEBOOKENTRY[ 1 ].FIRSTCHILD + 40) ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( NEXTAVAILABLEDIRENTRY >= PHONEBOOKENTRY[ 1 ].FIRSTCHILD ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( NEXTFOLDERENTRY < 2000 ) )) ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 1266;
                                    CancelWait ( "RECEIVEPHONEBOOK" ) ; 
                                    __context__.SourceCodeLine = 1267;
                                    NEXTAVAILABLEDIRENTRY = (ushort) ( ((PHONEBOOKENTRY[ 1 ].FIRSTCHILD + 40) - 1) ) ; 
                                    __context__.SourceCodeLine = 1268;
                                    INTERNALENTRYOFFSET = (ushort) ( 0 ) ; 
                                    __context__.SourceCodeLine = 1269;
                                    do 
                                        { 
                                        __context__.SourceCodeLine = 1271;
                                        NEXTFOLDERENTRY = (ushort) ( (NEXTFOLDERENTRY + 1) ) ; 
                                        } 
                                    while (false == ( Functions.TestForTrue  ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (NEXTFOLDERENTRY == 2000) ) || Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( Functions.Length( PHONEBOOKENTRY[ NEXTFOLDERENTRY ].FOLDERID ) > 0 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( NEXTFOLDERENTRY < 2000 ) )) ) )) )) )); 
                                    __context__.SourceCodeLine = 1274;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( NEXTFOLDERENTRY < 2000 ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 1276;
                                        RECEIVINGPHONEBOOK = (ushort) ( 0 ) ; 
                                        __context__.SourceCodeLine = 1277;
                                        PHONEBOOKENTRY [ NEXTFOLDERENTRY] . FIRSTCHILD = (ushort) ( (NEXTAVAILABLEDIRENTRY + 1) ) ; 
                                        __context__.SourceCodeLine = 1278;
                                        RUNSEARCHPHONEBOOK (  __context__  ) ; 
                                        } 
                                    
                                    else 
                                        {
                                        __context__.SourceCodeLine = 1280;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( NEXTFOLDERENTRY >= 2000 ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 1282;
                                            SELECTEDFOLDER = (ushort) ( 0 ) ; 
                                            __context__.SourceCodeLine = 1283;
                                            DIRECTORYONTOPLEVEL  .Value = (ushort) ( 1 ) ; 
                                            __context__.SourceCodeLine = 1284;
                                            GenerateUserNotice ( "Directory Download Finished. (Entries: {0:d})", (short)NEXTAVAILABLEDIRENTRY) ; 
                                            __context__.SourceCodeLine = 1285;
                                            ICURRENTPAGE = (ushort) ( 1 ) ; 
                                            __context__.SourceCodeLine = 1286;
                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Mod( PHONEBOOKENTRY[ SELECTEDFOLDER ].CHILDCOUNT , IENTRIESPERPAGE ) > 0 ))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 1288;
                                                ITOTALPAGES = (ushort) ( ((PHONEBOOKENTRY[ SELECTEDFOLDER ].CHILDCOUNT / IENTRIESPERPAGE) + 1) ) ; 
                                                } 
                                            
                                            else 
                                                { 
                                                __context__.SourceCodeLine = 1292;
                                                ITOTALPAGES = (ushort) ( (PHONEBOOKENTRY[ SELECTEDFOLDER ].CHILDCOUNT / IENTRIESPERPAGE) ) ; 
                                                } 
                                            
                                            __context__.SourceCodeLine = 1294;
                                            PHONEBOOKPAGESCURRENTPAGE  .Value = (ushort) ( ICURRENTPAGE ) ; 
                                            __context__.SourceCodeLine = 1295;
                                            PHONEBOOKPAGESTOTALPAGES  .Value = (ushort) ( ITOTALPAGES ) ; 
                                            __context__.SourceCodeLine = 1296;
                                            FDISPLAYNEWPAGE (  __context__  ) ; 
                                            __context__.SourceCodeLine = 1297;
                                            DISPLAYPHONEBOOK (  __context__  ) ; 
                                            __context__.SourceCodeLine = 1298;
                                            DIRECTORYDOWNLOADBUSY  .Value = (ushort) ( 0 ) ; 
                                            } 
                                        
                                        }
                                    
                                    } 
                                
                                }
                            
                            }
                        
                        }
                    
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 1302;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (RECEIVINGPHONEBOOK == 1) ) && Functions.TestForTrue ( Functions.BoolToInt (BGETRECENTSONLY == 1) )) ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 1304;
                        CancelWait ( "RECEIVEPHONEBOOK" ) ; 
                        __context__.SourceCodeLine = 1305;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (INTERNALENTRYOFFSET == 0) ) && Functions.TestForTrue ( Functions.BoolToInt (PHONEBOOKENTRY[ 1 ].CHILDCOUNT == 20) )) ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 1307;
                            INTERNALENTRYOFFSET = (ushort) ( (INTERNALENTRYOFFSET + 1) ) ; 
                            __context__.SourceCodeLine = 1308;
                            RECEIVINGPHONEBOOK = (ushort) ( 0 ) ; 
                            __context__.SourceCodeLine = 1309;
                            RUNSEARCHPHONEBOOK (  __context__  ) ; 
                            } 
                        
                        else 
                            { 
                            __context__.SourceCodeLine = 1313;
                            RECEIVINGPHONEBOOK = (ushort) ( 0 ) ; 
                            __context__.SourceCodeLine = 1314;
                            BGETRECENTSONLY = (ushort) ( 0 ) ; 
                            __context__.SourceCodeLine = 1315;
                            RECENTCALLSDOWNLOADBUSY  .Value = (ushort) ( BGETRECENTSONLY ) ; 
                            __context__.SourceCodeLine = 1316;
                            SELECTEDFOLDER = (ushort) ( 0 ) ; 
                            __context__.SourceCodeLine = 1317;
                            DIRECTORYONTOPLEVEL  .Value = (ushort) ( 1 ) ; 
                            __context__.SourceCodeLine = 1318;
                            GenerateUserNotice ( "Directory Download Finished. (Entries: {0:d})", (short)NEXTAVAILABLEDIRENTRY) ; 
                            __context__.SourceCodeLine = 1319;
                            ICURRENTPAGE = (ushort) ( 1 ) ; 
                            __context__.SourceCodeLine = 1320;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Mod( PHONEBOOKENTRY[ SELECTEDFOLDER ].CHILDCOUNT , IENTRIESPERPAGE ) > 0 ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 1322;
                                ITOTALPAGES = (ushort) ( ((PHONEBOOKENTRY[ SELECTEDFOLDER ].CHILDCOUNT / IENTRIESPERPAGE) + 1) ) ; 
                                } 
                            
                            else 
                                { 
                                __context__.SourceCodeLine = 1326;
                                ITOTALPAGES = (ushort) ( (PHONEBOOKENTRY[ SELECTEDFOLDER ].CHILDCOUNT / IENTRIESPERPAGE) ) ; 
                                } 
                            
                            __context__.SourceCodeLine = 1328;
                            PHONEBOOKPAGESCURRENTPAGE  .Value = (ushort) ( ICURRENTPAGE ) ; 
                            __context__.SourceCodeLine = 1329;
                            PHONEBOOKPAGESTOTALPAGES  .Value = (ushort) ( ITOTALPAGES ) ; 
                            __context__.SourceCodeLine = 1330;
                            FDISPLAYNEWPAGE (  __context__  ) ; 
                            __context__.SourceCodeLine = 1331;
                            DISPLAYPHONEBOOK (  __context__  ) ; 
                            __context__.SourceCodeLine = 1332;
                            DIRECTORYDOWNLOADBUSY  .Value = (ushort) ( 0 ) ; 
                            } 
                        
                        } 
                    
                    }
                
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 1338;
                GTEMPSEGMENT  .UpdateValue ( Functions.Remove ( "\u0020" , TEMPSTRING__DOLLAR__ )  ) ; 
                __context__.SourceCodeLine = 1340;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "*s "))  ) ) 
                    { 
                    __context__.SourceCodeLine = 1342;
                    GTEMPSEGMENT  .UpdateValue ( Functions.Remove ( "\u0020" , TEMPSTRING__DOLLAR__ )  ) ; 
                    __context__.SourceCodeLine = 1344;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (GTEMPSEGMENT == "Audio ") ) || Functions.TestForTrue ( Functions.BoolToInt (GTEMPSEGMENT == "Video ") )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (GTEMPSEGMENT == "Preset ") )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (GTEMPSEGMENT == "Standby ") )) ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 1346;
                        FROM_DEVICE_STATUSCHANGES  .UpdateValue ( GTEMPSEGMENT + TEMPSTRING__DOLLAR__  ) ; 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 1348;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "Call "))  ) ) 
                            { 
                            __context__.SourceCodeLine = 1350;
                            PARSECALLFEEDBACK (  __context__  ) ; 
                            __context__.SourceCodeLine = 1351;
                            VALIDRESPONSEFOUND (  __context__  ) ; 
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 1353;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "Conference "))  ) ) 
                                { 
                                __context__.SourceCodeLine = 1355;
                                VALIDRESPONSEFOUND (  __context__  ) ; 
                                __context__.SourceCodeLine = 1356;
                                FROM_DEVICE_STATUSCHANGES  .UpdateValue ( GTEMPSEGMENT + TEMPSTRING__DOLLAR__  ) ; 
                                __context__.SourceCodeLine = 1357;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.Left( TEMPSTRING__DOLLAR__ , (int)( 4 ) ) == "Site"))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 1359;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Find( "Capabilities Presentation:" , TEMPSTRING__DOLLAR__ ) > 0 ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 1361;
                                        CALLID = (ushort) ( Functions.Atoi( TEMPSTRING__DOLLAR__ ) ) ; 
                                        __context__.SourceCodeLine = 1362;
                                        CALLSLOT = (ushort) ( GETCALLSLOTFROMCALLID( __context__ , (ushort)( CALLID ) ) ) ; 
                                        __context__.SourceCodeLine = 1363;
                                        GTEMPVALUE  .UpdateValue ( GETSTRINGTOKENVALUE (  __context__ , TEMPSTRING__DOLLAR__, "Presentation:")  ) ; 
                                        __context__.SourceCodeLine = 1364;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPVALUE == "True"))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 1366;
                                            CALLSTATUS_SUPPORTSPRESENTATION [ CALLSLOT]  .Value = (ushort) ( 1 ) ; 
                                            } 
                                        
                                        else 
                                            { 
                                            __context__.SourceCodeLine = 1370;
                                            CALLSTATUS_SUPPORTSPRESENTATION [ CALLSLOT]  .Value = (ushort) ( 0 ) ; 
                                            } 
                                        
                                        } 
                                    
                                    } 
                                
                                } 
                            
                            else 
                                {
                                __context__.SourceCodeLine = 1375;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "SystemUnit "))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 1377;
                                    VALIDRESPONSEFOUND (  __context__  ) ; 
                                    __context__.SourceCodeLine = 1378;
                                    GTEMPSEGMENT  .UpdateValue ( Functions.Remove ( "\u0020" , TEMPSTRING__DOLLAR__ )  ) ; 
                                    __context__.SourceCodeLine = 1379;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "State "))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 1381;
                                        GTEMPSEGMENT  .UpdateValue ( Functions.Remove ( "\u0020" , TEMPSTRING__DOLLAR__ )  ) ; 
                                        __context__.SourceCodeLine = 1383;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "NumberOfInProgressCalls: "))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 1385;
                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (TEMPSTRING__DOLLAR__ == "0\u000D\u000A"))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 1387;
                                                SIGNALINCOMINGCALL  .Value = (ushort) ( 0 ) ; 
                                                __context__.SourceCodeLine = 1388;
                                                INCOMINGCALLNUMBER  .UpdateValue ( ""  ) ; 
                                                } 
                                            
                                            } 
                                        
                                        else 
                                            {
                                            __context__.SourceCodeLine = 1391;
                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "NumberOfActiveCalls: "))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 1393;
                                                NUMBEROFACTIVECALLS  .Value = (ushort) ( Functions.Atoi( TEMPSTRING__DOLLAR__ ) ) ; 
                                                } 
                                            
                                            }
                                        
                                        } 
                                    
                                    else 
                                        {
                                        __context__.SourceCodeLine = 1396;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "Software "))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 1398;
                                            GTEMPSEGMENT  .UpdateValue ( Functions.Remove ( "\u0020" , TEMPSTRING__DOLLAR__ )  ) ; 
                                            __context__.SourceCodeLine = 1400;
                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "Version: "))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 1402;
                                                GETSYSTEMSOFTWAREVERSION  .UpdateValue ( Functions.Left ( TEMPSTRING__DOLLAR__ ,  (int) ( (Functions.Length( TEMPSTRING__DOLLAR__ ) - 2) ) )  ) ; 
                                                } 
                                            
                                            } 
                                        
                                        }
                                    
                                    } 
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 1406;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "H323 "))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 1408;
                                        VALIDRESPONSEFOUND (  __context__  ) ; 
                                        __context__.SourceCodeLine = 1409;
                                        GTEMPSEGMENT  .UpdateValue ( Functions.Remove ( "\u0020" , TEMPSTRING__DOLLAR__ )  ) ; 
                                        __context__.SourceCodeLine = 1411;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "GateKeeper "))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 1413;
                                            GTEMPSEGMENT  .UpdateValue ( Functions.Remove ( "\u0020" , TEMPSTRING__DOLLAR__ )  ) ; 
                                            __context__.SourceCodeLine = 1415;
                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "Status: "))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 1417;
                                                GETGATEKEEPERSTATUS  .UpdateValue ( Functions.Left ( TEMPSTRING__DOLLAR__ ,  (int) ( (Functions.Length( TEMPSTRING__DOLLAR__ ) - 2) ) )  ) ; 
                                                } 
                                            
                                            else 
                                                {
                                                __context__.SourceCodeLine = 1419;
                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "Address: "))  ) ) 
                                                    { 
                                                    __context__.SourceCodeLine = 1421;
                                                    GETGATEKEEPERNETWORKADDRESS  .UpdateValue ( Functions.Left ( TEMPSTRING__DOLLAR__ ,  (int) ( (Functions.Length( TEMPSTRING__DOLLAR__ ) - 2) ) )  ) ; 
                                                    } 
                                                
                                                }
                                            
                                            } 
                                        
                                        } 
                                    
                                    else 
                                        {
                                        __context__.SourceCodeLine = 1425;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "Network "))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 1427;
                                            VALIDRESPONSEFOUND (  __context__  ) ; 
                                            __context__.SourceCodeLine = 1428;
                                            GTEMPSEGMENT  .UpdateValue ( Functions.Remove ( "\u0020" , TEMPSTRING__DOLLAR__ )  ) ; 
                                            __context__.SourceCodeLine = 1430;
                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "1 "))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 1432;
                                                GTEMPSEGMENT  .UpdateValue ( Functions.Remove ( "\u0020" , TEMPSTRING__DOLLAR__ )  ) ; 
                                                __context__.SourceCodeLine = 1434;
                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "IPv4 "))  ) ) 
                                                    { 
                                                    __context__.SourceCodeLine = 1436;
                                                    GTEMPSEGMENT  .UpdateValue ( Functions.Remove ( "\u0020" , TEMPSTRING__DOLLAR__ )  ) ; 
                                                    __context__.SourceCodeLine = 1438;
                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "Address: "))  ) ) 
                                                        { 
                                                        __context__.SourceCodeLine = 1440;
                                                        GETSYSTEMNETWORKADDRESS  .UpdateValue ( Functions.Left ( TEMPSTRING__DOLLAR__ ,  (int) ( (Functions.Length( TEMPSTRING__DOLLAR__ ) - 2) ) )  ) ; 
                                                        } 
                                                    
                                                    } 
                                                
                                                } 
                                            
                                            } 
                                        
                                        }
                                    
                                    }
                                
                                }
                            
                            }
                        
                        }
                    
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 1446;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "*e "))  ) ) 
                        { 
                        __context__.SourceCodeLine = 1448;
                        GTEMPSEGMENT  .UpdateValue ( Functions.Remove ( "\u0020" , TEMPSTRING__DOLLAR__ )  ) ; 
                        __context__.SourceCodeLine = 1450;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "IncomingCallIndication "))  ) ) 
                            { 
                            __context__.SourceCodeLine = 1452;
                            VALIDRESPONSEFOUND (  __context__  ) ; 
                            __context__.SourceCodeLine = 1453;
                            INCOMINGCALLNUMBER  .UpdateValue ( GETSTRINGTOKENVALUE (  __context__ , TEMPSTRING__DOLLAR__, "DisplayNameValue:")  ) ; 
                            __context__.SourceCodeLine = 1454;
                            SIGNALINCOMINGCALL  .Value = (ushort) ( 1 ) ; 
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 1456;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "SString "))  ) ) 
                                { 
                                __context__.SourceCodeLine = 1458;
                                VALIDRESPONSEFOUND (  __context__  ) ; 
                                __context__.SourceCodeLine = 1459;
                                SSTRING_RECEIVE_TEXT  .UpdateValue ( GETTAGVALUEREMOVEAMPERSANDS (  __context__ ,   ref  TEMPSTRING__DOLLAR__ )  ) ; 
                                } 
                            
                            else 
                                {
                                __context__.SourceCodeLine = 1461;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "TString: "))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 1463;
                                    VALIDRESPONSEFOUND (  __context__  ) ; 
                                    __context__.SourceCodeLine = 1464;
                                    TSTRING_RECEIVE_TEXT  .UpdateValue ( GETTAGVALUEREMOVEAMPERSANDS (  __context__ ,   ref  TEMPSTRING__DOLLAR__ )  ) ; 
                                    } 
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 1466;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "Message "))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 1468;
                                        VALIDRESPONSEFOUND (  __context__  ) ; 
                                        __context__.SourceCodeLine = 1469;
                                        GTEMPSEGMENT  .UpdateValue ( Functions.Remove ( "\u0020" , TEMPSTRING__DOLLAR__ )  ) ; 
                                        __context__.SourceCodeLine = 1471;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "Prompt "))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 1473;
                                            GTEMPSEGMENT  .UpdateValue ( Functions.Remove ( "\u0020" , TEMPSTRING__DOLLAR__ )  ) ; 
                                            __context__.SourceCodeLine = 1475;
                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (Functions.Length( GTEMPSEGMENT ) == 0) ) && Functions.TestForTrue ( Functions.BoolToInt ( Functions.Find( "Clear" , TEMPSTRING__DOLLAR__ ) > 0 ) )) ))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 1477;
                                                RESETMESSAGEPROMPT (  __context__  ) ; 
                                                } 
                                            
                                            else 
                                                {
                                                __context__.SourceCodeLine = 1479;
                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "Display "))  ) ) 
                                                    { 
                                                    __context__.SourceCodeLine = 1481;
                                                    GTEMPSEGMENT  .UpdateValue ( Functions.Remove ( "\u0020" , TEMPSTRING__DOLLAR__ )  ) ; 
                                                    __context__.SourceCodeLine = 1482;
                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "Title: "))  ) ) 
                                                        { 
                                                        __context__.SourceCodeLine = 1484;
                                                        MESSAGE_PROMPT_TITLE  .UpdateValue ( GETTAGVALUEREMOVEAMPERSANDS (  __context__ ,   ref  TEMPSTRING__DOLLAR__ )  ) ; 
                                                        } 
                                                    
                                                    else 
                                                        {
                                                        __context__.SourceCodeLine = 1486;
                                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "Text: "))  ) ) 
                                                            { 
                                                            __context__.SourceCodeLine = 1488;
                                                            MESSAGE_PROMPT_TITLE  .UpdateValue ( GETTAGVALUEREMOVEAMPERSANDS (  __context__ ,   ref  TEMPSTRING__DOLLAR__ )  ) ; 
                                                            __context__.SourceCodeLine = 1489;
                                                            MESSAGE_PROMPT_ISACTIVE  .Value = (ushort) ( 1 ) ; 
                                                            } 
                                                        
                                                        else 
                                                            {
                                                            __context__.SourceCodeLine = 1491;
                                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "FeedbackID: "))  ) ) 
                                                                { 
                                                                __context__.SourceCodeLine = 1493;
                                                                MESSAGE_PROMPT_FEEDBACKID  .UpdateValue ( GETTAGVALUEREMOVEAMPERSANDS (  __context__ ,   ref  TEMPSTRING__DOLLAR__ )  ) ; 
                                                                } 
                                                            
                                                            else 
                                                                {
                                                                __context__.SourceCodeLine = 1495;
                                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "Option.1: "))  ) ) 
                                                                    { 
                                                                    __context__.SourceCodeLine = 1497;
                                                                    MESSAGE_PROMPT_OPTION1_TEXT  .UpdateValue ( GETTAGVALUEREMOVEAMPERSANDS (  __context__ ,   ref  TEMPSTRING__DOLLAR__ )  ) ; 
                                                                    __context__.SourceCodeLine = 1498;
                                                                    MESSAGE_PROMPT_OPTION1_ISACTIVE  .Value = (ushort) ( 1 ) ; 
                                                                    } 
                                                                
                                                                else 
                                                                    {
                                                                    __context__.SourceCodeLine = 1500;
                                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "Option.2: "))  ) ) 
                                                                        { 
                                                                        __context__.SourceCodeLine = 1502;
                                                                        MESSAGE_PROMPT_OPTION2_TEXT  .UpdateValue ( GETTAGVALUEREMOVEAMPERSANDS (  __context__ ,   ref  TEMPSTRING__DOLLAR__ )  ) ; 
                                                                        __context__.SourceCodeLine = 1503;
                                                                        MESSAGE_PROMPT_OPTION2_ISACTIVE  .Value = (ushort) ( 1 ) ; 
                                                                        } 
                                                                    
                                                                    else 
                                                                        {
                                                                        __context__.SourceCodeLine = 1505;
                                                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "Option.3: "))  ) ) 
                                                                            { 
                                                                            __context__.SourceCodeLine = 1507;
                                                                            MESSAGE_PROMPT_OPTION3_TEXT  .UpdateValue ( GETTAGVALUEREMOVEAMPERSANDS (  __context__ ,   ref  TEMPSTRING__DOLLAR__ )  ) ; 
                                                                            __context__.SourceCodeLine = 1508;
                                                                            MESSAGE_PROMPT_OPTION3_ISACTIVE  .Value = (ushort) ( 1 ) ; 
                                                                            } 
                                                                        
                                                                        else 
                                                                            {
                                                                            __context__.SourceCodeLine = 1510;
                                                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "Option.4: "))  ) ) 
                                                                                { 
                                                                                __context__.SourceCodeLine = 1512;
                                                                                MESSAGE_PROMPT_OPTION4_TEXT  .UpdateValue ( GETTAGVALUEREMOVEAMPERSANDS (  __context__ ,   ref  TEMPSTRING__DOLLAR__ )  ) ; 
                                                                                __context__.SourceCodeLine = 1513;
                                                                                MESSAGE_PROMPT_OPTION4_ISACTIVE  .Value = (ushort) ( 1 ) ; 
                                                                                } 
                                                                            
                                                                            else 
                                                                                {
                                                                                __context__.SourceCodeLine = 1515;
                                                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "Option.5: "))  ) ) 
                                                                                    { 
                                                                                    __context__.SourceCodeLine = 1517;
                                                                                    MESSAGE_PROMPT_OPTION5_TEXT  .UpdateValue ( GETTAGVALUEREMOVEAMPERSANDS (  __context__ ,   ref  TEMPSTRING__DOLLAR__ )  ) ; 
                                                                                    __context__.SourceCodeLine = 1518;
                                                                                    MESSAGE_PROMPT_OPTION5_ISACTIVE  .Value = (ushort) ( 1 ) ; 
                                                                                    } 
                                                                                
                                                                                }
                                                                            
                                                                            }
                                                                        
                                                                        }
                                                                    
                                                                    }
                                                                
                                                                }
                                                            
                                                            }
                                                        
                                                        }
                                                    
                                                    } 
                                                
                                                else 
                                                    {
                                                    __context__.SourceCodeLine = 1521;
                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "Response "))  ) ) 
                                                        { 
                                                        __context__.SourceCodeLine = 1523;
                                                        GTEMPSEGMENT  .UpdateValue ( Functions.Remove ( "\u0020" , TEMPSTRING__DOLLAR__ )  ) ; 
                                                        __context__.SourceCodeLine = 1524;
                                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "OptionId: "))  ) ) 
                                                            { 
                                                            __context__.SourceCodeLine = 1526;
                                                            SETSELECTEDMESSAGEPROMPTRESPONSE (  __context__ , (ushort)( Functions.Atoi( TEMPSTRING__DOLLAR__ ) )) ; 
                                                            } 
                                                        
                                                        } 
                                                    
                                                    else 
                                                        {
                                                        __context__.SourceCodeLine = 1529;
                                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (GTEMPSEGMENT == "Cleared ") ) || Functions.TestForTrue ( Functions.BoolToInt (GTEMPSEGMENT == "Clear ") )) ))  ) ) 
                                                            { 
                                                            __context__.SourceCodeLine = 1531;
                                                            RESETMESSAGEPROMPT (  __context__  ) ; 
                                                            } 
                                                        
                                                        }
                                                    
                                                    }
                                                
                                                }
                                            
                                            } 
                                        
                                        else 
                                            {
                                            __context__.SourceCodeLine = 1534;
                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "Alert "))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 1536;
                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Find( "Display Text:" , TEMPSTRING__DOLLAR__ ) > 0 ))  ) ) 
                                                    { 
                                                    __context__.SourceCodeLine = 1538;
                                                    MESSAGE_ALERT_TEXT  .UpdateValue ( GETTAGVALUEREMOVEAMPERSANDS (  __context__ ,   ref  TEMPSTRING__DOLLAR__ )  ) ; 
                                                    __context__.SourceCodeLine = 1539;
                                                    MESSAGE_ALERT_ISACTIVE  .Value = (ushort) ( 1 ) ; 
                                                    } 
                                                
                                                else 
                                                    {
                                                    __context__.SourceCodeLine = 1541;
                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Find( "Cleared" , TEMPSTRING__DOLLAR__ ) > 0 ))  ) ) 
                                                        { 
                                                        __context__.SourceCodeLine = 1543;
                                                        MESSAGE_ALERT_TEXT  .UpdateValue ( ""  ) ; 
                                                        __context__.SourceCodeLine = 1544;
                                                        MESSAGE_ALERT_ISACTIVE  .Value = (ushort) ( 0 ) ; 
                                                        } 
                                                    
                                                    }
                                                
                                                } 
                                            
                                            }
                                        
                                        } 
                                    
                                    }
                                
                                }
                            
                            }
                        
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 1549;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "*c "))  ) ) 
                            { 
                            __context__.SourceCodeLine = 1551;
                            GTEMPSEGMENT  .UpdateValue ( Functions.Remove ( "\u0020" , TEMPSTRING__DOLLAR__ )  ) ; 
                            __context__.SourceCodeLine = 1553;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "xConfiguration "))  ) ) 
                                { 
                                __context__.SourceCodeLine = 1555;
                                VALIDRESPONSEFOUND (  __context__  ) ; 
                                __context__.SourceCodeLine = 1556;
                                GTEMPSEGMENT  .UpdateValue ( Functions.Remove ( "\u0020" , TEMPSTRING__DOLLAR__ )  ) ; 
                                __context__.SourceCodeLine = 1558;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (GTEMPSEGMENT == "Conference ") ) || Functions.TestForTrue ( Functions.BoolToInt (GTEMPSEGMENT == "Standby ") )) ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 1560;
                                    FROM_DEVICE_CONFIGURATIONCHANGES  .UpdateValue ( GTEMPSEGMENT + TEMPSTRING__DOLLAR__  ) ; 
                                    } 
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 1562;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "Audio "))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 1564;
                                        GETVOLUMELEVEL  .Value = (ushort) ( Functions.Atoi( TEMPSTRING__DOLLAR__ ) ) ; 
                                        } 
                                    
                                    else 
                                        {
                                        __context__.SourceCodeLine = 1566;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "SystemUnit "))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 1568;
                                            GTEMPSEGMENT  .UpdateValue ( Functions.Remove ( "\u0020" , TEMPSTRING__DOLLAR__ )  ) ; 
                                            __context__.SourceCodeLine = 1570;
                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "Name: "))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 1572;
                                                GETSYSTEMNAME  .UpdateValue ( Functions.Left ( TEMPSTRING__DOLLAR__ ,  (int) ( (Functions.Length( TEMPSTRING__DOLLAR__ ) - 2) ) )  ) ; 
                                                } 
                                            
                                            } 
                                        
                                        else 
                                            {
                                            __context__.SourceCodeLine = 1575;
                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "H323 "))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 1577;
                                                GTEMPSEGMENT  .UpdateValue ( Functions.Remove ( "\u0020" , TEMPSTRING__DOLLAR__ )  ) ; 
                                                __context__.SourceCodeLine = 1579;
                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "Profile "))  ) ) 
                                                    { 
                                                    __context__.SourceCodeLine = 1581;
                                                    GTEMPSEGMENT  .UpdateValue ( Functions.Remove ( "\u0020" , TEMPSTRING__DOLLAR__ )  ) ; 
                                                    __context__.SourceCodeLine = 1583;
                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "1 "))  ) ) 
                                                        { 
                                                        __context__.SourceCodeLine = 1585;
                                                        GTEMPSEGMENT  .UpdateValue ( Functions.Remove ( "\u0020" , TEMPSTRING__DOLLAR__ )  ) ; 
                                                        __context__.SourceCodeLine = 1587;
                                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "H323Alias"))  ) ) 
                                                            { 
                                                            __context__.SourceCodeLine = 1589;
                                                            GTEMPSEGMENT  .UpdateValue ( Functions.Remove ( "\u0020" , TEMPSTRING__DOLLAR__ )  ) ; 
                                                            __context__.SourceCodeLine = 1591;
                                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "ID: "))  ) ) 
                                                                { 
                                                                __context__.SourceCodeLine = 1593;
                                                                GETSYSTEMH323ID  .UpdateValue ( Functions.Left ( TEMPSTRING__DOLLAR__ ,  (int) ( (Functions.Length( TEMPSTRING__DOLLAR__ ) - 2) ) )  ) ; 
                                                                } 
                                                            
                                                            else 
                                                                {
                                                                __context__.SourceCodeLine = 1595;
                                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "E164: "))  ) ) 
                                                                    { 
                                                                    __context__.SourceCodeLine = 1597;
                                                                    GETSYSTEME164ALIAS  .UpdateValue ( Functions.Left ( TEMPSTRING__DOLLAR__ ,  (int) ( (Functions.Length( TEMPSTRING__DOLLAR__ ) - 2) ) )  ) ; 
                                                                    } 
                                                                
                                                                }
                                                            
                                                            } 
                                                        
                                                        else 
                                                            {
                                                            __context__.SourceCodeLine = 1600;
                                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "Gatekeeper "))  ) ) 
                                                                { 
                                                                __context__.SourceCodeLine = 1602;
                                                                GTEMPSEGMENT  .UpdateValue ( Functions.Remove ( "\u0020" , TEMPSTRING__DOLLAR__ )  ) ; 
                                                                __context__.SourceCodeLine = 1604;
                                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "Discovery "))  ) ) 
                                                                    { 
                                                                    __context__.SourceCodeLine = 1606;
                                                                    GETGATEKEEPERREGISTRATIONMODE  .UpdateValue ( GETTAGVALUE (  __context__ ,   ref  TEMPSTRING__DOLLAR__ )  ) ; 
                                                                    } 
                                                                
                                                                } 
                                                            
                                                            }
                                                        
                                                        } 
                                                    
                                                    } 
                                                
                                                } 
                                            
                                            else 
                                                {
                                                __context__.SourceCodeLine = 1612;
                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "SIP "))  ) ) 
                                                    { 
                                                    __context__.SourceCodeLine = 1614;
                                                    GTEMPSEGMENT  .UpdateValue ( Functions.Remove ( "\u0020" , TEMPSTRING__DOLLAR__ )  ) ; 
                                                    __context__.SourceCodeLine = 1616;
                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "Profile "))  ) ) 
                                                        { 
                                                        __context__.SourceCodeLine = 1618;
                                                        GTEMPSEGMENT  .UpdateValue ( Functions.Remove ( "\u0020" , TEMPSTRING__DOLLAR__ )  ) ; 
                                                        __context__.SourceCodeLine = 1620;
                                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "1 "))  ) ) 
                                                            { 
                                                            __context__.SourceCodeLine = 1622;
                                                            GTEMPSEGMENT  .UpdateValue ( Functions.Remove ( "\u0020" , TEMPSTRING__DOLLAR__ )  ) ; 
                                                            __context__.SourceCodeLine = 1624;
                                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "URI: "))  ) ) 
                                                                { 
                                                                __context__.SourceCodeLine = 1626;
                                                                GETSYSTEMSIPURI  .UpdateValue ( Functions.Left ( TEMPSTRING__DOLLAR__ ,  (int) ( (Functions.Length( TEMPSTRING__DOLLAR__ ) - 2) ) )  ) ; 
                                                                } 
                                                            
                                                            } 
                                                        
                                                        } 
                                                    
                                                    } 
                                                
                                                else 
                                                    {
                                                    __context__.SourceCodeLine = 1631;
                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "Video "))  ) ) 
                                                        { 
                                                        __context__.SourceCodeLine = 1633;
                                                        FROM_DEVICE_CONFIGURATIONCHANGES  .UpdateValue ( GTEMPSEGMENT + TEMPSTRING__DOLLAR__  ) ; 
                                                        __context__.SourceCodeLine = 1634;
                                                        GTEMPSEGMENT  .UpdateValue ( Functions.Remove ( "\u0020" , TEMPSTRING__DOLLAR__ )  ) ; 
                                                        __context__.SourceCodeLine = 1636;
                                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "Input "))  ) ) 
                                                            { 
                                                            __context__.SourceCodeLine = 1638;
                                                            GTEMPSEGMENT  .UpdateValue ( Functions.Remove ( "\u0020" , TEMPSTRING__DOLLAR__ )  ) ; 
                                                            __context__.SourceCodeLine = 1640;
                                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "Source "))  ) ) 
                                                                { 
                                                                __context__.SourceCodeLine = 1642;
                                                                GTEMPSEGMENT  .UpdateValue ( Functions.Remove ( "\u0020" , TEMPSTRING__DOLLAR__ )  ) ; 
                                                                __context__.SourceCodeLine = 1644;
                                                                INPUTSOURCEITEM = (ushort) ( Functions.Atoi( GTEMPSEGMENT ) ) ; 
                                                                __context__.SourceCodeLine = 1645;
                                                                GTEMPSEGMENT  .UpdateValue ( Functions.Remove ( "\u0020" , TEMPSTRING__DOLLAR__ )  ) ; 
                                                                __context__.SourceCodeLine = 1647;
                                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "Name: "))  ) ) 
                                                                    { 
                                                                    __context__.SourceCodeLine = 1649;
                                                                    INPUT_SOURCE_NAME [ INPUTSOURCEITEM]  .UpdateValue ( GETTAGVALUEREMOVEAMPERSANDS (  __context__ ,   ref  TEMPSTRING__DOLLAR__ )  ) ; 
                                                                    } 
                                                                
                                                                } 
                                                            
                                                            } 
                                                        
                                                        } 
                                                    
                                                    }
                                                
                                                }
                                            
                                            }
                                        
                                        }
                                    
                                    }
                                
                                } 
                            
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 1656;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "*r "))  ) ) 
                                { 
                                __context__.SourceCodeLine = 1658;
                                VALIDRESPONSEFOUND (  __context__  ) ; 
                                __context__.SourceCodeLine = 1659;
                                RetimeWait ( (int)1000, "RECEIVEPHONEBOOK" ) ; 
                                __context__.SourceCodeLine = 1660;
                                GTEMPSEGMENT  .UpdateValue ( Functions.Remove ( "\u0020" , TEMPSTRING__DOLLAR__ )  ) ; 
                                __context__.SourceCodeLine = 1661;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "PhonebookSearchResult "))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 1663;
                                    GTEMPSEGMENT  .UpdateValue ( Functions.Remove ( "\u0020" , TEMPSTRING__DOLLAR__ )  ) ; 
                                    } 
                                
                                __context__.SourceCodeLine = 1665;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "BootResult "))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 1667;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (TEMPSTRING__DOLLAR__ == "(status=OK):\u000D\u000A"))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 1669;
                                        Functions.Pulse ( 10, GETREBOOTCODEC_IS_IN_PROGRESS ) ; 
                                        } 
                                    
                                    } 
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 1672;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "CallHistoryRecentsResult "))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 1674;
                                        GTEMPSEGMENT  .UpdateValue ( Functions.Remove ( "\u0020" , TEMPSTRING__DOLLAR__ )  ) ; 
                                        __context__.SourceCodeLine = 1675;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "Entry "))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 1677;
                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (NEXTAVAILABLEDIRENTRY == 0))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 1679;
                                                CURRENTFIRSTPHONEBOOKITEMNUMBER  .Value = (ushort) ( ((INTERNALENTRYOFFSET * 20) + 1) ) ; 
                                                } 
                                            
                                            __context__.SourceCodeLine = 1681;
                                            CONTACTITEM = (ushort) ( Functions.Atoi( Functions.Remove( " " , TEMPSTRING__DOLLAR__ ) ) ) ; 
                                            __context__.SourceCodeLine = 1683;
                                            CONTACTITEM = (ushort) ( (CONTACTITEM + 1) ) ; 
                                            __context__.SourceCodeLine = 1685;
                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (PREVIOUSFOUNDCONTACTITEM != CONTACTITEM))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 1687;
                                                NEXTAVAILABLEDIRENTRY = (ushort) ( (NEXTAVAILABLEDIRENTRY + 1) ) ; 
                                                __context__.SourceCodeLine = 1688;
                                                NEXTAVAILABLECONTACTMETHOD = (ushort) ( (NEXTAVAILABLECONTACTMETHOD + 1) ) ; 
                                                __context__.SourceCodeLine = 1689;
                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( NEXTAVAILABLEDIRENTRY > 2000 ))  ) ) 
                                                    { 
                                                    __context__.SourceCodeLine = 1691;
                                                    GenerateUserNotice ( "Directory Exceeds Module's Max Entries. (Max Entries: {0:d})", (short)2000) ; 
                                                    __context__.SourceCodeLine = 1692;
                                                    NEXTAVAILABLEDIRENTRY = (ushort) ( 2000 ) ; 
                                                    __context__.SourceCodeLine = 1693;
                                                    Functions.ClearBuffer ( DEVICE_RX__DOLLAR__ ) ; 
                                                    __context__.SourceCodeLine = 1694;
                                                    CancelWait ( "RECEIVEPHONEBOOK" ) ; 
                                                    __context__.SourceCodeLine = 1695;
                                                    ICURRENTPAGE = (ushort) ( 1 ) ; 
                                                    __context__.SourceCodeLine = 1696;
                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Mod( PHONEBOOKENTRY[ SELECTEDFOLDER ].CHILDCOUNT , IENTRIESPERPAGE ) > 0 ))  ) ) 
                                                        { 
                                                        __context__.SourceCodeLine = 1698;
                                                        ITOTALPAGES = (ushort) ( ((PHONEBOOKENTRY[ SELECTEDFOLDER ].CHILDCOUNT / IENTRIESPERPAGE) + 1) ) ; 
                                                        } 
                                                    
                                                    else 
                                                        { 
                                                        __context__.SourceCodeLine = 1702;
                                                        ITOTALPAGES = (ushort) ( (PHONEBOOKENTRY[ SELECTEDFOLDER ].CHILDCOUNT / IENTRIESPERPAGE) ) ; 
                                                        } 
                                                    
                                                    __context__.SourceCodeLine = 1704;
                                                    PHONEBOOKPAGESCURRENTPAGE  .Value = (ushort) ( ICURRENTPAGE ) ; 
                                                    __context__.SourceCodeLine = 1705;
                                                    PHONEBOOKPAGESTOTALPAGES  .Value = (ushort) ( ITOTALPAGES ) ; 
                                                    __context__.SourceCodeLine = 1706;
                                                    FDISPLAYNEWPAGE (  __context__  ) ; 
                                                    __context__.SourceCodeLine = 1707;
                                                    DISPLAYPHONEBOOK (  __context__  ) ; 
                                                    __context__.SourceCodeLine = 1708;
                                                    DIRECTORYDOWNLOADBUSY  .Value = (ushort) ( 0 ) ; 
                                                    __context__.SourceCodeLine = 1709;
                                                    RECEIVINGPHONEBOOK = (ushort) ( 0 ) ; 
                                                    __context__.SourceCodeLine = 1710;
                                                    break ; 
                                                    } 
                                                
                                                __context__.SourceCodeLine = 1712;
                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( NEXTAVAILABLECONTACTMETHOD > 4000 ))  ) ) 
                                                    { 
                                                    __context__.SourceCodeLine = 1714;
                                                    GenerateUserNotice ( "Directory Exceeds Module's Max Contact Methods. (Max Entries: {0:d})", (short)4000) ; 
                                                    __context__.SourceCodeLine = 1715;
                                                    NEXTAVAILABLECONTACTMETHOD = (ushort) ( 4000 ) ; 
                                                    __context__.SourceCodeLine = 1716;
                                                    Functions.ClearBuffer ( DEVICE_RX__DOLLAR__ ) ; 
                                                    __context__.SourceCodeLine = 1717;
                                                    CancelWait ( "RECEIVEPHONEBOOK" ) ; 
                                                    __context__.SourceCodeLine = 1718;
                                                    ICURRENTPAGE = (ushort) ( 1 ) ; 
                                                    __context__.SourceCodeLine = 1719;
                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Mod( PHONEBOOKENTRY[ SELECTEDFOLDER ].CHILDCOUNT , IENTRIESPERPAGE ) > 0 ))  ) ) 
                                                        { 
                                                        __context__.SourceCodeLine = 1721;
                                                        ITOTALPAGES = (ushort) ( ((PHONEBOOKENTRY[ SELECTEDFOLDER ].CHILDCOUNT / IENTRIESPERPAGE) + 1) ) ; 
                                                        } 
                                                    
                                                    else 
                                                        { 
                                                        __context__.SourceCodeLine = 1725;
                                                        ITOTALPAGES = (ushort) ( (PHONEBOOKENTRY[ SELECTEDFOLDER ].CHILDCOUNT / IENTRIESPERPAGE) ) ; 
                                                        } 
                                                    
                                                    __context__.SourceCodeLine = 1727;
                                                    PHONEBOOKPAGESCURRENTPAGE  .Value = (ushort) ( ICURRENTPAGE ) ; 
                                                    __context__.SourceCodeLine = 1728;
                                                    PHONEBOOKPAGESTOTALPAGES  .Value = (ushort) ( ITOTALPAGES ) ; 
                                                    __context__.SourceCodeLine = 1729;
                                                    FDISPLAYNEWPAGE (  __context__  ) ; 
                                                    __context__.SourceCodeLine = 1730;
                                                    DISPLAYPHONEBOOK (  __context__  ) ; 
                                                    __context__.SourceCodeLine = 1731;
                                                    DIRECTORYDOWNLOADBUSY  .Value = (ushort) ( 0 ) ; 
                                                    __context__.SourceCodeLine = 1732;
                                                    RECEIVINGPHONEBOOK = (ushort) ( 0 ) ; 
                                                    __context__.SourceCodeLine = 1733;
                                                    break ; 
                                                    } 
                                                
                                                __context__.SourceCodeLine = 1735;
                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (BGETRECENTSONLY == 0))  ) ) 
                                                    { 
                                                    __context__.SourceCodeLine = 1737;
                                                    CURRENTLASTPHONEBOOKITEMNUMBER  .Value = (ushort) ( (CURRENTLASTPHONEBOOKITEMNUMBER  .Value + 1) ) ; 
                                                    } 
                                                
                                                __context__.SourceCodeLine = 1739;
                                                PREVIOUSFOUNDCONTACTMETHOD = (ushort) ( 1 ) ; 
                                                __context__.SourceCodeLine = 1740;
                                                PHONEBOOKENTRY [ NEXTAVAILABLEDIRENTRY] . FIRSTCHILD = (ushort) ( NEXTAVAILABLECONTACTMETHOD ) ; 
                                                __context__.SourceCodeLine = 1741;
                                                PHONEBOOKENTRY [ NEXTFOLDERENTRY] . CHILDCOUNT = (ushort) ( (PHONEBOOKENTRY[ NEXTFOLDERENTRY ].CHILDCOUNT + 1) ) ; 
                                                __context__.SourceCodeLine = 1742;
                                                PHONEBOOKLEVELTOTALROWS = (ushort) ( PHONEBOOKENTRY[ NEXTFOLDERENTRY ].CHILDCOUNT ) ; 
                                                __context__.SourceCodeLine = 1743;
                                                PHONEBOOKENTRY [ NEXTAVAILABLEDIRENTRY] . PARENTFOLDER = (ushort) ( NEXTFOLDERENTRY ) ; 
                                                } 
                                            
                                            __context__.SourceCodeLine = 1745;
                                            GTEMPSEGMENT  .UpdateValue ( Functions.Remove ( "\u0020" , TEMPSTRING__DOLLAR__ )  ) ; 
                                            __context__.SourceCodeLine = 1746;
                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "DisplayName: "))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 1748;
                                                PHONEBOOKENTRY [ NEXTAVAILABLEDIRENTRY] . ENTRYNAME  .UpdateValue ( UTF8TOISO8859 (  __context__ , REMOVEAMPERSANDS( __context__ , TEMPSTRING__DOLLAR__ ))  ) ; 
                                                } 
                                            
                                            else 
                                                {
                                                __context__.SourceCodeLine = 1750;
                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "CallbackNumber: "))  ) ) 
                                                    { 
                                                    __context__.SourceCodeLine = 1752;
                                                    PHONEBOOKENTRY [ NEXTAVAILABLEDIRENTRY] . CHILDCOUNT = (ushort) ( (PHONEBOOKENTRY[ NEXTAVAILABLEDIRENTRY ].CHILDCOUNT + 1) ) ; 
                                                    __context__.SourceCodeLine = 1753;
                                                    CONTACT [ NEXTAVAILABLECONTACTMETHOD] . CONTACTNUMBER  .UpdateValue ( REMOVEAMPERSANDS (  __context__ , TEMPSTRING__DOLLAR__)  ) ; 
                                                    __context__.SourceCodeLine = 1754;
                                                    CONTACT [ NEXTAVAILABLECONTACTMETHOD] . CALLTYPE = (ushort) ( 3 ) ; 
                                                    } 
                                                
                                                }
                                            
                                            __context__.SourceCodeLine = 1756;
                                            PREVIOUSFOUNDCONTACTITEM = (ushort) ( CONTACTITEM ) ; 
                                            } 
                                        
                                        } 
                                    
                                    else 
                                        {
                                        __context__.SourceCodeLine = 1759;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "ResultSet "))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 1761;
                                            GTEMPSEGMENT  .UpdateValue ( Functions.Remove ( "\u0020" , TEMPSTRING__DOLLAR__ )  ) ; 
                                            __context__.SourceCodeLine = 1762;
                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "Contact "))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 1764;
                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (NEXTAVAILABLEDIRENTRY == 0))  ) ) 
                                                    { 
                                                    __context__.SourceCodeLine = 1766;
                                                    CURRENTFIRSTPHONEBOOKITEMNUMBER  .Value = (ushort) ( ((INTERNALENTRYOFFSET * 20) + 1) ) ; 
                                                    } 
                                                
                                                __context__.SourceCodeLine = 1768;
                                                CONTACTITEM = (ushort) ( Functions.Atoi( Functions.Remove( " " , TEMPSTRING__DOLLAR__ ) ) ) ; 
                                                __context__.SourceCodeLine = 1771;
                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (PREVIOUSFOUNDCONTACTITEM != CONTACTITEM))  ) ) 
                                                    { 
                                                    __context__.SourceCodeLine = 1773;
                                                    NEXTAVAILABLEDIRENTRY = (ushort) ( (NEXTAVAILABLEDIRENTRY + 1) ) ; 
                                                    __context__.SourceCodeLine = 1774;
                                                    NEXTAVAILABLECONTACTMETHOD = (ushort) ( (NEXTAVAILABLECONTACTMETHOD + 1) ) ; 
                                                    __context__.SourceCodeLine = 1775;
                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( NEXTAVAILABLEDIRENTRY > 2000 ))  ) ) 
                                                        { 
                                                        __context__.SourceCodeLine = 1777;
                                                        GenerateUserNotice ( "Directory Exceeds Module's Max Entries. (Max Entries: {0:d})", (short)2000) ; 
                                                        __context__.SourceCodeLine = 1778;
                                                        NEXTAVAILABLEDIRENTRY = (ushort) ( 2000 ) ; 
                                                        __context__.SourceCodeLine = 1779;
                                                        Functions.ClearBuffer ( DEVICE_RX__DOLLAR__ ) ; 
                                                        __context__.SourceCodeLine = 1780;
                                                        CancelWait ( "RECEIVEPHONEBOOK" ) ; 
                                                        __context__.SourceCodeLine = 1781;
                                                        ICURRENTPAGE = (ushort) ( 1 ) ; 
                                                        __context__.SourceCodeLine = 1782;
                                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Mod( PHONEBOOKENTRY[ SELECTEDFOLDER ].CHILDCOUNT , IENTRIESPERPAGE ) > 0 ))  ) ) 
                                                            { 
                                                            __context__.SourceCodeLine = 1784;
                                                            ITOTALPAGES = (ushort) ( ((PHONEBOOKENTRY[ SELECTEDFOLDER ].CHILDCOUNT / IENTRIESPERPAGE) + 1) ) ; 
                                                            } 
                                                        
                                                        else 
                                                            { 
                                                            __context__.SourceCodeLine = 1788;
                                                            ITOTALPAGES = (ushort) ( (PHONEBOOKENTRY[ SELECTEDFOLDER ].CHILDCOUNT / IENTRIESPERPAGE) ) ; 
                                                            } 
                                                        
                                                        __context__.SourceCodeLine = 1790;
                                                        PHONEBOOKPAGESCURRENTPAGE  .Value = (ushort) ( ICURRENTPAGE ) ; 
                                                        __context__.SourceCodeLine = 1791;
                                                        PHONEBOOKPAGESTOTALPAGES  .Value = (ushort) ( ITOTALPAGES ) ; 
                                                        __context__.SourceCodeLine = 1792;
                                                        FDISPLAYNEWPAGE (  __context__  ) ; 
                                                        __context__.SourceCodeLine = 1793;
                                                        DISPLAYPHONEBOOK (  __context__  ) ; 
                                                        __context__.SourceCodeLine = 1794;
                                                        DIRECTORYDOWNLOADBUSY  .Value = (ushort) ( 0 ) ; 
                                                        __context__.SourceCodeLine = 1795;
                                                        RECEIVINGPHONEBOOK = (ushort) ( 0 ) ; 
                                                        __context__.SourceCodeLine = 1796;
                                                        break ; 
                                                        } 
                                                    
                                                    __context__.SourceCodeLine = 1798;
                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( NEXTAVAILABLECONTACTMETHOD > 4000 ))  ) ) 
                                                        { 
                                                        __context__.SourceCodeLine = 1800;
                                                        GenerateUserNotice ( "Directory Exceeds Module's Max Contact Methods. (Max Entries: {0:d})", (short)4000) ; 
                                                        __context__.SourceCodeLine = 1801;
                                                        NEXTAVAILABLECONTACTMETHOD = (ushort) ( 4000 ) ; 
                                                        __context__.SourceCodeLine = 1802;
                                                        Functions.ClearBuffer ( DEVICE_RX__DOLLAR__ ) ; 
                                                        __context__.SourceCodeLine = 1803;
                                                        CancelWait ( "RECEIVEPHONEBOOK" ) ; 
                                                        __context__.SourceCodeLine = 1804;
                                                        ICURRENTPAGE = (ushort) ( 1 ) ; 
                                                        __context__.SourceCodeLine = 1805;
                                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Mod( PHONEBOOKENTRY[ SELECTEDFOLDER ].CHILDCOUNT , IENTRIESPERPAGE ) > 0 ))  ) ) 
                                                            { 
                                                            __context__.SourceCodeLine = 1807;
                                                            ITOTALPAGES = (ushort) ( ((PHONEBOOKENTRY[ SELECTEDFOLDER ].CHILDCOUNT / IENTRIESPERPAGE) + 1) ) ; 
                                                            } 
                                                        
                                                        else 
                                                            { 
                                                            __context__.SourceCodeLine = 1811;
                                                            ITOTALPAGES = (ushort) ( (PHONEBOOKENTRY[ SELECTEDFOLDER ].CHILDCOUNT / IENTRIESPERPAGE) ) ; 
                                                            } 
                                                        
                                                        __context__.SourceCodeLine = 1813;
                                                        PHONEBOOKPAGESCURRENTPAGE  .Value = (ushort) ( ICURRENTPAGE ) ; 
                                                        __context__.SourceCodeLine = 1814;
                                                        PHONEBOOKPAGESTOTALPAGES  .Value = (ushort) ( ITOTALPAGES ) ; 
                                                        __context__.SourceCodeLine = 1815;
                                                        FDISPLAYNEWPAGE (  __context__  ) ; 
                                                        __context__.SourceCodeLine = 1816;
                                                        DISPLAYPHONEBOOK (  __context__  ) ; 
                                                        __context__.SourceCodeLine = 1817;
                                                        DIRECTORYDOWNLOADBUSY  .Value = (ushort) ( 0 ) ; 
                                                        __context__.SourceCodeLine = 1818;
                                                        RECEIVINGPHONEBOOK = (ushort) ( 0 ) ; 
                                                        __context__.SourceCodeLine = 1819;
                                                        break ; 
                                                        } 
                                                    
                                                    __context__.SourceCodeLine = 1821;
                                                    CURRENTLASTPHONEBOOKITEMNUMBER  .Value = (ushort) ( (CURRENTLASTPHONEBOOKITEMNUMBER  .Value + 1) ) ; 
                                                    __context__.SourceCodeLine = 1822;
                                                    PREVIOUSFOUNDCONTACTMETHOD = (ushort) ( 1 ) ; 
                                                    __context__.SourceCodeLine = 1823;
                                                    PHONEBOOKENTRY [ NEXTAVAILABLEDIRENTRY] . FIRSTCHILD = (ushort) ( NEXTAVAILABLECONTACTMETHOD ) ; 
                                                    __context__.SourceCodeLine = 1824;
                                                    PHONEBOOKENTRY [ NEXTFOLDERENTRY] . CHILDCOUNT = (ushort) ( (PHONEBOOKENTRY[ NEXTFOLDERENTRY ].CHILDCOUNT + 1) ) ; 
                                                    __context__.SourceCodeLine = 1825;
                                                    PHONEBOOKENTRY [ NEXTAVAILABLEDIRENTRY] . PARENTFOLDER = (ushort) ( NEXTFOLDERENTRY ) ; 
                                                    } 
                                                
                                                __context__.SourceCodeLine = 1827;
                                                GTEMPSEGMENT  .UpdateValue ( Functions.Remove ( "\u0020" , TEMPSTRING__DOLLAR__ )  ) ; 
                                                __context__.SourceCodeLine = 1828;
                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "Name: "))  ) ) 
                                                    { 
                                                    __context__.SourceCodeLine = 1830;
                                                    PHONEBOOKENTRY [ NEXTAVAILABLEDIRENTRY] . ENTRYNAME  .UpdateValue ( UTF8TOISO8859 (  __context__ , REMOVEAMPERSANDS( __context__ , TEMPSTRING__DOLLAR__ ))  ) ; 
                                                    } 
                                                
                                                else 
                                                    {
                                                    __context__.SourceCodeLine = 1832;
                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "ContactMethod "))  ) ) 
                                                        { 
                                                        __context__.SourceCodeLine = 1834;
                                                        GTEMPSEGMENT  .UpdateValue ( Functions.Remove ( "\u0020" , TEMPSTRING__DOLLAR__ )  ) ; 
                                                        __context__.SourceCodeLine = 1835;
                                                        CONTACTMETHOD = (ushort) ( Functions.Atoi( GTEMPSEGMENT ) ) ; 
                                                        __context__.SourceCodeLine = 1836;
                                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (PREVIOUSFOUNDCONTACTMETHOD != CONTACTMETHOD))  ) ) 
                                                            { 
                                                            __context__.SourceCodeLine = 1838;
                                                            PREVIOUSFOUNDCONTACTMETHOD = (ushort) ( CONTACTMETHOD ) ; 
                                                            __context__.SourceCodeLine = 1839;
                                                            NEXTAVAILABLECONTACTMETHOD = (ushort) ( (NEXTAVAILABLECONTACTMETHOD + 1) ) ; 
                                                            } 
                                                        
                                                        __context__.SourceCodeLine = 1841;
                                                        GTEMPSEGMENT  .UpdateValue ( Functions.Remove ( "\u0020" , TEMPSTRING__DOLLAR__ )  ) ; 
                                                        __context__.SourceCodeLine = 1843;
                                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "Number: "))  ) ) 
                                                            { 
                                                            __context__.SourceCodeLine = 1845;
                                                            PHONEBOOKENTRY [ NEXTAVAILABLEDIRENTRY] . CHILDCOUNT = (ushort) ( (PHONEBOOKENTRY[ NEXTAVAILABLEDIRENTRY ].CHILDCOUNT + 1) ) ; 
                                                            __context__.SourceCodeLine = 1846;
                                                            CONTACT [ NEXTAVAILABLECONTACTMETHOD] . CONTACTNUMBER  .UpdateValue ( REMOVEAMPERSANDS (  __context__ , TEMPSTRING__DOLLAR__)  ) ; 
                                                            } 
                                                        
                                                        else 
                                                            {
                                                            __context__.SourceCodeLine = 1848;
                                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "Protocol: "))  ) ) 
                                                                { 
                                                                __context__.SourceCodeLine = 1850;
                                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.Left( TEMPSTRING__DOLLAR__ , (int)( 3 ) ) == "SIP"))  ) ) 
                                                                    { 
                                                                    __context__.SourceCodeLine = 1852;
                                                                    CONTACT [ NEXTAVAILABLECONTACTMETHOD] . CALLTYPE = (ushort) ( (CONTACT[ NEXTAVAILABLECONTACTMETHOD ].CALLTYPE | 2) ) ; 
                                                                    } 
                                                                
                                                                } 
                                                            
                                                            else 
                                                                {
                                                                __context__.SourceCodeLine = 1855;
                                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "CallRate: "))  ) ) 
                                                                    { 
                                                                    __context__.SourceCodeLine = 1857;
                                                                    CONTACT [ NEXTAVAILABLECONTACTMETHOD] . CALLRATE  .UpdateValue ( REMOVEAMPERSANDS (  __context__ , TEMPSTRING__DOLLAR__)  ) ; 
                                                                    } 
                                                                
                                                                }
                                                            
                                                            }
                                                        
                                                        } 
                                                    
                                                    }
                                                
                                                __context__.SourceCodeLine = 1860;
                                                PREVIOUSFOUNDCONTACTITEM = (ushort) ( CONTACTITEM ) ; 
                                                } 
                                            
                                            else 
                                                {
                                                __context__.SourceCodeLine = 1862;
                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "Folder "))  ) ) 
                                                    { 
                                                    __context__.SourceCodeLine = 1864;
                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (NEXTAVAILABLEDIRENTRY == 0))  ) ) 
                                                        { 
                                                        __context__.SourceCodeLine = 1866;
                                                        CURRENTFIRSTPHONEBOOKITEMNUMBER  .Value = (ushort) ( ((INTERNALENTRYOFFSET * 20) + 1) ) ; 
                                                        } 
                                                    
                                                    __context__.SourceCodeLine = 1868;
                                                    CONTACTITEM = (ushort) ( Functions.Atoi( Functions.Remove( " " , TEMPSTRING__DOLLAR__ ) ) ) ; 
                                                    __context__.SourceCodeLine = 1869;
                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (PREVIOUSFOUNDCONTACTITEM != (CONTACTITEM + 2000)))  ) ) 
                                                        { 
                                                        __context__.SourceCodeLine = 1871;
                                                        NEXTAVAILABLEDIRENTRY = (ushort) ( (NEXTAVAILABLEDIRENTRY + 1) ) ; 
                                                        __context__.SourceCodeLine = 1872;
                                                        PHONEBOOKENTRY [ NEXTFOLDERENTRY] . CHILDCOUNT = (ushort) ( (PHONEBOOKENTRY[ NEXTFOLDERENTRY ].CHILDCOUNT + 1) ) ; 
                                                        __context__.SourceCodeLine = 1873;
                                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( NEXTAVAILABLEDIRENTRY > 2000 ))  ) ) 
                                                            { 
                                                            __context__.SourceCodeLine = 1875;
                                                            GenerateUserNotice ( "Directory Exceeds Module's Max Entries. (Max Entries: {0:d})", (short)2000) ; 
                                                            __context__.SourceCodeLine = 1876;
                                                            NEXTAVAILABLEDIRENTRY = (ushort) ( 2000 ) ; 
                                                            __context__.SourceCodeLine = 1877;
                                                            Functions.ClearBuffer ( DEVICE_RX__DOLLAR__ ) ; 
                                                            __context__.SourceCodeLine = 1878;
                                                            CancelWait ( "RECEIVEPHONEBOOK" ) ; 
                                                            __context__.SourceCodeLine = 1879;
                                                            ICURRENTPAGE = (ushort) ( 1 ) ; 
                                                            __context__.SourceCodeLine = 1880;
                                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Mod( PHONEBOOKENTRY[ SELECTEDFOLDER ].CHILDCOUNT , IENTRIESPERPAGE ) > 0 ))  ) ) 
                                                                { 
                                                                __context__.SourceCodeLine = 1882;
                                                                ITOTALPAGES = (ushort) ( ((PHONEBOOKENTRY[ SELECTEDFOLDER ].CHILDCOUNT / IENTRIESPERPAGE) + 1) ) ; 
                                                                } 
                                                            
                                                            else 
                                                                { 
                                                                __context__.SourceCodeLine = 1886;
                                                                ITOTALPAGES = (ushort) ( (PHONEBOOKENTRY[ SELECTEDFOLDER ].CHILDCOUNT / IENTRIESPERPAGE) ) ; 
                                                                } 
                                                            
                                                            __context__.SourceCodeLine = 1888;
                                                            PHONEBOOKPAGESCURRENTPAGE  .Value = (ushort) ( ICURRENTPAGE ) ; 
                                                            __context__.SourceCodeLine = 1889;
                                                            PHONEBOOKPAGESTOTALPAGES  .Value = (ushort) ( ITOTALPAGES ) ; 
                                                            __context__.SourceCodeLine = 1890;
                                                            FDISPLAYNEWPAGE (  __context__  ) ; 
                                                            __context__.SourceCodeLine = 1891;
                                                            DISPLAYPHONEBOOK (  __context__  ) ; 
                                                            __context__.SourceCodeLine = 1892;
                                                            DIRECTORYDOWNLOADBUSY  .Value = (ushort) ( 0 ) ; 
                                                            __context__.SourceCodeLine = 1893;
                                                            RECEIVINGPHONEBOOK = (ushort) ( 0 ) ; 
                                                            __context__.SourceCodeLine = 1894;
                                                            break ; 
                                                            } 
                                                        
                                                        __context__.SourceCodeLine = 1896;
                                                        CURRENTLASTPHONEBOOKITEMNUMBER  .Value = (ushort) ( (CURRENTLASTPHONEBOOKITEMNUMBER  .Value + 1) ) ; 
                                                        __context__.SourceCodeLine = 1897;
                                                        PHONEBOOKENTRY [ NEXTAVAILABLEDIRENTRY] . PARENTFOLDER = (ushort) ( NEXTFOLDERENTRY ) ; 
                                                        } 
                                                    
                                                    __context__.SourceCodeLine = 1899;
                                                    PHONEBOOKENTRY [ NEXTAVAILABLEDIRENTRY] . FIRSTCHILD = (ushort) ( (2000 + 1) ) ; 
                                                    __context__.SourceCodeLine = 1900;
                                                    PHONEBOOKENTRYTYPE  .UpdateValue ( Functions.Mid ( TEMPSTRING__DOLLAR__ ,  (int) ( 1 ) ,  (int) ( (Functions.Find( ": " , TEMPSTRING__DOLLAR__ ) - 1) ) )  ) ; 
                                                    __context__.SourceCodeLine = 1901;
                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (PHONEBOOKENTRYTYPE == "Name"))  ) ) 
                                                        { 
                                                        __context__.SourceCodeLine = 1903;
                                                        PHONEBOOKENTRY [ NEXTAVAILABLEDIRENTRY] . ENTRYNAME  .UpdateValue ( UTF8TOISO8859 (  __context__ , GETTAGVALUEREMOVEAMPERSANDS( __context__ , ref TEMPSTRING__DOLLAR__ ))  ) ; 
                                                        } 
                                                    
                                                    else 
                                                        {
                                                        __context__.SourceCodeLine = 1905;
                                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (PHONEBOOKENTRYTYPE == "FolderId"))  ) ) 
                                                            { 
                                                            __context__.SourceCodeLine = 1907;
                                                            PHONEBOOKENTRY [ NEXTAVAILABLEDIRENTRY] . FOLDERID  .UpdateValue ( GETTAGVALUEREMOVEAMPERSANDS (  __context__ ,   ref  TEMPSTRING__DOLLAR__ )  ) ; 
                                                            } 
                                                        
                                                        }
                                                    
                                                    __context__.SourceCodeLine = 1909;
                                                    PREVIOUSFOUNDCONTACTITEM = (ushort) ( (CONTACTITEM + 2000) ) ; 
                                                    } 
                                                
                                                else 
                                                    {
                                                    __context__.SourceCodeLine = 1911;
                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "ResultInfo "))  ) ) 
                                                        { 
                                                        __context__.SourceCodeLine = 1913;
                                                        GTEMPSEGMENT  .UpdateValue ( Functions.Remove ( "\u0020" , TEMPSTRING__DOLLAR__ )  ) ; 
                                                        __context__.SourceCodeLine = 1915;
                                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GTEMPSEGMENT == "TotalRows: "))  ) ) 
                                                            { 
                                                            __context__.SourceCodeLine = 1917;
                                                            PHONEBOOKLEVELTOTALROWS = (ushort) ( Functions.Atoi( TEMPSTRING__DOLLAR__ ) ) ; 
                                                            } 
                                                        
                                                        } 
                                                    
                                                    }
                                                
                                                }
                                            
                                            } 
                                        
                                        }
                                    
                                    }
                                
                                } 
                            
                            }
                        
                        }
                    
                    }
                
                } 
            
            }
        
        __context__.SourceCodeLine = 1923;
        IPROCESS = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 1036;
        } 
    
    
    }
    
private void RESETRECENTSONLY (  SplusExecutionContext __context__ ) 
    { 
    ushort I = 0;
    ushort J = 0;
    
    
    __context__.SourceCodeLine = 1931;
    SELECTEDFOLDER = (ushort) ( 0 ) ; 
    __context__.SourceCodeLine = 1932;
    DIRECTORYONTOPLEVEL  .Value = (ushort) ( 1 ) ; 
    __context__.SourceCodeLine = 1933;
    PHONEBOOK_SEARCHRESULT_IS_FROM_SUBFOLDER  .Value = (ushort) ( 0 ) ; 
    __context__.SourceCodeLine = 1934;
    PHONEBOOK_SELECTED_FOLDER_NAME  .UpdateValue ( ""  ) ; 
    __context__.SourceCodeLine = 1935;
    SEARCHACTIVE = (ushort) ( 0 ) ; 
    __context__.SourceCodeLine = 1936;
    SEARCH_IS_ACTIVE  .Value = (ushort) ( SEARCHACTIVE ) ; 
    __context__.SourceCodeLine = 1937;
    SSEARCHTEXT  .UpdateValue ( ""  ) ; 
    __context__.SourceCodeLine = 1938;
    PHONEBOOK_SEARCH_TEXT  .UpdateValue ( SSEARCHTEXT  ) ; 
    __context__.SourceCodeLine = 1939;
    NEXTAVAILABLECONTACTMETHOD = (ushort) ( (PHONEBOOKENTRY[ PHONEBOOKENTRY[ 1 ].FIRSTCHILD ].FIRSTCHILD - 1) ) ; 
    __context__.SourceCodeLine = 1940;
    NEXTAVAILABLEDIRENTRY = (ushort) ( (PHONEBOOKENTRY[ 1 ].FIRSTCHILD - 1) ) ; 
    __context__.SourceCodeLine = 1941;
    NEXTFOLDERENTRY = (ushort) ( 1 ) ; 
    __context__.SourceCodeLine = 1942;
    ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
    ushort __FN_FOREND_VAL__1 = (ushort)(40 - 1); 
    int __FN_FORSTEP_VAL__1 = (int)1; 
    for ( I  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (I  >= __FN_FORSTART_VAL__1) && (I  <= __FN_FOREND_VAL__1) ) : ( (I  <= __FN_FORSTART_VAL__1) && (I  >= __FN_FOREND_VAL__1) ) ; I  += (ushort)__FN_FORSTEP_VAL__1) 
        { 
        __context__.SourceCodeLine = 1944;
        J = (ushort) ( (I + PHONEBOOKENTRY[ 1 ].FIRSTCHILD) ) ; 
        __context__.SourceCodeLine = 1945;
        CONTACT [ PHONEBOOKENTRY[ J ].FIRSTCHILD] . CONTACTNUMBER  .UpdateValue ( ""  ) ; 
        __context__.SourceCodeLine = 1946;
        CONTACT [ PHONEBOOKENTRY[ J ].FIRSTCHILD] . CALLRATE  .UpdateValue ( ""  ) ; 
        __context__.SourceCodeLine = 1947;
        CONTACT [ PHONEBOOKENTRY[ J ].FIRSTCHILD] . CALLTYPE = (ushort) ( 3 ) ; 
        __context__.SourceCodeLine = 1948;
        PHONEBOOKENTRY [ J] . ENTRYNAME  .UpdateValue ( ""  ) ; 
        __context__.SourceCodeLine = 1949;
        PHONEBOOKENTRY [ J] . FOLDERID  .UpdateValue ( ""  ) ; 
        __context__.SourceCodeLine = 1950;
        PHONEBOOKENTRY [ J] . CHILDCOUNT = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 1951;
        PHONEBOOKENTRY [ J] . FIRSTCHILD = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 1952;
        PHONEBOOKENTRY [ J] . PARENTFOLDER = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 1942;
        } 
    
    __context__.SourceCodeLine = 1954;
    PHONEBOOKENTRY [ 1] . CHILDCOUNT = (ushort) ( 0 ) ; 
    __context__.SourceCodeLine = 1955;
    BGETRECENTSONLY = (ushort) ( 0 ) ; 
    __context__.SourceCodeLine = 1956;
    RECENTCALLSDOWNLOADBUSY  .Value = (ushort) ( BGETRECENTSONLY ) ; 
    
    }
    
private void RESETEVERYTHING (  SplusExecutionContext __context__ ) 
    { 
    ushort I = 0;
    
    
    __context__.SourceCodeLine = 1963;
    TOTALROWSFORSEARCH  .Value = (ushort) ( 0 ) ; 
    __context__.SourceCodeLine = 1964;
    PHONEBOOKLEVELTOTALROWS = (ushort) ( 0 ) ; 
    __context__.SourceCodeLine = 1965;
    INTERNALENTRYOFFSET = (ushort) ( 0 ) ; 
    __context__.SourceCodeLine = 1966;
    SELECTEDFOLDER = (ushort) ( 0 ) ; 
    __context__.SourceCodeLine = 1967;
    DIRECTORYONTOPLEVEL  .Value = (ushort) ( 1 ) ; 
    __context__.SourceCodeLine = 1968;
    PHONEBOOK_SEARCHRESULT_IS_FROM_SUBFOLDER  .Value = (ushort) ( 0 ) ; 
    __context__.SourceCodeLine = 1969;
    NEXTAVAILABLEDIRENTRY = (ushort) ( 0 ) ; 
    __context__.SourceCodeLine = 1970;
    NEXTAVAILABLECONTACTMETHOD = (ushort) ( 0 ) ; 
    __context__.SourceCodeLine = 1971;
    NEXTFOLDERENTRY = (ushort) ( 0 ) ; 
    __context__.SourceCodeLine = 1972;
    PREVIOUSFOUNDCONTACTITEM = (ushort) ( 0 ) ; 
    __context__.SourceCodeLine = 1973;
    CURRENTFIRSTPHONEBOOKITEMNUMBER  .Value = (ushort) ( 0 ) ; 
    __context__.SourceCodeLine = 1974;
    CURRENTLASTPHONEBOOKITEMNUMBER  .Value = (ushort) ( 0 ) ; 
    __context__.SourceCodeLine = 1975;
    PHONEBOOK_SELECTED_FOLDER_NAME  .UpdateValue ( ""  ) ; 
    __context__.SourceCodeLine = 1976;
    RESETMESSAGEPROMPT (  __context__  ) ; 
    __context__.SourceCodeLine = 1977;
    BGETRECENTSONLY = (ushort) ( 0 ) ; 
    __context__.SourceCodeLine = 1978;
    RECENTCALLSDOWNLOADBUSY  .Value = (ushort) ( BGETRECENTSONLY ) ; 
    __context__.SourceCodeLine = 1979;
    ICURRENTPAGE = (ushort) ( 1 ) ; 
    __context__.SourceCodeLine = 1980;
    ITOTALPAGES = (ushort) ( 1 ) ; 
    __context__.SourceCodeLine = 1981;
    PHONEBOOKPAGESCURRENTPAGE  .Value = (ushort) ( ICURRENTPAGE ) ; 
    __context__.SourceCodeLine = 1982;
    PHONEBOOKPAGESTOTALPAGES  .Value = (ushort) ( ITOTALPAGES ) ; 
    __context__.SourceCodeLine = 1984;
    ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
    ushort __FN_FOREND_VAL__1 = (ushort)IENTRIESPERPAGE; 
    int __FN_FORSTEP_VAL__1 = (int)1; 
    for ( I  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (I  >= __FN_FORSTART_VAL__1) && (I  <= __FN_FOREND_VAL__1) ) : ( (I  <= __FN_FORSTART_VAL__1) && (I  >= __FN_FOREND_VAL__1) ) ; I  += (ushort)__FN_FORSTEP_VAL__1) 
        { 
        __context__.SourceCodeLine = 1986;
        PHONEBOOK_PAGES_NAME [ I]  .UpdateValue ( ""  ) ; 
        __context__.SourceCodeLine = 1984;
        } 
    
    __context__.SourceCodeLine = 1988;
    ushort __FN_FORSTART_VAL__2 = (ushort) ( 0 ) ;
    ushort __FN_FOREND_VAL__2 = (ushort)2000; 
    int __FN_FORSTEP_VAL__2 = (int)1; 
    for ( I  = __FN_FORSTART_VAL__2; (__FN_FORSTEP_VAL__2 > 0)  ? ( (I  >= __FN_FORSTART_VAL__2) && (I  <= __FN_FOREND_VAL__2) ) : ( (I  <= __FN_FORSTART_VAL__2) && (I  >= __FN_FOREND_VAL__2) ) ; I  += (ushort)__FN_FORSTEP_VAL__2) 
        { 
        __context__.SourceCodeLine = 1990;
        PHONEBOOKENTRY [ I] . PARENTFOLDER = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 1991;
        PHONEBOOKENTRY [ I] . FIRSTCHILD = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 1992;
        Functions.ClearBuffer ( PHONEBOOKENTRY [ I] . ENTRYNAME ) ; 
        __context__.SourceCodeLine = 1993;
        Functions.ClearBuffer ( PHONEBOOKENTRY [ I] . FOLDERID ) ; 
        __context__.SourceCodeLine = 1994;
        PHONEBOOKENTRY [ I] . CHILDCOUNT = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 1988;
        } 
    
    __context__.SourceCodeLine = 1997;
    ushort __FN_FORSTART_VAL__3 = (ushort) ( 0 ) ;
    ushort __FN_FOREND_VAL__3 = (ushort)4000; 
    int __FN_FORSTEP_VAL__3 = (int)1; 
    for ( I  = __FN_FORSTART_VAL__3; (__FN_FORSTEP_VAL__3 > 0)  ? ( (I  >= __FN_FORSTART_VAL__3) && (I  <= __FN_FOREND_VAL__3) ) : ( (I  <= __FN_FORSTART_VAL__3) && (I  >= __FN_FOREND_VAL__3) ) ; I  += (ushort)__FN_FORSTEP_VAL__3) 
        { 
        __context__.SourceCodeLine = 1999;
        CONTACT [ I] . CALLTYPE = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 2000;
        Functions.ClearBuffer ( CONTACT [ I] . CONTACTNUMBER ) ; 
        __context__.SourceCodeLine = 2001;
        Functions.ClearBuffer ( CONTACT [ I] . CALLRATE ) ; 
        __context__.SourceCodeLine = 1997;
        } 
    
    __context__.SourceCodeLine = 2003;
    PHONEBOOKENTRY [ 0] . FIRSTCHILD = (ushort) ( 1 ) ; 
    
    }
    
private void SETPHONEBOOKOFFSET (  SplusExecutionContext __context__, ushort NEWOFFSET ) 
    { 
    
    __context__.SourceCodeLine = 2008;
    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (INTERNALENTRYOFFSET != NEWOFFSET))  ) ) 
        { 
        __context__.SourceCodeLine = 2010;
        INTERNALENTRYOFFSET = (ushort) ( NEWOFFSET ) ; 
        } 
    
    __context__.SourceCodeLine = 2012;
    CURRENTFIRSTPHONEBOOKITEMNUMBER  .Value = (ushort) ( NEWOFFSET ) ; 
    __context__.SourceCodeLine = 2013;
    CURRENTLASTPHONEBOOKITEMNUMBER  .Value = (ushort) ( CURRENTFIRSTPHONEBOOKITEMNUMBER  .Value ) ; 
    
    }
    
object SETVOLUMELEVEL_OnChange_1 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 2021;
        MakeString ( DEVICE_TX__DOLLAR__ , "xConfiguration Audio Volume: {0}\r\n", Functions.ItoA (  (int) ( SETVOLUMELEVEL  .UshortValue ) ) ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DEVICE_RX__DOLLAR___OnChange_2 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 2027;
        try 
            { 
            __context__.SourceCodeLine = 2029;
            while ( Functions.TestForTrue  ( ( 1)  ) ) 
                { 
                __context__.SourceCodeLine = 2031;
                TEMPSTRING__DOLLAR__  .UpdateValue ( Functions.Gather ( "\u000D\u000A" , DEVICE_RX__DOLLAR__ )  ) ; 
                __context__.SourceCodeLine = 2032;
                IPROCESS = (ushort) ( 1 ) ; 
                __context__.SourceCodeLine = 2033;
                PROCESSFEEDBACK (  __context__  ) ; 
                __context__.SourceCodeLine = 2029;
                } 
            
            } 
        
        catch (Exception __splus_exception__)
            { 
            SimplPlusException __splus_exceptionobj__ = new SimplPlusException(__splus_exception__, this );
            
            __context__.SourceCodeLine = 2038;
            SEMAPHORE = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 2039;
            GenerateUserNotice ( "Error in Cisco C90+ C60 Call Control v2.5: {0}", Functions.GetExceptionMessage (  __splus_exceptionobj__ ) ) ; 
            
            }
            
            
            
        }
        catch(Exception e) { ObjectCatchHandler(e); }
        finally { ObjectFinallyHandler( __SignalEventArg__ ); }
        return this;
        
    }
    

object ENTRIESPERPAGE_OnChange_3 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 2061;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( ENTRIESPERPAGE  .UshortValue > 0 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( ENTRIESPERPAGE  .UshortValue <= 20 ) )) ))  ) ) 
            { 
            __context__.SourceCodeLine = 2063;
            IENTRIESPERPAGE = (ushort) ( ENTRIESPERPAGE  .UshortValue ) ; 
            } 
        
        else 
            { 
            __context__.SourceCodeLine = 2067;
            IENTRIESPERPAGE = (ushort) ( 6 ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object PAGESSELECTEDENTRYNUMBER_OnChange_4 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        ushort ITEMPSELECTEDENTRY = 0;
        ushort I = 0;
        
        ushort URIENTRIESFOUND = 0;
        
        ushort FOUNDH323 = 0;
        
        ushort FOUNDSIP = 0;
        
        
        __context__.SourceCodeLine = 2078;
        URIENTRIESFOUND = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 2079;
        FOUNDH323 = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 2080;
        FOUNDSIP = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 2082;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (RECENTCALLSDOWNLOADBUSY  .Value == 0) ) && Functions.TestForTrue ( Functions.BoolToInt (DIRECTORYDOWNLOADBUSY  .Value == 0) )) ))  ) ) 
            { 
            __context__.SourceCodeLine = 2084;
            RESETSELECTEDPHONEBOOKITEM (  __context__  ) ; 
            __context__.SourceCodeLine = 2086;
            ITEMPSELECTEDENTRY = (ushort) ( (PAGESSELECTEDENTRYNUMBER  .UshortValue + ((ICURRENTPAGE - 1) * IENTRIESPERPAGE)) ) ; 
            __context__.SourceCodeLine = 2087;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (SEARCHACTIVE == 1))  ) ) 
                { 
                __context__.SourceCodeLine = 2089;
                SELECTEDENTRY = (ushort) ( SEARCHRESULT[ ITEMPSELECTEDENTRY ] ) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 2091;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( ITEMPSELECTEDENTRY <= PHONEBOOKENTRY[ SELECTEDFOLDER ].CHILDCOUNT ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 2093;
                    SELECTEDENTRY = (ushort) ( ((ITEMPSELECTEDENTRY + PHONEBOOKENTRY[ SELECTEDFOLDER ].FIRSTCHILD) - 1) ) ; 
                    } 
                
                else 
                    { 
                    __context__.SourceCodeLine = 2097;
                    SELECTEDENTRY = (ushort) ( 0 ) ; 
                    } 
                
                }
            
            __context__.SourceCodeLine = 2099;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( SELECTEDENTRY > 0 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( Functions.Length( PHONEBOOKENTRY[ SELECTEDENTRY ].ENTRYNAME ) > 0 ) )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 2101;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( PHONEBOOKENTRY[ SELECTEDENTRY ].FOLDERID ) > 0 ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 2105;
                    SELECTEDFOLDER = (ushort) ( SELECTEDENTRY ) ; 
                    __context__.SourceCodeLine = 2106;
                    DIRECTORYONTOPLEVEL  .Value = (ushort) ( 0 ) ; 
                    __context__.SourceCodeLine = 2107;
                    PHONEBOOK_SELECTED_FOLDER_NAME  .UpdateValue ( PHONEBOOKENTRY [ SELECTEDFOLDER] . ENTRYNAME  ) ; 
                    __context__.SourceCodeLine = 2108;
                    SELECTEDISFOLDER  .Value = (ushort) ( 1 ) ; 
                    __context__.SourceCodeLine = 2109;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( SSEARCHTEXT ) > 0 ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 2111;
                        PHONEBOOK_SEARCH_TEXT  .UpdateValue ( ""  ) ; 
                        } 
                    
                    __context__.SourceCodeLine = 2113;
                    ICURRENTPAGE = (ushort) ( 1 ) ; 
                    __context__.SourceCodeLine = 2114;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Mod( PHONEBOOKENTRY[ SELECTEDFOLDER ].CHILDCOUNT , IENTRIESPERPAGE ) > 0 ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 2116;
                        ITOTALPAGES = (ushort) ( ((PHONEBOOKENTRY[ SELECTEDFOLDER ].CHILDCOUNT / IENTRIESPERPAGE) + 1) ) ; 
                        } 
                    
                    else 
                        { 
                        __context__.SourceCodeLine = 2120;
                        ITOTALPAGES = (ushort) ( (PHONEBOOKENTRY[ SELECTEDFOLDER ].CHILDCOUNT / IENTRIESPERPAGE) ) ; 
                        } 
                    
                    __context__.SourceCodeLine = 2122;
                    PHONEBOOKPAGESCURRENTPAGE  .Value = (ushort) ( ICURRENTPAGE ) ; 
                    __context__.SourceCodeLine = 2123;
                    PHONEBOOKPAGESTOTALPAGES  .Value = (ushort) ( ITOTALPAGES ) ; 
                    __context__.SourceCodeLine = 2124;
                    FDISPLAYNEWPAGE (  __context__  ) ; 
                    __context__.SourceCodeLine = 2125;
                    DISPLAYPHONEBOOK (  __context__  ) ; 
                    } 
                
                else 
                    { 
                    __context__.SourceCodeLine = 2130;
                    SELECTEDPHONEBOOK_NAME  .UpdateValue ( PHONEBOOKENTRY [ SELECTEDENTRY] . ENTRYNAME  ) ; 
                    __context__.SourceCodeLine = 2132;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (PHONEBOOKENTRY[ SELECTEDENTRY ].CHILDCOUNT == 1))  ) ) 
                        { 
                        __context__.SourceCodeLine = 2134;
                        SELECTEDENTRYMETHOD = (ushort) ( PHONEBOOKENTRY[ SELECTEDENTRY ].FIRSTCHILD ) ; 
                        } 
                    
                    else 
                        { 
                        __context__.SourceCodeLine = 2139;
                        ushort __FN_FORSTART_VAL__1 = (ushort) ( PHONEBOOKENTRY[ SELECTEDENTRY ].FIRSTCHILD ) ;
                        ushort __FN_FOREND_VAL__1 = (ushort)((PHONEBOOKENTRY[ SELECTEDENTRY ].FIRSTCHILD + PHONEBOOKENTRY[ SELECTEDENTRY ].CHILDCOUNT) - 1); 
                        int __FN_FORSTEP_VAL__1 = (int)1; 
                        for ( I  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (I  >= __FN_FORSTART_VAL__1) && (I  <= __FN_FOREND_VAL__1) ) : ( (I  <= __FN_FORSTART_VAL__1) && (I  >= __FN_FOREND_VAL__1) ) ; I  += (ushort)__FN_FORSTEP_VAL__1) 
                            { 
                            __context__.SourceCodeLine = 2141;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Find( "@" , CONTACT[ I ].CONTACTNUMBER ) > 0 ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 2143;
                                SELECTEDENTRYMETHOD = (ushort) ( I ) ; 
                                __context__.SourceCodeLine = 2144;
                                URIENTRIESFOUND = (ushort) ( (URIENTRIESFOUND + 1) ) ; 
                                __context__.SourceCodeLine = 2145;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( (CONTACT[ I ].CALLTYPE & 2) ) && Functions.TestForTrue ( Functions.BoolToInt (FOUNDSIP == 0) )) ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 2147;
                                    FOUNDSIP = (ushort) ( I ) ; 
                                    } 
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 2149;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( (CONTACT[ I ].CALLTYPE & 0) ) && Functions.TestForTrue ( Functions.BoolToInt (FOUNDH323 == 0) )) ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 2151;
                                        FOUNDH323 = (ushort) ( I ) ; 
                                        } 
                                    
                                    }
                                
                                } 
                            
                            __context__.SourceCodeLine = 2139;
                            } 
                        
                        __context__.SourceCodeLine = 2155;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (URIENTRIESFOUND == 0))  ) ) 
                            { 
                            __context__.SourceCodeLine = 2157;
                            ushort __FN_FORSTART_VAL__2 = (ushort) ( PHONEBOOKENTRY[ SELECTEDENTRY ].FIRSTCHILD ) ;
                            ushort __FN_FOREND_VAL__2 = (ushort)((PHONEBOOKENTRY[ SELECTEDENTRY ].FIRSTCHILD + PHONEBOOKENTRY[ SELECTEDENTRY ].CHILDCOUNT) - 1); 
                            int __FN_FORSTEP_VAL__2 = (int)1; 
                            for ( I  = __FN_FORSTART_VAL__2; (__FN_FORSTEP_VAL__2 > 0)  ? ( (I  >= __FN_FORSTART_VAL__2) && (I  <= __FN_FOREND_VAL__2) ) : ( (I  <= __FN_FORSTART_VAL__2) && (I  >= __FN_FOREND_VAL__2) ) ; I  += (ushort)__FN_FORSTEP_VAL__2) 
                                { 
                                __context__.SourceCodeLine = 2159;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( (CONTACT[ I ].CALLTYPE & 2) ) && Functions.TestForTrue ( Functions.BoolToInt (FOUNDSIP == 0) )) ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 2161;
                                    FOUNDSIP = (ushort) ( I ) ; 
                                    } 
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 2163;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( (CONTACT[ I ].CALLTYPE & 0) ) && Functions.TestForTrue ( Functions.BoolToInt (FOUNDH323 == 0) )) ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 2165;
                                        FOUNDH323 = (ushort) ( I ) ; 
                                        } 
                                    
                                    }
                                
                                __context__.SourceCodeLine = 2157;
                                } 
                            
                            } 
                        
                        __context__.SourceCodeLine = 2169;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (URIENTRIESFOUND != 1))  ) ) 
                            { 
                            __context__.SourceCodeLine = 2171;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (DEFAULTCALLTYPESIP  .Value == 1) ) || Functions.TestForTrue ( Functions.BoolToInt (FOUNDH323 == 0) )) ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 2173;
                                SELECTEDENTRYMETHOD = (ushort) ( FOUNDSIP ) ; 
                                } 
                            
                            else 
                                { 
                                __context__.SourceCodeLine = 2177;
                                SELECTEDENTRYMETHOD = (ushort) ( FOUNDH323 ) ; 
                                } 
                            
                            } 
                        
                        } 
                    
                    __context__.SourceCodeLine = 2181;
                    SELECTEDPHONEBOOK_NUMBER  .UpdateValue ( CONTACT [ SELECTEDENTRYMETHOD] . CONTACTNUMBER  ) ; 
                    __context__.SourceCodeLine = 2182;
                    SELECTEDPHONEBOOK_CALLRATE  .UpdateValue ( CONTACT [ SELECTEDENTRYMETHOD] . CALLRATE  ) ; 
                    __context__.SourceCodeLine = 2183;
                    SELECTEDISCONTACT  .Value = (ushort) ( 1 ) ; 
                    } 
                
                } 
            
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object SELECTEDENTRYNUMBER_OnChange_5 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        ushort I = 0;
        
        ushort URIENTRIESFOUND = 0;
        
        ushort FOUNDH323 = 0;
        
        ushort FOUNDSIP = 0;
        
        
        __context__.SourceCodeLine = 2196;
        URIENTRIESFOUND = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 2197;
        FOUNDH323 = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 2198;
        FOUNDSIP = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 2200;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (RECENTCALLSDOWNLOADBUSY  .Value == 0) ) && Functions.TestForTrue ( Functions.BoolToInt (DIRECTORYDOWNLOADBUSY  .Value == 0) )) ))  ) ) 
            { 
            __context__.SourceCodeLine = 2202;
            RESETSELECTEDPHONEBOOKITEM (  __context__  ) ; 
            __context__.SourceCodeLine = 2204;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (SEARCHACTIVE == 1))  ) ) 
                { 
                __context__.SourceCodeLine = 2206;
                SELECTEDENTRY = (ushort) ( SEARCHRESULT[ SELECTEDENTRYNUMBER  .UshortValue ] ) ; 
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 2210;
                SELECTEDENTRY = (ushort) ( ((SELECTEDENTRYNUMBER  .UshortValue + PHONEBOOKENTRY[ SELECTEDFOLDER ].FIRSTCHILD) - 1) ) ; 
                } 
            
            __context__.SourceCodeLine = 2212;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( SELECTEDENTRYNUMBER  .UshortValue > 0 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( PHONEBOOKENTRY[ SELECTEDFOLDER ].FIRSTCHILD <= 2000 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( Functions.Length( PHONEBOOKENTRY[ SELECTEDENTRY ].ENTRYNAME ) > 0 ) )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 2214;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( PHONEBOOKENTRY[ SELECTEDENTRY ].FOLDERID ) > 0 ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 2218;
                    SELECTEDFOLDER = (ushort) ( SELECTEDENTRY ) ; 
                    __context__.SourceCodeLine = 2219;
                    DIRECTORYONTOPLEVEL  .Value = (ushort) ( 0 ) ; 
                    __context__.SourceCodeLine = 2220;
                    PHONEBOOK_SELECTED_FOLDER_NAME  .UpdateValue ( PHONEBOOKENTRY [ SELECTEDFOLDER] . ENTRYNAME  ) ; 
                    __context__.SourceCodeLine = 2221;
                    SELECTEDISFOLDER  .Value = (ushort) ( 1 ) ; 
                    __context__.SourceCodeLine = 2222;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( SSEARCHTEXT ) > 0 ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 2224;
                        PHONEBOOK_SEARCH_TEXT  .UpdateValue ( ""  ) ; 
                        } 
                    
                    __context__.SourceCodeLine = 2226;
                    ICURRENTPAGE = (ushort) ( 1 ) ; 
                    __context__.SourceCodeLine = 2227;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Mod( PHONEBOOKENTRY[ SELECTEDFOLDER ].CHILDCOUNT , IENTRIESPERPAGE ) > 0 ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 2229;
                        ITOTALPAGES = (ushort) ( ((PHONEBOOKENTRY[ SELECTEDFOLDER ].CHILDCOUNT / IENTRIESPERPAGE) + 1) ) ; 
                        } 
                    
                    else 
                        { 
                        __context__.SourceCodeLine = 2233;
                        ITOTALPAGES = (ushort) ( (PHONEBOOKENTRY[ SELECTEDFOLDER ].CHILDCOUNT / IENTRIESPERPAGE) ) ; 
                        } 
                    
                    __context__.SourceCodeLine = 2235;
                    PHONEBOOKPAGESCURRENTPAGE  .Value = (ushort) ( ICURRENTPAGE ) ; 
                    __context__.SourceCodeLine = 2236;
                    PHONEBOOKPAGESTOTALPAGES  .Value = (ushort) ( ITOTALPAGES ) ; 
                    __context__.SourceCodeLine = 2237;
                    FDISPLAYNEWPAGE (  __context__  ) ; 
                    __context__.SourceCodeLine = 2238;
                    DISPLAYPHONEBOOK (  __context__  ) ; 
                    } 
                
                else 
                    { 
                    __context__.SourceCodeLine = 2243;
                    SELECTEDPHONEBOOK_NAME  .UpdateValue ( PHONEBOOKENTRY [ SELECTEDENTRY] . ENTRYNAME  ) ; 
                    __context__.SourceCodeLine = 2245;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (PHONEBOOKENTRY[ SELECTEDENTRY ].CHILDCOUNT == 1))  ) ) 
                        { 
                        __context__.SourceCodeLine = 2247;
                        SELECTEDENTRYMETHOD = (ushort) ( PHONEBOOKENTRY[ SELECTEDENTRY ].FIRSTCHILD ) ; 
                        } 
                    
                    else 
                        { 
                        __context__.SourceCodeLine = 2252;
                        ushort __FN_FORSTART_VAL__1 = (ushort) ( PHONEBOOKENTRY[ SELECTEDENTRY ].FIRSTCHILD ) ;
                        ushort __FN_FOREND_VAL__1 = (ushort)((PHONEBOOKENTRY[ SELECTEDENTRY ].FIRSTCHILD + PHONEBOOKENTRY[ SELECTEDENTRY ].CHILDCOUNT) - 1); 
                        int __FN_FORSTEP_VAL__1 = (int)1; 
                        for ( I  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (I  >= __FN_FORSTART_VAL__1) && (I  <= __FN_FOREND_VAL__1) ) : ( (I  <= __FN_FORSTART_VAL__1) && (I  >= __FN_FOREND_VAL__1) ) ; I  += (ushort)__FN_FORSTEP_VAL__1) 
                            { 
                            __context__.SourceCodeLine = 2254;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Find( "@" , CONTACT[ I ].CONTACTNUMBER ) > 0 ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 2256;
                                SELECTEDENTRYMETHOD = (ushort) ( I ) ; 
                                __context__.SourceCodeLine = 2257;
                                URIENTRIESFOUND = (ushort) ( (URIENTRIESFOUND + 1) ) ; 
                                __context__.SourceCodeLine = 2258;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( (CONTACT[ I ].CALLTYPE & 2) ) && Functions.TestForTrue ( Functions.BoolToInt (FOUNDSIP == 0) )) ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 2260;
                                    FOUNDSIP = (ushort) ( I ) ; 
                                    } 
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 2262;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( (CONTACT[ I ].CALLTYPE & 0) ) && Functions.TestForTrue ( Functions.BoolToInt (FOUNDH323 == 0) )) ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 2264;
                                        FOUNDH323 = (ushort) ( I ) ; 
                                        } 
                                    
                                    }
                                
                                } 
                            
                            __context__.SourceCodeLine = 2252;
                            } 
                        
                        __context__.SourceCodeLine = 2268;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (URIENTRIESFOUND == 0))  ) ) 
                            { 
                            __context__.SourceCodeLine = 2270;
                            ushort __FN_FORSTART_VAL__2 = (ushort) ( PHONEBOOKENTRY[ SELECTEDENTRY ].FIRSTCHILD ) ;
                            ushort __FN_FOREND_VAL__2 = (ushort)((PHONEBOOKENTRY[ SELECTEDENTRY ].FIRSTCHILD + PHONEBOOKENTRY[ SELECTEDENTRY ].CHILDCOUNT) - 1); 
                            int __FN_FORSTEP_VAL__2 = (int)1; 
                            for ( I  = __FN_FORSTART_VAL__2; (__FN_FORSTEP_VAL__2 > 0)  ? ( (I  >= __FN_FORSTART_VAL__2) && (I  <= __FN_FOREND_VAL__2) ) : ( (I  <= __FN_FORSTART_VAL__2) && (I  >= __FN_FOREND_VAL__2) ) ; I  += (ushort)__FN_FORSTEP_VAL__2) 
                                { 
                                __context__.SourceCodeLine = 2272;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( (CONTACT[ I ].CALLTYPE & 2) ) && Functions.TestForTrue ( Functions.BoolToInt (FOUNDSIP == 0) )) ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 2274;
                                    FOUNDSIP = (ushort) ( I ) ; 
                                    } 
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 2276;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( (CONTACT[ I ].CALLTYPE & 0) ) && Functions.TestForTrue ( Functions.BoolToInt (FOUNDH323 == 0) )) ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 2278;
                                        FOUNDH323 = (ushort) ( I ) ; 
                                        } 
                                    
                                    }
                                
                                __context__.SourceCodeLine = 2270;
                                } 
                            
                            } 
                        
                        __context__.SourceCodeLine = 2282;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (URIENTRIESFOUND != 1))  ) ) 
                            { 
                            __context__.SourceCodeLine = 2284;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (DEFAULTCALLTYPESIP  .Value == 1) ) || Functions.TestForTrue ( Functions.BoolToInt (FOUNDH323 == 0) )) ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 2286;
                                SELECTEDENTRYMETHOD = (ushort) ( FOUNDSIP ) ; 
                                } 
                            
                            else 
                                { 
                                __context__.SourceCodeLine = 2290;
                                SELECTEDENTRYMETHOD = (ushort) ( FOUNDH323 ) ; 
                                } 
                            
                            } 
                        
                        } 
                    
                    __context__.SourceCodeLine = 2294;
                    SELECTEDPHONEBOOK_NUMBER  .UpdateValue ( CONTACT [ SELECTEDENTRYMETHOD] . CONTACTNUMBER  ) ; 
                    __context__.SourceCodeLine = 2295;
                    SELECTEDPHONEBOOK_CALLRATE  .UpdateValue ( CONTACT [ SELECTEDENTRYMETHOD] . CALLRATE  ) ; 
                    __context__.SourceCodeLine = 2297;
                    SELECTEDISCONTACT  .Value = (ushort) ( 1 ) ; 
                    } 
                
                } 
            
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object CLEARSELECTEDENTRYNUMBER_OnPush_6 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 2305;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (RECENTCALLSDOWNLOADBUSY  .Value == 0) ) && Functions.TestForTrue ( Functions.BoolToInt (DIRECTORYDOWNLOADBUSY  .Value == 0) )) ))  ) ) 
            { 
            __context__.SourceCodeLine = 2307;
            RESETSELECTEDPHONEBOOKITEM (  __context__  ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object GETLOCALPHONEBOOK_OnPush_7 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 2313;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (RECENTCALLSDOWNLOADBUSY  .Value == 0) ) && Functions.TestForTrue ( Functions.BoolToInt (DIRECTORYDOWNLOADBUSY  .Value == 0) )) ))  ) ) 
            { 
            __context__.SourceCodeLine = 2315;
            DIRECTORYDOWNLOADBUSY  .Value = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 2316;
            RESETEVERYTHING (  __context__  ) ; 
            __context__.SourceCodeLine = 2317;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (PHONEBOOKSOURCEID  .UshortValue == 2))  ) ) 
                { 
                __context__.SourceCodeLine = 2319;
                PHONEBOOKTYPE  .UpdateValue ( "Corporate"  ) ; 
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 2323;
                PHONEBOOKTYPE  .UpdateValue ( "Local"  ) ; 
                } 
            
            __context__.SourceCodeLine = 2325;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (ENABLERECENTCALLS  .Value == 1))  ) ) 
                { 
                __context__.SourceCodeLine = 2327;
                PHONEBOOKENTRY [ 1] . PARENTFOLDER = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 2328;
                PHONEBOOKENTRY [ 1] . ENTRYNAME  .UpdateValue ( "Recent Calls"  ) ; 
                __context__.SourceCodeLine = 2329;
                PHONEBOOKENTRY [ 1] . FIRSTCHILD = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 2330;
                PHONEBOOKENTRY [ 1] . FOLDERID  .UpdateValue ( "crestron_0"  ) ; 
                __context__.SourceCodeLine = 2331;
                PHONEBOOKENTRY [ 1] . CHILDCOUNT = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 2332;
                PHONEBOOKENTRY [ 0] . FIRSTCHILD = (ushort) ( 1 ) ; 
                __context__.SourceCodeLine = 2333;
                PHONEBOOKENTRY [ 0] . CHILDCOUNT = (ushort) ( 1 ) ; 
                __context__.SourceCodeLine = 2334;
                NEXTAVAILABLEDIRENTRY = (ushort) ( 1 ) ; 
                } 
            
            __context__.SourceCodeLine = 2336;
            RUNSEARCHPHONEBOOK (  __context__  ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object GETRECENTCALLS_OnPush_8 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 2342;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (DIRECTORYDOWNLOADBUSY  .Value == 0) ) && Functions.TestForTrue ( Functions.BoolToInt (BGETRECENTSONLY == 0) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt (ENABLERECENTCALLS  .Value == 1) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt (PHONEBOOKENTRY[ 1 ].FOLDERID == "crestron_0") )) ))  ) ) 
            { 
            __context__.SourceCodeLine = 2344;
            RESETRECENTSONLY (  __context__  ) ; 
            __context__.SourceCodeLine = 2345;
            BGETRECENTSONLY = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 2346;
            RECENTCALLSDOWNLOADBUSY  .Value = (ushort) ( BGETRECENTSONLY ) ; 
            __context__.SourceCodeLine = 2347;
            INTERNALENTRYOFFSET = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 2348;
            INTERNALPAGESENTRYOFFSET = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 2349;
            RUNSEARCHPHONEBOOK (  __context__  ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DIALSELECTEDPHONEBOOKENTRYNUMBER_OnPush_9 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString DIALSTRING;
        DIALSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 220, this );
        
        
        __context__.SourceCodeLine = 2361;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (SELECTEDENTRYMETHOD != 0) ) && Functions.TestForTrue ( Functions.BoolToInt ( Functions.Length( CONTACT[ SELECTEDENTRYMETHOD ].CONTACTNUMBER ) > 0 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt (DIRECTORYDOWNLOADBUSY  .Value == 0) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt (RECENTCALLSDOWNLOADBUSY  .Value == 0) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt (CONTACT[ SELECTEDENTRYMETHOD ].CALLTYPE != 3) )) ))  ) ) 
            { 
            __context__.SourceCodeLine = 2363;
            MakeString ( DIALSTRING , "xCommand Dial Number: \"{0}\"", CONTACT [ SELECTEDENTRYMETHOD] . CONTACTNUMBER ) ; 
            __context__.SourceCodeLine = 2364;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( CONTACT[ SELECTEDENTRYMETHOD ].CALLRATE ) > 0 ))  ) ) 
                { 
                __context__.SourceCodeLine = 2366;
                DIALSTRING  .UpdateValue ( DIALSTRING + " CallRate: " + CONTACT [ SELECTEDENTRYMETHOD] . CALLRATE  ) ; 
                } 
            
            __context__.SourceCodeLine = 2369;
            if ( Functions.TestForTrue  ( ( (CONTACT[ SELECTEDENTRYMETHOD ].CALLTYPE & 2))  ) ) 
                { 
                __context__.SourceCodeLine = 2371;
                DIALSTRING  .UpdateValue ( DIALSTRING + " Protocol: SIP"  ) ; 
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 2375;
                DIALSTRING  .UpdateValue ( DIALSTRING + " Protocol: H323"  ) ; 
                } 
            
            __context__.SourceCodeLine = 2378;
            if ( Functions.TestForTrue  ( ( (CONTACT[ SELECTEDENTRYMETHOD ].CALLTYPE & 1))  ) ) 
                { 
                __context__.SourceCodeLine = 2380;
                DIALSTRING  .UpdateValue ( DIALSTRING + " CallType: Audio"  ) ; 
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 2384;
                DIALSTRING  .UpdateValue ( DIALSTRING + " CallType: Video"  ) ; 
                } 
            
            __context__.SourceCodeLine = 2386;
            MakeString ( DEVICE_TX__DOLLAR__ , "{0}\r\n", DIALSTRING ) ; 
            __context__.SourceCodeLine = 2388;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( PHONEBOOKENTRY[ SELECTEDENTRY ].ENTRYNAME ) > 0 ))  ) ) 
                { 
                __context__.SourceCodeLine = 2390;
                CALLHISTORYNAME  .UpdateValue ( PHONEBOOKENTRY [ SELECTEDENTRY] . ENTRYNAME  ) ; 
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 2394;
                CALLHISTORYNAME  .UpdateValue ( CONTACT [ SELECTEDENTRYMETHOD] . CONTACTNUMBER  ) ; 
                } 
            
            __context__.SourceCodeLine = 2396;
            CALLHISTORYNUMBER  .UpdateValue ( CONTACT [ SELECTEDENTRYMETHOD] . CONTACTNUMBER  ) ; 
            } 
        
        else 
            {
            __context__.SourceCodeLine = 2398;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (SELECTEDENTRYMETHOD != 0) ) && Functions.TestForTrue ( Functions.BoolToInt ( Functions.Length( CONTACT[ SELECTEDENTRYMETHOD ].CONTACTNUMBER ) > 0 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt (DIRECTORYDOWNLOADBUSY  .Value == 0) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt (RECENTCALLSDOWNLOADBUSY  .Value == 0) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt (CONTACT[ SELECTEDENTRYMETHOD ].CALLTYPE == 3) )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 2400;
                MakeString ( DEVICE_TX__DOLLAR__ , "xcommand Dial Number: {0}\r\n", CONTACT [ SELECTEDENTRYMETHOD] . CONTACTNUMBER ) ; 
                } 
            
            }
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object SEARCHPHONEBOOK_OnRelease_10 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 2406;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (RECENTCALLSDOWNLOADBUSY  .Value == 0) ) && Functions.TestForTrue ( Functions.BoolToInt (DIRECTORYDOWNLOADBUSY  .Value == 0) )) ))  ) ) 
            { 
            __context__.SourceCodeLine = 2408;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( PHONEBOOKSEARCHSTRING ) > 0 ))  ) ) 
                { 
                __context__.SourceCodeLine = 2410;
                SEARCHACTIVE = (ushort) ( 1 ) ; 
                __context__.SourceCodeLine = 2411;
                SEARCH_IS_ACTIVE  .Value = (ushort) ( SEARCHACTIVE ) ; 
                __context__.SourceCodeLine = 2412;
                GLOBALSEARCHCOUNT = (ushort) ( (GLOBALSEARCHCOUNT + 1) ) ; 
                __context__.SourceCodeLine = 2413;
                PHONEBOOKSEARCH (  __context__ , (ushort)( GLOBALSEARCHCOUNT )) ; 
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 2417;
                SEARCHACTIVE = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 2418;
                SEARCH_IS_ACTIVE  .Value = (ushort) ( SEARCHACTIVE ) ; 
                __context__.SourceCodeLine = 2419;
                ICURRENTPAGE = (ushort) ( 1 ) ; 
                __context__.SourceCodeLine = 2420;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Mod( PHONEBOOKENTRY[ SELECTEDFOLDER ].CHILDCOUNT , IENTRIESPERPAGE ) > 0 ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 2422;
                    ITOTALPAGES = (ushort) ( ((PHONEBOOKENTRY[ SELECTEDFOLDER ].CHILDCOUNT / IENTRIESPERPAGE) + 1) ) ; 
                    } 
                
                else 
                    { 
                    __context__.SourceCodeLine = 2426;
                    ITOTALPAGES = (ushort) ( (PHONEBOOKENTRY[ SELECTEDFOLDER ].CHILDCOUNT / IENTRIESPERPAGE) ) ; 
                    } 
                
                __context__.SourceCodeLine = 2428;
                PHONEBOOKPAGESCURRENTPAGE  .Value = (ushort) ( ICURRENTPAGE ) ; 
                __context__.SourceCodeLine = 2429;
                PHONEBOOKPAGESTOTALPAGES  .Value = (ushort) ( ITOTALPAGES ) ; 
                __context__.SourceCodeLine = 2430;
                FDISPLAYNEWPAGE (  __context__  ) ; 
                __context__.SourceCodeLine = 2431;
                DISPLAYPHONEBOOK (  __context__  ) ; 
                } 
            
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object PHONEBOOKSEARCHSTRING_OnChange_11 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 2438;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (SSEARCHTEXT != PHONEBOOKSEARCHSTRING) ) && Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (RECENTCALLSDOWNLOADBUSY  .Value == 0) ) && Functions.TestForTrue ( Functions.BoolToInt (DIRECTORYDOWNLOADBUSY  .Value == 0) )) ) )) ))  ) ) 
            { 
            __context__.SourceCodeLine = 2440;
            SSEARCHTEXT  .UpdateValue ( PHONEBOOKSEARCHSTRING  ) ; 
            __context__.SourceCodeLine = 2441;
            PHONEBOOK_SEARCH_TEXT  .UpdateValue ( SSEARCHTEXT  ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object TOPLEVEL_OnPush_12 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 2447;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (RECENTCALLSDOWNLOADBUSY  .Value == 0) ) && Functions.TestForTrue ( Functions.BoolToInt (DIRECTORYDOWNLOADBUSY  .Value == 0) )) ))  ) ) 
            { 
            __context__.SourceCodeLine = 2449;
            RESETSELECTEDPHONEBOOKITEM (  __context__  ) ; 
            __context__.SourceCodeLine = 2450;
            SELECTEDFOLDER = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 2451;
            DIRECTORYONTOPLEVEL  .Value = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 2452;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( SSEARCHTEXT ) > 0 ))  ) ) 
                { 
                __context__.SourceCodeLine = 2454;
                PHONEBOOK_SEARCH_TEXT  .UpdateValue ( ""  ) ; 
                } 
            
            __context__.SourceCodeLine = 2456;
            PHONEBOOK_SELECTED_FOLDER_NAME  .UpdateValue ( ""  ) ; 
            __context__.SourceCodeLine = 2457;
            ICURRENTPAGE = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 2458;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Mod( PHONEBOOKENTRY[ SELECTEDFOLDER ].CHILDCOUNT , IENTRIESPERPAGE ) > 0 ))  ) ) 
                { 
                __context__.SourceCodeLine = 2460;
                ITOTALPAGES = (ushort) ( ((PHONEBOOKENTRY[ SELECTEDFOLDER ].CHILDCOUNT / IENTRIESPERPAGE) + 1) ) ; 
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 2464;
                ITOTALPAGES = (ushort) ( (PHONEBOOKENTRY[ SELECTEDFOLDER ].CHILDCOUNT / IENTRIESPERPAGE) ) ; 
                } 
            
            __context__.SourceCodeLine = 2466;
            PHONEBOOKPAGESCURRENTPAGE  .Value = (ushort) ( ICURRENTPAGE ) ; 
            __context__.SourceCodeLine = 2467;
            PHONEBOOKPAGESTOTALPAGES  .Value = (ushort) ( ITOTALPAGES ) ; 
            __context__.SourceCodeLine = 2468;
            FDISPLAYNEWPAGE (  __context__  ) ; 
            __context__.SourceCodeLine = 2469;
            DISPLAYPHONEBOOK (  __context__  ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object UPONELEVEL_OnPush_13 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 2475;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (RECENTCALLSDOWNLOADBUSY  .Value == 0) ) && Functions.TestForTrue ( Functions.BoolToInt (DIRECTORYDOWNLOADBUSY  .Value == 0) )) ))  ) ) 
            { 
            __context__.SourceCodeLine = 2477;
            RESETSELECTEDPHONEBOOKITEM (  __context__  ) ; 
            __context__.SourceCodeLine = 2478;
            SELECTEDFOLDER = (ushort) ( PHONEBOOKENTRY[ SELECTEDFOLDER ].PARENTFOLDER ) ; 
            __context__.SourceCodeLine = 2479;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (SELECTEDFOLDER == 0))  ) ) 
                { 
                __context__.SourceCodeLine = 2481;
                DIRECTORYONTOPLEVEL  .Value = (ushort) ( 1 ) ; 
                __context__.SourceCodeLine = 2482;
                PHONEBOOK_SELECTED_FOLDER_NAME  .UpdateValue ( ""  ) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 2484;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( PHONEBOOKENTRY[ SELECTEDENTRY ].FOLDERID ) > 0 ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 2486;
                    PHONEBOOK_SELECTED_FOLDER_NAME  .UpdateValue ( PHONEBOOKENTRY [ SELECTEDFOLDER] . ENTRYNAME  ) ; 
                    } 
                
                }
            
            __context__.SourceCodeLine = 2488;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( SSEARCHTEXT ) > 0 ))  ) ) 
                { 
                __context__.SourceCodeLine = 2490;
                PHONEBOOK_SEARCH_TEXT  .UpdateValue ( ""  ) ; 
                } 
            
            __context__.SourceCodeLine = 2492;
            INTERNALENTRYOFFSET = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 2493;
            ICURRENTPAGE = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 2494;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Mod( PHONEBOOKENTRY[ SELECTEDFOLDER ].CHILDCOUNT , IENTRIESPERPAGE ) > 0 ))  ) ) 
                { 
                __context__.SourceCodeLine = 2496;
                ITOTALPAGES = (ushort) ( ((PHONEBOOKENTRY[ SELECTEDFOLDER ].CHILDCOUNT / IENTRIESPERPAGE) + 1) ) ; 
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 2500;
                ITOTALPAGES = (ushort) ( (PHONEBOOKENTRY[ SELECTEDFOLDER ].CHILDCOUNT / IENTRIESPERPAGE) ) ; 
                } 
            
            __context__.SourceCodeLine = 2502;
            PHONEBOOKPAGESCURRENTPAGE  .Value = (ushort) ( ICURRENTPAGE ) ; 
            __context__.SourceCodeLine = 2503;
            PHONEBOOKPAGESTOTALPAGES  .Value = (ushort) ( ITOTALPAGES ) ; 
            __context__.SourceCodeLine = 2504;
            FDISPLAYNEWPAGE (  __context__  ) ; 
            __context__.SourceCodeLine = 2505;
            DISPLAYPHONEBOOK (  __context__  ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object PAGESPHONEBOOKFIRSTPAGE_OnPush_14 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 2511;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (ICURRENTPAGE != 1) ) && Functions.TestForTrue ( Functions.BoolToInt (RECENTCALLSDOWNLOADBUSY  .Value == 0) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt (DIRECTORYDOWNLOADBUSY  .Value == 0) )) ))  ) ) 
            { 
            __context__.SourceCodeLine = 2513;
            RESETSELECTEDPHONEBOOKITEM (  __context__  ) ; 
            __context__.SourceCodeLine = 2514;
            ICURRENTPAGE = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 2515;
            PHONEBOOKPAGESCURRENTPAGE  .Value = (ushort) ( ICURRENTPAGE ) ; 
            __context__.SourceCodeLine = 2516;
            FDISPLAYNEWPAGE (  __context__  ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object PAGESPHONEBOOKNEXTPAGE_OnPush_15 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 2522;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( ICURRENTPAGE < ITOTALPAGES ) ) && Functions.TestForTrue ( Functions.BoolToInt (RECENTCALLSDOWNLOADBUSY  .Value == 0) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt (DIRECTORYDOWNLOADBUSY  .Value == 0) )) ))  ) ) 
            { 
            __context__.SourceCodeLine = 2524;
            RESETSELECTEDPHONEBOOKITEM (  __context__  ) ; 
            __context__.SourceCodeLine = 2525;
            ICURRENTPAGE = (ushort) ( (ICURRENTPAGE + 1) ) ; 
            __context__.SourceCodeLine = 2526;
            PHONEBOOKPAGESCURRENTPAGE  .Value = (ushort) ( ICURRENTPAGE ) ; 
            __context__.SourceCodeLine = 2527;
            FDISPLAYNEWPAGE (  __context__  ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object PAGESPHONEBOOKPREVIOUSPAGE_OnPush_16 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 2533;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( ICURRENTPAGE > 1 ) ) && Functions.TestForTrue ( Functions.BoolToInt (RECENTCALLSDOWNLOADBUSY  .Value == 0) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt (DIRECTORYDOWNLOADBUSY  .Value == 0) )) ))  ) ) 
            { 
            __context__.SourceCodeLine = 2535;
            RESETSELECTEDPHONEBOOKITEM (  __context__  ) ; 
            __context__.SourceCodeLine = 2536;
            ICURRENTPAGE = (ushort) ( (ICURRENTPAGE - 1) ) ; 
            __context__.SourceCodeLine = 2537;
            PHONEBOOKPAGESCURRENTPAGE  .Value = (ushort) ( ICURRENTPAGE ) ; 
            __context__.SourceCodeLine = 2538;
            FDISPLAYNEWPAGE (  __context__  ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DIALCALL_OnPush_17 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString DIALSTRING;
        DIALSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 220, this );
        
        
        __context__.SourceCodeLine = 2546;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( DIALNUMBER ) > 0 ))  ) ) 
            { 
            __context__.SourceCodeLine = 2548;
            MakeString ( DIALSTRING , "xCommand Dial Number: {0}", DIALNUMBER ) ; 
            __context__.SourceCodeLine = 2549;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( DIALCALLRATE ) > 0 ))  ) ) 
                { 
                __context__.SourceCodeLine = 2551;
                DIALSTRING  .UpdateValue ( DIALSTRING + " CallRate: " + DIALCALLRATE  ) ; 
                } 
            
            __context__.SourceCodeLine = 2553;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( DIALCALLPROTOCOL ) > 0 ))  ) ) 
                { 
                __context__.SourceCodeLine = 2555;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Find( "VOIP" , DIALCALLPROTOCOL ) > 0 ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 2557;
                    MakeString ( DIALSTRING , "{0} onlyaudio: true", DIALSTRING ) ; 
                    } 
                
                else 
                    { 
                    __context__.SourceCodeLine = 2561;
                    MakeString ( DIALSTRING , "{0} Protocol: {1}", DIALSTRING , DIALCALLPROTOCOL ) ; 
                    } 
                
                } 
            
            __context__.SourceCodeLine = 2564;
            MakeString ( DEVICE_TX__DOLLAR__ , "{0}\r\n", DIALSTRING ) ; 
            __context__.SourceCodeLine = 2565;
            CALLHISTORYNUMBER  .UpdateValue ( DIALNUMBER  ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DISCONNECTCALLWITHID_OnChange_18 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 2571;
        DISCONNECTCALL (  __context__ , (ushort)( DISCONNECTCALLWITHID  .UshortValue )) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object CALLCONTROLACCEPTINCOMINGCALL_OnPush_19 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 2576;
        MakeString ( DEVICE_TX__DOLLAR__ , "xCommand Call Accept\r\n") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object CALLCONTROLREJECTINCOMINGCALL_OnPush_20 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 2581;
        MakeString ( DEVICE_TX__DOLLAR__ , "xCommand Call Reject\r\n") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DISCONNECTALLCALLS_OnPush_21 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 2586;
        MakeString ( DEVICE_TX__DOLLAR__ , "xCommand Call DisconnectAll\r\n") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DTMFSEND_OnChange_22 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 2592;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (DTMFSENDCALLSLOT  .UshortValue == 0))  ) ) 
            { 
            __context__.SourceCodeLine = 2594;
            MakeString ( DEVICE_TX__DOLLAR__ , "xCommand DTMFSend DTMFString: \"{0}\" CallId: \"{1:d}\"\r\n", DTMFSEND , (short)CALLSTATUS_CALLID[ 1 ] .Value) ; 
            } 
        
        else 
            { 
            __context__.SourceCodeLine = 2598;
            MakeString ( DEVICE_TX__DOLLAR__ , "xCommand DTMFSend DTMFString: \"{0}\" CallId: \"{1:d}\"\r\n", DTMFSEND , (short)CALLSTATUS_CALLID[ DTMFSENDCALLSLOT  .UshortValue ] .Value) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

private void FARENDCONTROLCAMERAMOVE (  SplusExecutionContext __context__, CrestronString DIRECTION ) 
    { 
    
    __context__.SourceCodeLine = 2604;
    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (FARENDCONTROLCALLSLOT  .UshortValue == 0))  ) ) 
        { 
        __context__.SourceCodeLine = 2606;
        MakeString ( DEVICE_TX__DOLLAR__ , "xCommand FarEndControl Camera Move Value: {0}\r\n", DIRECTION ) ; 
        } 
    
    else 
        { 
        __context__.SourceCodeLine = 2610;
        MakeString ( DEVICE_TX__DOLLAR__ , "xCommand FarEndControl Camera Move Value: {0} CallId: \"{1:d}\"\r\n", DIRECTION , (short)CALLSTATUS_CALLID[ FARENDCONTROLCALLSLOT  .UshortValue ] .Value) ; 
        } 
    
    
    }
    
object FARENDCONTROLPRESENTATIONSOURCEID_OnChange_23 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 2616;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (FARENDCONTROLCALLSLOT  .UshortValue == 0))  ) ) 
            { 
            __context__.SourceCodeLine = 2618;
            MakeString ( DEVICE_TX__DOLLAR__ , "xCommand FarEndControl Source Select SourceId: {0:d}\r\n", (short)FARENDCONTROLPRESENTATIONSOURCEID  .UshortValue) ; 
            } 
        
        else 
            { 
            __context__.SourceCodeLine = 2622;
            MakeString ( DEVICE_TX__DOLLAR__ , "xCommand FarEndControl Source Select SourceId: {0:d}  CallId: \"{1:d}\"\r\n", (short)FARENDCONTROLPRESENTATIONSOURCEID  .UshortValue, (short)CALLSTATUS_CALLID[ FARENDCONTROLCALLSLOT  .UshortValue ] .Value) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object FARENDCONTROLPRESETACTIVATEID_OnChange_24 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 2628;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (FARENDCONTROLCALLSLOT  .UshortValue == 0))  ) ) 
            { 
            __context__.SourceCodeLine = 2630;
            MakeString ( DEVICE_TX__DOLLAR__ , "xCommand FarEndControl Preset Activate PresetId: {0:d}\r\n", (short)FARENDCONTROLPRESETACTIVATEID  .UshortValue) ; 
            } 
        
        else 
            { 
            __context__.SourceCodeLine = 2634;
            MakeString ( DEVICE_TX__DOLLAR__ , "xCommand FarEndControl Preset Activate PresetId: {0:d}  CallId: \"{1:d}\"\r\n", (short)FARENDCONTROLPRESETACTIVATEID  .UshortValue, (short)CALLSTATUS_CALLID[ FARENDCONTROLCALLSLOT  .UshortValue ] .Value) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object FARENDCONTROLCAMERASTOP_OnPush_25 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 2640;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (FARENDCONTROLCALLSLOT  .UshortValue == 0))  ) ) 
            { 
            __context__.SourceCodeLine = 2642;
            MakeString ( DEVICE_TX__DOLLAR__ , "xCommand FarEndControl Camera Stop\r\n") ; 
            } 
        
        else 
            { 
            __context__.SourceCodeLine = 2646;
            MakeString ( DEVICE_TX__DOLLAR__ , "xcommand FarEndControl Camera Stop CallId: \"{0:d}\"\r\n", (short)CALLSTATUS_CALLID[ FARENDCONTROLCALLSLOT  .UshortValue ] .Value) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

private void SSTRINGSENDMESSAGE (  SplusExecutionContext __context__, CrestronString MESSAGE ) 
    { 
    
    __context__.SourceCodeLine = 2652;
    MakeString ( DEVICE_TX__DOLLAR__ , "xCommand SStringSend Message: \"{0}\"\r\n", MESSAGE ) ; 
    
    }
    
private void TSTRINGSENDMESSAGE (  SplusExecutionContext __context__, CrestronString MESSAGE ) 
    { 
    
    __context__.SourceCodeLine = 2657;
    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (DTMFSENDCALLSLOT  .UshortValue == 0))  ) ) 
        { 
        __context__.SourceCodeLine = 2659;
        MakeString ( DEVICE_TX__DOLLAR__ , "xCommand TStringSend Message: \"{0}\"\r\n", MESSAGE ) ; 
        } 
    
    else 
        { 
        __context__.SourceCodeLine = 2663;
        MakeString ( DEVICE_TX__DOLLAR__ , "xCommand TStringSend Message: \"{0}\" CallId: \"{1:d}\"\r\n", MESSAGE , (short)CALLSTATUS_CALLID[ DTMFSENDCALLSLOT  .UshortValue ] .Value) ; 
        } 
    
    
    }
    
object TSTRING_SEND_TEXT_OnChange_26 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 2669;
        TSTRINGSENDMESSAGE (  __context__ , TSTRING_SEND_TEXT) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object SSTRING_SEND_TEXT_OnChange_27 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 2675;
        SSTRINGSENDMESSAGE (  __context__ , SSTRING_SEND_TEXT) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object FARENDCONTROLCAMERAPANLEFT_OnPush_28 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 2681;
        FARENDCONTROLCAMERAMOVE (  __context__ , "Left") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object FARENDCONTROLCAMERAPANRIGHT_OnPush_29 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 2686;
        FARENDCONTROLCAMERAMOVE (  __context__ , "Right") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object FARENDCONTROLCAMERATILTUP_OnPush_30 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 2691;
        FARENDCONTROLCAMERAMOVE (  __context__ , "Up") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object FARENDCONTROLCAMERATILTDOWN_OnPush_31 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 2696;
        FARENDCONTROLCAMERAMOVE (  __context__ , "Down") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object FARENDCONTROLCAMERAZOOMIN_OnPush_32 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 2701;
        FARENDCONTROLCAMERAMOVE (  __context__ , "ZoomIn") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object FARENDCONTROLCAMERAZOOMOUT_OnPush_33 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 2706;
        FARENDCONTROLCAMERAMOVE (  __context__ , "ZoomOut") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object FARENDCONTROLCAMERAFOCUSIN_OnPush_34 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 2711;
        FARENDCONTROLCAMERAMOVE (  __context__ , "FocusIn") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object FARENDCONTROLCAMERAFOCUSOUT_OnPush_35 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 2716;
        FARENDCONTROLCAMERAMOVE (  __context__ , "FocusOut") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DATATOTRACE_OnPush_36 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 2721;
        PRINTSAVEDDATA (  __context__  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public override object FunctionMain (  object __obj__ ) 
    { 
    try
    {
        SplusExecutionContext __context__ = SplusFunctionMainStartCode();
        
        __context__.SourceCodeLine = 2730;
        SEMAPHORE = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 2731;
        IPROCESS = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 2732;
        DIALSEMAPHORE = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 2733;
        SIGNALINCOMINGCALL  .Value = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 2734;
        NUMBEROFACTIVECALLS  .Value = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 2735;
        GLOBALSEARCHCOUNT = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 2736;
        RESETPHONEBOOKLIST (  __context__  ) ; 
        __context__.SourceCodeLine = 2737;
        RESETSELECTEDPHONEBOOKITEM (  __context__  ) ; 
        __context__.SourceCodeLine = 2738;
        RESETEVERYTHING (  __context__  ) ; 
        __context__.SourceCodeLine = 2739;
        PHONEBOOKENTRY [ 0] . FIRSTCHILD = (ushort) ( 1 ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    return __obj__;
    }
    

public override void LogosSplusInitialize()
{
    _SplusNVRAM = new SplusNVRAM( this );
    SEARCHRESULT  = new ushort[ 51 ];
    TEMPSTRING__DOLLAR__  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1000, this );
    CALLRESPONSETYPE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 40, this );
    CALLSTATUS  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 30, this );
    REMOTENUMBER  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
    GTEMPDISPLAYNAME  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 40, this );
    GTEMPVALUE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 10, this );
    GTEMPSEGMENT  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 64, this );
    PHONEBOOKENTRYTYPE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 30, this );
    PHONEBOOKTYPE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 9, this );
    MESSAGE_PROMPT_FEEDBACKID  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
    SSEARCHTEXT  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 82, this );
    PHONEBOOKENTRY  = new PHONEBOOK[ 2001 ];
    for( uint i = 0; i < 2001; i++ )
    {
        PHONEBOOKENTRY [i] = new PHONEBOOK( this, true );
        PHONEBOOKENTRY [i].PopulateCustomAttributeList( false );
        
    }
    CONTACT  = new CONTACTLIST[ 4001 ];
    for( uint i = 0; i < 4001; i++ )
    {
        CONTACT [i] = new CONTACTLIST( this, true );
        CONTACT [i].PopulateCustomAttributeList( false );
        
    }
    
    GETLOCALPHONEBOOK = new Crestron.Logos.SplusObjects.DigitalInput( GETLOCALPHONEBOOK__DigitalInput__, this );
    m_DigitalInputList.Add( GETLOCALPHONEBOOK__DigitalInput__, GETLOCALPHONEBOOK );
    
    ENABLERECENTCALLS = new Crestron.Logos.SplusObjects.DigitalInput( ENABLERECENTCALLS__DigitalInput__, this );
    m_DigitalInputList.Add( ENABLERECENTCALLS__DigitalInput__, ENABLERECENTCALLS );
    
    GETRECENTCALLS = new Crestron.Logos.SplusObjects.DigitalInput( GETRECENTCALLS__DigitalInput__, this );
    m_DigitalInputList.Add( GETRECENTCALLS__DigitalInput__, GETRECENTCALLS );
    
    CLEARSELECTEDENTRYNUMBER = new Crestron.Logos.SplusObjects.DigitalInput( CLEARSELECTEDENTRYNUMBER__DigitalInput__, this );
    m_DigitalInputList.Add( CLEARSELECTEDENTRYNUMBER__DigitalInput__, CLEARSELECTEDENTRYNUMBER );
    
    DIALSELECTEDPHONEBOOKENTRYNUMBER = new Crestron.Logos.SplusObjects.DigitalInput( DIALSELECTEDPHONEBOOKENTRYNUMBER__DigitalInput__, this );
    m_DigitalInputList.Add( DIALSELECTEDPHONEBOOKENTRYNUMBER__DigitalInput__, DIALSELECTEDPHONEBOOKENTRYNUMBER );
    
    TOPLEVEL = new Crestron.Logos.SplusObjects.DigitalInput( TOPLEVEL__DigitalInput__, this );
    m_DigitalInputList.Add( TOPLEVEL__DigitalInput__, TOPLEVEL );
    
    UPONELEVEL = new Crestron.Logos.SplusObjects.DigitalInput( UPONELEVEL__DigitalInput__, this );
    m_DigitalInputList.Add( UPONELEVEL__DigitalInput__, UPONELEVEL );
    
    PAGESPHONEBOOKFIRSTPAGE = new Crestron.Logos.SplusObjects.DigitalInput( PAGESPHONEBOOKFIRSTPAGE__DigitalInput__, this );
    m_DigitalInputList.Add( PAGESPHONEBOOKFIRSTPAGE__DigitalInput__, PAGESPHONEBOOKFIRSTPAGE );
    
    PAGESPHONEBOOKNEXTPAGE = new Crestron.Logos.SplusObjects.DigitalInput( PAGESPHONEBOOKNEXTPAGE__DigitalInput__, this );
    m_DigitalInputList.Add( PAGESPHONEBOOKNEXTPAGE__DigitalInput__, PAGESPHONEBOOKNEXTPAGE );
    
    PAGESPHONEBOOKPREVIOUSPAGE = new Crestron.Logos.SplusObjects.DigitalInput( PAGESPHONEBOOKPREVIOUSPAGE__DigitalInput__, this );
    m_DigitalInputList.Add( PAGESPHONEBOOKPREVIOUSPAGE__DigitalInput__, PAGESPHONEBOOKPREVIOUSPAGE );
    
    SEARCHPHONEBOOK = new Crestron.Logos.SplusObjects.DigitalInput( SEARCHPHONEBOOK__DigitalInput__, this );
    m_DigitalInputList.Add( SEARCHPHONEBOOK__DigitalInput__, SEARCHPHONEBOOK );
    
    CALLCONTROLACCEPTINCOMINGCALL = new Crestron.Logos.SplusObjects.DigitalInput( CALLCONTROLACCEPTINCOMINGCALL__DigitalInput__, this );
    m_DigitalInputList.Add( CALLCONTROLACCEPTINCOMINGCALL__DigitalInput__, CALLCONTROLACCEPTINCOMINGCALL );
    
    CALLCONTROLREJECTINCOMINGCALL = new Crestron.Logos.SplusObjects.DigitalInput( CALLCONTROLREJECTINCOMINGCALL__DigitalInput__, this );
    m_DigitalInputList.Add( CALLCONTROLREJECTINCOMINGCALL__DigitalInput__, CALLCONTROLREJECTINCOMINGCALL );
    
    DISCONNECTALLCALLS = new Crestron.Logos.SplusObjects.DigitalInput( DISCONNECTALLCALLS__DigitalInput__, this );
    m_DigitalInputList.Add( DISCONNECTALLCALLS__DigitalInput__, DISCONNECTALLCALLS );
    
    DIALCALL = new Crestron.Logos.SplusObjects.DigitalInput( DIALCALL__DigitalInput__, this );
    m_DigitalInputList.Add( DIALCALL__DigitalInput__, DIALCALL );
    
    FARENDCONTROLCAMERAPANLEFT = new Crestron.Logos.SplusObjects.DigitalInput( FARENDCONTROLCAMERAPANLEFT__DigitalInput__, this );
    m_DigitalInputList.Add( FARENDCONTROLCAMERAPANLEFT__DigitalInput__, FARENDCONTROLCAMERAPANLEFT );
    
    FARENDCONTROLCAMERAPANRIGHT = new Crestron.Logos.SplusObjects.DigitalInput( FARENDCONTROLCAMERAPANRIGHT__DigitalInput__, this );
    m_DigitalInputList.Add( FARENDCONTROLCAMERAPANRIGHT__DigitalInput__, FARENDCONTROLCAMERAPANRIGHT );
    
    FARENDCONTROLCAMERATILTUP = new Crestron.Logos.SplusObjects.DigitalInput( FARENDCONTROLCAMERATILTUP__DigitalInput__, this );
    m_DigitalInputList.Add( FARENDCONTROLCAMERATILTUP__DigitalInput__, FARENDCONTROLCAMERATILTUP );
    
    FARENDCONTROLCAMERATILTDOWN = new Crestron.Logos.SplusObjects.DigitalInput( FARENDCONTROLCAMERATILTDOWN__DigitalInput__, this );
    m_DigitalInputList.Add( FARENDCONTROLCAMERATILTDOWN__DigitalInput__, FARENDCONTROLCAMERATILTDOWN );
    
    FARENDCONTROLCAMERAZOOMIN = new Crestron.Logos.SplusObjects.DigitalInput( FARENDCONTROLCAMERAZOOMIN__DigitalInput__, this );
    m_DigitalInputList.Add( FARENDCONTROLCAMERAZOOMIN__DigitalInput__, FARENDCONTROLCAMERAZOOMIN );
    
    FARENDCONTROLCAMERAZOOMOUT = new Crestron.Logos.SplusObjects.DigitalInput( FARENDCONTROLCAMERAZOOMOUT__DigitalInput__, this );
    m_DigitalInputList.Add( FARENDCONTROLCAMERAZOOMOUT__DigitalInput__, FARENDCONTROLCAMERAZOOMOUT );
    
    FARENDCONTROLCAMERAFOCUSIN = new Crestron.Logos.SplusObjects.DigitalInput( FARENDCONTROLCAMERAFOCUSIN__DigitalInput__, this );
    m_DigitalInputList.Add( FARENDCONTROLCAMERAFOCUSIN__DigitalInput__, FARENDCONTROLCAMERAFOCUSIN );
    
    FARENDCONTROLCAMERAFOCUSOUT = new Crestron.Logos.SplusObjects.DigitalInput( FARENDCONTROLCAMERAFOCUSOUT__DigitalInput__, this );
    m_DigitalInputList.Add( FARENDCONTROLCAMERAFOCUSOUT__DigitalInput__, FARENDCONTROLCAMERAFOCUSOUT );
    
    FARENDCONTROLCAMERASTOP = new Crestron.Logos.SplusObjects.DigitalInput( FARENDCONTROLCAMERASTOP__DigitalInput__, this );
    m_DigitalInputList.Add( FARENDCONTROLCAMERASTOP__DigitalInput__, FARENDCONTROLCAMERASTOP );
    
    RESYNCRONIZECALLSTATUSLIST = new Crestron.Logos.SplusObjects.DigitalInput( RESYNCRONIZECALLSTATUSLIST__DigitalInput__, this );
    m_DigitalInputList.Add( RESYNCRONIZECALLSTATUSLIST__DigitalInput__, RESYNCRONIZECALLSTATUSLIST );
    
    DATATOTRACE = new Crestron.Logos.SplusObjects.DigitalInput( DATATOTRACE__DigitalInput__, this );
    m_DigitalInputList.Add( DATATOTRACE__DigitalInput__, DATATOTRACE );
    
    DEFAULTCALLTYPESIP = new Crestron.Logos.SplusObjects.DigitalInput( DEFAULTCALLTYPESIP__DigitalInput__, this );
    m_DigitalInputList.Add( DEFAULTCALLTYPESIP__DigitalInput__, DEFAULTCALLTYPESIP );
    
    MESSAGE_ALERT_ISACTIVE = new Crestron.Logos.SplusObjects.DigitalOutput( MESSAGE_ALERT_ISACTIVE__DigitalOutput__, this );
    m_DigitalOutputList.Add( MESSAGE_ALERT_ISACTIVE__DigitalOutput__, MESSAGE_ALERT_ISACTIVE );
    
    MESSAGE_PROMPT_ISACTIVE = new Crestron.Logos.SplusObjects.DigitalOutput( MESSAGE_PROMPT_ISACTIVE__DigitalOutput__, this );
    m_DigitalOutputList.Add( MESSAGE_PROMPT_ISACTIVE__DigitalOutput__, MESSAGE_PROMPT_ISACTIVE );
    
    MESSAGE_PROMPT_OPTION1_ISACTIVE = new Crestron.Logos.SplusObjects.DigitalOutput( MESSAGE_PROMPT_OPTION1_ISACTIVE__DigitalOutput__, this );
    m_DigitalOutputList.Add( MESSAGE_PROMPT_OPTION1_ISACTIVE__DigitalOutput__, MESSAGE_PROMPT_OPTION1_ISACTIVE );
    
    MESSAGE_PROMPT_OPTION2_ISACTIVE = new Crestron.Logos.SplusObjects.DigitalOutput( MESSAGE_PROMPT_OPTION2_ISACTIVE__DigitalOutput__, this );
    m_DigitalOutputList.Add( MESSAGE_PROMPT_OPTION2_ISACTIVE__DigitalOutput__, MESSAGE_PROMPT_OPTION2_ISACTIVE );
    
    MESSAGE_PROMPT_OPTION3_ISACTIVE = new Crestron.Logos.SplusObjects.DigitalOutput( MESSAGE_PROMPT_OPTION3_ISACTIVE__DigitalOutput__, this );
    m_DigitalOutputList.Add( MESSAGE_PROMPT_OPTION3_ISACTIVE__DigitalOutput__, MESSAGE_PROMPT_OPTION3_ISACTIVE );
    
    MESSAGE_PROMPT_OPTION4_ISACTIVE = new Crestron.Logos.SplusObjects.DigitalOutput( MESSAGE_PROMPT_OPTION4_ISACTIVE__DigitalOutput__, this );
    m_DigitalOutputList.Add( MESSAGE_PROMPT_OPTION4_ISACTIVE__DigitalOutput__, MESSAGE_PROMPT_OPTION4_ISACTIVE );
    
    MESSAGE_PROMPT_OPTION5_ISACTIVE = new Crestron.Logos.SplusObjects.DigitalOutput( MESSAGE_PROMPT_OPTION5_ISACTIVE__DigitalOutput__, this );
    m_DigitalOutputList.Add( MESSAGE_PROMPT_OPTION5_ISACTIVE__DigitalOutput__, MESSAGE_PROMPT_OPTION5_ISACTIVE );
    
    MESSAGE_PROMPT_OPTION1_WASSELECTED = new Crestron.Logos.SplusObjects.DigitalOutput( MESSAGE_PROMPT_OPTION1_WASSELECTED__DigitalOutput__, this );
    m_DigitalOutputList.Add( MESSAGE_PROMPT_OPTION1_WASSELECTED__DigitalOutput__, MESSAGE_PROMPT_OPTION1_WASSELECTED );
    
    MESSAGE_PROMPT_OPTION2_WASSELECTED = new Crestron.Logos.SplusObjects.DigitalOutput( MESSAGE_PROMPT_OPTION2_WASSELECTED__DigitalOutput__, this );
    m_DigitalOutputList.Add( MESSAGE_PROMPT_OPTION2_WASSELECTED__DigitalOutput__, MESSAGE_PROMPT_OPTION2_WASSELECTED );
    
    MESSAGE_PROMPT_OPTION3_WASSELECTED = new Crestron.Logos.SplusObjects.DigitalOutput( MESSAGE_PROMPT_OPTION3_WASSELECTED__DigitalOutput__, this );
    m_DigitalOutputList.Add( MESSAGE_PROMPT_OPTION3_WASSELECTED__DigitalOutput__, MESSAGE_PROMPT_OPTION3_WASSELECTED );
    
    MESSAGE_PROMPT_OPTION4_WASSELECTED = new Crestron.Logos.SplusObjects.DigitalOutput( MESSAGE_PROMPT_OPTION4_WASSELECTED__DigitalOutput__, this );
    m_DigitalOutputList.Add( MESSAGE_PROMPT_OPTION4_WASSELECTED__DigitalOutput__, MESSAGE_PROMPT_OPTION4_WASSELECTED );
    
    MESSAGE_PROMPT_OPTION5_WASSELECTED = new Crestron.Logos.SplusObjects.DigitalOutput( MESSAGE_PROMPT_OPTION5_WASSELECTED__DigitalOutput__, this );
    m_DigitalOutputList.Add( MESSAGE_PROMPT_OPTION5_WASSELECTED__DigitalOutput__, MESSAGE_PROMPT_OPTION5_WASSELECTED );
    
    PHONEBOOK_SEARCHRESULT_IS_FROM_SUBFOLDER = new Crestron.Logos.SplusObjects.DigitalOutput( PHONEBOOK_SEARCHRESULT_IS_FROM_SUBFOLDER__DigitalOutput__, this );
    m_DigitalOutputList.Add( PHONEBOOK_SEARCHRESULT_IS_FROM_SUBFOLDER__DigitalOutput__, PHONEBOOK_SEARCHRESULT_IS_FROM_SUBFOLDER );
    
    DIRECTORYDOWNLOADBUSY = new Crestron.Logos.SplusObjects.DigitalOutput( DIRECTORYDOWNLOADBUSY__DigitalOutput__, this );
    m_DigitalOutputList.Add( DIRECTORYDOWNLOADBUSY__DigitalOutput__, DIRECTORYDOWNLOADBUSY );
    
    RECENTCALLSDOWNLOADBUSY = new Crestron.Logos.SplusObjects.DigitalOutput( RECENTCALLSDOWNLOADBUSY__DigitalOutput__, this );
    m_DigitalOutputList.Add( RECENTCALLSDOWNLOADBUSY__DigitalOutput__, RECENTCALLSDOWNLOADBUSY );
    
    SELECTEDISFOLDER = new Crestron.Logos.SplusObjects.DigitalOutput( SELECTEDISFOLDER__DigitalOutput__, this );
    m_DigitalOutputList.Add( SELECTEDISFOLDER__DigitalOutput__, SELECTEDISFOLDER );
    
    SELECTEDISCONTACT = new Crestron.Logos.SplusObjects.DigitalOutput( SELECTEDISCONTACT__DigitalOutput__, this );
    m_DigitalOutputList.Add( SELECTEDISCONTACT__DigitalOutput__, SELECTEDISCONTACT );
    
    SEARCH_IS_ACTIVE = new Crestron.Logos.SplusObjects.DigitalOutput( SEARCH_IS_ACTIVE__DigitalOutput__, this );
    m_DigitalOutputList.Add( SEARCH_IS_ACTIVE__DigitalOutput__, SEARCH_IS_ACTIVE );
    
    GETREBOOTCODEC_IS_IN_PROGRESS = new Crestron.Logos.SplusObjects.DigitalOutput( GETREBOOTCODEC_IS_IN_PROGRESS__DigitalOutput__, this );
    m_DigitalOutputList.Add( GETREBOOTCODEC_IS_IN_PROGRESS__DigitalOutput__, GETREBOOTCODEC_IS_IN_PROGRESS );
    
    SIGNALINCOMINGCALL = new Crestron.Logos.SplusObjects.DigitalOutput( SIGNALINCOMINGCALL__DigitalOutput__, this );
    m_DigitalOutputList.Add( SIGNALINCOMINGCALL__DigitalOutput__, SIGNALINCOMINGCALL );
    
    DEVICE_ONLINE = new Crestron.Logos.SplusObjects.DigitalOutput( DEVICE_ONLINE__DigitalOutput__, this );
    m_DigitalOutputList.Add( DEVICE_ONLINE__DigitalOutput__, DEVICE_ONLINE );
    
    DIRECTORYONTOPLEVEL = new Crestron.Logos.SplusObjects.DigitalOutput( DIRECTORYONTOPLEVEL__DigitalOutput__, this );
    m_DigitalOutputList.Add( DIRECTORYONTOPLEVEL__DigitalOutput__, DIRECTORYONTOPLEVEL );
    
    CALLSTATUS_ISCONNECTED = new InOutArray<DigitalOutput>( 3, this );
    for( uint i = 0; i < 3; i++ )
    {
        CALLSTATUS_ISCONNECTED[i+1] = new Crestron.Logos.SplusObjects.DigitalOutput( CALLSTATUS_ISCONNECTED__DigitalOutput__ + i, this );
        m_DigitalOutputList.Add( CALLSTATUS_ISCONNECTED__DigitalOutput__ + i, CALLSTATUS_ISCONNECTED[i+1] );
    }
    
    CALLSTATUS_SUPPORTSPRESENTATION = new InOutArray<DigitalOutput>( 3, this );
    for( uint i = 0; i < 3; i++ )
    {
        CALLSTATUS_SUPPORTSPRESENTATION[i+1] = new Crestron.Logos.SplusObjects.DigitalOutput( CALLSTATUS_SUPPORTSPRESENTATION__DigitalOutput__ + i, this );
        m_DigitalOutputList.Add( CALLSTATUS_SUPPORTSPRESENTATION__DigitalOutput__ + i, CALLSTATUS_SUPPORTSPRESENTATION[i+1] );
    }
    
    CALLSTATUS_ISACTIVE = new InOutArray<DigitalOutput>( 3, this );
    for( uint i = 0; i < 3; i++ )
    {
        CALLSTATUS_ISACTIVE[i+1] = new Crestron.Logos.SplusObjects.DigitalOutput( CALLSTATUS_ISACTIVE__DigitalOutput__ + i, this );
        m_DigitalOutputList.Add( CALLSTATUS_ISACTIVE__DigitalOutput__ + i, CALLSTATUS_ISACTIVE[i+1] );
    }
    
    ENTRIESPERPAGE = new Crestron.Logos.SplusObjects.AnalogInput( ENTRIESPERPAGE__AnalogSerialInput__, this );
    m_AnalogInputList.Add( ENTRIESPERPAGE__AnalogSerialInput__, ENTRIESPERPAGE );
    
    DTMFSENDCALLSLOT = new Crestron.Logos.SplusObjects.AnalogInput( DTMFSENDCALLSLOT__AnalogSerialInput__, this );
    m_AnalogInputList.Add( DTMFSENDCALLSLOT__AnalogSerialInput__, DTMFSENDCALLSLOT );
    
    SELECTEDENTRYNUMBER = new Crestron.Logos.SplusObjects.AnalogInput( SELECTEDENTRYNUMBER__AnalogSerialInput__, this );
    m_AnalogInputList.Add( SELECTEDENTRYNUMBER__AnalogSerialInput__, SELECTEDENTRYNUMBER );
    
    PAGESSELECTEDENTRYNUMBER = new Crestron.Logos.SplusObjects.AnalogInput( PAGESSELECTEDENTRYNUMBER__AnalogSerialInput__, this );
    m_AnalogInputList.Add( PAGESSELECTEDENTRYNUMBER__AnalogSerialInput__, PAGESSELECTEDENTRYNUMBER );
    
    PHONEBOOKSOURCEID = new Crestron.Logos.SplusObjects.AnalogInput( PHONEBOOKSOURCEID__AnalogSerialInput__, this );
    m_AnalogInputList.Add( PHONEBOOKSOURCEID__AnalogSerialInput__, PHONEBOOKSOURCEID );
    
    DISCONNECTCALLWITHID = new Crestron.Logos.SplusObjects.AnalogInput( DISCONNECTCALLWITHID__AnalogSerialInput__, this );
    m_AnalogInputList.Add( DISCONNECTCALLWITHID__AnalogSerialInput__, DISCONNECTCALLWITHID );
    
    FARENDCONTROLCALLSLOT = new Crestron.Logos.SplusObjects.AnalogInput( FARENDCONTROLCALLSLOT__AnalogSerialInput__, this );
    m_AnalogInputList.Add( FARENDCONTROLCALLSLOT__AnalogSerialInput__, FARENDCONTROLCALLSLOT );
    
    FARENDCONTROLPRESENTATIONSOURCEID = new Crestron.Logos.SplusObjects.AnalogInput( FARENDCONTROLPRESENTATIONSOURCEID__AnalogSerialInput__, this );
    m_AnalogInputList.Add( FARENDCONTROLPRESENTATIONSOURCEID__AnalogSerialInput__, FARENDCONTROLPRESENTATIONSOURCEID );
    
    FARENDCONTROLPRESETACTIVATEID = new Crestron.Logos.SplusObjects.AnalogInput( FARENDCONTROLPRESETACTIVATEID__AnalogSerialInput__, this );
    m_AnalogInputList.Add( FARENDCONTROLPRESETACTIVATEID__AnalogSerialInput__, FARENDCONTROLPRESETACTIVATEID );
    
    SETVOLUMELEVEL = new Crestron.Logos.SplusObjects.AnalogInput( SETVOLUMELEVEL__AnalogSerialInput__, this );
    m_AnalogInputList.Add( SETVOLUMELEVEL__AnalogSerialInput__, SETVOLUMELEVEL );
    
    SEARCHDELAYSTART = new Crestron.Logos.SplusObjects.AnalogInput( SEARCHDELAYSTART__AnalogSerialInput__, this );
    m_AnalogInputList.Add( SEARCHDELAYSTART__AnalogSerialInput__, SEARCHDELAYSTART );
    
    SEARCHDELAYMOD = new Crestron.Logos.SplusObjects.AnalogInput( SEARCHDELAYMOD__AnalogSerialInput__, this );
    m_AnalogInputList.Add( SEARCHDELAYMOD__AnalogSerialInput__, SEARCHDELAYMOD );
    
    SEARCHDELAY = new Crestron.Logos.SplusObjects.AnalogInput( SEARCHDELAY__AnalogSerialInput__, this );
    m_AnalogInputList.Add( SEARCHDELAY__AnalogSerialInput__, SEARCHDELAY );
    
    GETVOLUMELEVEL = new Crestron.Logos.SplusObjects.AnalogOutput( GETVOLUMELEVEL__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( GETVOLUMELEVEL__AnalogSerialOutput__, GETVOLUMELEVEL );
    
    CURRENTFIRSTPHONEBOOKITEMNUMBER = new Crestron.Logos.SplusObjects.AnalogOutput( CURRENTFIRSTPHONEBOOKITEMNUMBER__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( CURRENTFIRSTPHONEBOOKITEMNUMBER__AnalogSerialOutput__, CURRENTFIRSTPHONEBOOKITEMNUMBER );
    
    CURRENTLASTPHONEBOOKITEMNUMBER = new Crestron.Logos.SplusObjects.AnalogOutput( CURRENTLASTPHONEBOOKITEMNUMBER__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( CURRENTLASTPHONEBOOKITEMNUMBER__AnalogSerialOutput__, CURRENTLASTPHONEBOOKITEMNUMBER );
    
    TOTALROWSFORSEARCH = new Crestron.Logos.SplusObjects.AnalogOutput( TOTALROWSFORSEARCH__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( TOTALROWSFORSEARCH__AnalogSerialOutput__, TOTALROWSFORSEARCH );
    
    NUMBEROFACTIVECALLS = new Crestron.Logos.SplusObjects.AnalogOutput( NUMBEROFACTIVECALLS__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( NUMBEROFACTIVECALLS__AnalogSerialOutput__, NUMBEROFACTIVECALLS );
    
    PHONEBOOKPAGESCURRENTPAGE = new Crestron.Logos.SplusObjects.AnalogOutput( PHONEBOOKPAGESCURRENTPAGE__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( PHONEBOOKPAGESCURRENTPAGE__AnalogSerialOutput__, PHONEBOOKPAGESCURRENTPAGE );
    
    PHONEBOOKPAGESTOTALPAGES = new Crestron.Logos.SplusObjects.AnalogOutput( PHONEBOOKPAGESTOTALPAGES__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( PHONEBOOKPAGESTOTALPAGES__AnalogSerialOutput__, PHONEBOOKPAGESTOTALPAGES );
    
    CALLSTATUS_CALLID = new InOutArray<AnalogOutput>( 3, this );
    for( uint i = 0; i < 3; i++ )
    {
        CALLSTATUS_CALLID[i+1] = new Crestron.Logos.SplusObjects.AnalogOutput( CALLSTATUS_CALLID__AnalogSerialOutput__ + i, this );
        m_AnalogOutputList.Add( CALLSTATUS_CALLID__AnalogSerialOutput__ + i, CALLSTATUS_CALLID[i+1] );
    }
    
    DTMFSEND = new Crestron.Logos.SplusObjects.StringInput( DTMFSEND__AnalogSerialInput__, 32, this );
    m_StringInputList.Add( DTMFSEND__AnalogSerialInput__, DTMFSEND );
    
    PHONEBOOKSEARCHSTRING = new Crestron.Logos.SplusObjects.StringInput( PHONEBOOKSEARCHSTRING__AnalogSerialInput__, 82, this );
    m_StringInputList.Add( PHONEBOOKSEARCHSTRING__AnalogSerialInput__, PHONEBOOKSEARCHSTRING );
    
    DIALNUMBER = new Crestron.Logos.SplusObjects.StringInput( DIALNUMBER__AnalogSerialInput__, 255, this );
    m_StringInputList.Add( DIALNUMBER__AnalogSerialInput__, DIALNUMBER );
    
    DIALCALLRATE = new Crestron.Logos.SplusObjects.StringInput( DIALCALLRATE__AnalogSerialInput__, 6, this );
    m_StringInputList.Add( DIALCALLRATE__AnalogSerialInput__, DIALCALLRATE );
    
    DIALCALLPROTOCOL = new Crestron.Logos.SplusObjects.StringInput( DIALCALLPROTOCOL__AnalogSerialInput__, 4, this );
    m_StringInputList.Add( DIALCALLPROTOCOL__AnalogSerialInput__, DIALCALLPROTOCOL );
    
    TSTRING_SEND_TEXT = new Crestron.Logos.SplusObjects.StringInput( TSTRING_SEND_TEXT__AnalogSerialInput__, 1450, this );
    m_StringInputList.Add( TSTRING_SEND_TEXT__AnalogSerialInput__, TSTRING_SEND_TEXT );
    
    SSTRING_SEND_TEXT = new Crestron.Logos.SplusObjects.StringInput( SSTRING_SEND_TEXT__AnalogSerialInput__, 256, this );
    m_StringInputList.Add( SSTRING_SEND_TEXT__AnalogSerialInput__, SSTRING_SEND_TEXT );
    
    DEVICE_TX__DOLLAR__ = new Crestron.Logos.SplusObjects.StringOutput( DEVICE_TX__DOLLAR____AnalogSerialOutput__, this );
    m_StringOutputList.Add( DEVICE_TX__DOLLAR____AnalogSerialOutput__, DEVICE_TX__DOLLAR__ );
    
    TSTRING_RECEIVE_TEXT = new Crestron.Logos.SplusObjects.StringOutput( TSTRING_RECEIVE_TEXT__AnalogSerialOutput__, this );
    m_StringOutputList.Add( TSTRING_RECEIVE_TEXT__AnalogSerialOutput__, TSTRING_RECEIVE_TEXT );
    
    SSTRING_RECEIVE_TEXT = new Crestron.Logos.SplusObjects.StringOutput( SSTRING_RECEIVE_TEXT__AnalogSerialOutput__, this );
    m_StringOutputList.Add( SSTRING_RECEIVE_TEXT__AnalogSerialOutput__, SSTRING_RECEIVE_TEXT );
    
    SELECTEDPHONEBOOK_NAME = new Crestron.Logos.SplusObjects.StringOutput( SELECTEDPHONEBOOK_NAME__AnalogSerialOutput__, this );
    m_StringOutputList.Add( SELECTEDPHONEBOOK_NAME__AnalogSerialOutput__, SELECTEDPHONEBOOK_NAME );
    
    SELECTEDPHONEBOOK_NUMBER = new Crestron.Logos.SplusObjects.StringOutput( SELECTEDPHONEBOOK_NUMBER__AnalogSerialOutput__, this );
    m_StringOutputList.Add( SELECTEDPHONEBOOK_NUMBER__AnalogSerialOutput__, SELECTEDPHONEBOOK_NUMBER );
    
    SELECTEDPHONEBOOK_CALLRATE = new Crestron.Logos.SplusObjects.StringOutput( SELECTEDPHONEBOOK_CALLRATE__AnalogSerialOutput__, this );
    m_StringOutputList.Add( SELECTEDPHONEBOOK_CALLRATE__AnalogSerialOutput__, SELECTEDPHONEBOOK_CALLRATE );
    
    PHONEBOOK_SEARCH_TEXT = new Crestron.Logos.SplusObjects.StringOutput( PHONEBOOK_SEARCH_TEXT__AnalogSerialOutput__, this );
    m_StringOutputList.Add( PHONEBOOK_SEARCH_TEXT__AnalogSerialOutput__, PHONEBOOK_SEARCH_TEXT );
    
    GETSYSTEMNAME = new Crestron.Logos.SplusObjects.StringOutput( GETSYSTEMNAME__AnalogSerialOutput__, this );
    m_StringOutputList.Add( GETSYSTEMNAME__AnalogSerialOutput__, GETSYSTEMNAME );
    
    GETSYSTEMNETWORKADDRESS = new Crestron.Logos.SplusObjects.StringOutput( GETSYSTEMNETWORKADDRESS__AnalogSerialOutput__, this );
    m_StringOutputList.Add( GETSYSTEMNETWORKADDRESS__AnalogSerialOutput__, GETSYSTEMNETWORKADDRESS );
    
    GETGATEKEEPERSTATUS = new Crestron.Logos.SplusObjects.StringOutput( GETGATEKEEPERSTATUS__AnalogSerialOutput__, this );
    m_StringOutputList.Add( GETGATEKEEPERSTATUS__AnalogSerialOutput__, GETGATEKEEPERSTATUS );
    
    GETGATEKEEPERREGISTRATIONMODE = new Crestron.Logos.SplusObjects.StringOutput( GETGATEKEEPERREGISTRATIONMODE__AnalogSerialOutput__, this );
    m_StringOutputList.Add( GETGATEKEEPERREGISTRATIONMODE__AnalogSerialOutput__, GETGATEKEEPERREGISTRATIONMODE );
    
    GETGATEKEEPERNETWORKADDRESS = new Crestron.Logos.SplusObjects.StringOutput( GETGATEKEEPERNETWORKADDRESS__AnalogSerialOutput__, this );
    m_StringOutputList.Add( GETGATEKEEPERNETWORKADDRESS__AnalogSerialOutput__, GETGATEKEEPERNETWORKADDRESS );
    
    GETSYSTEMH323ID = new Crestron.Logos.SplusObjects.StringOutput( GETSYSTEMH323ID__AnalogSerialOutput__, this );
    m_StringOutputList.Add( GETSYSTEMH323ID__AnalogSerialOutput__, GETSYSTEMH323ID );
    
    GETSYSTEME164ALIAS = new Crestron.Logos.SplusObjects.StringOutput( GETSYSTEME164ALIAS__AnalogSerialOutput__, this );
    m_StringOutputList.Add( GETSYSTEME164ALIAS__AnalogSerialOutput__, GETSYSTEME164ALIAS );
    
    GETSYSTEMSIPURI = new Crestron.Logos.SplusObjects.StringOutput( GETSYSTEMSIPURI__AnalogSerialOutput__, this );
    m_StringOutputList.Add( GETSYSTEMSIPURI__AnalogSerialOutput__, GETSYSTEMSIPURI );
    
    GETSYSTEMSOFTWAREVERSION = new Crestron.Logos.SplusObjects.StringOutput( GETSYSTEMSOFTWAREVERSION__AnalogSerialOutput__, this );
    m_StringOutputList.Add( GETSYSTEMSOFTWAREVERSION__AnalogSerialOutput__, GETSYSTEMSOFTWAREVERSION );
    
    FROM_DEVICE_CONFIGURATIONCHANGES = new Crestron.Logos.SplusObjects.StringOutput( FROM_DEVICE_CONFIGURATIONCHANGES__AnalogSerialOutput__, this );
    m_StringOutputList.Add( FROM_DEVICE_CONFIGURATIONCHANGES__AnalogSerialOutput__, FROM_DEVICE_CONFIGURATIONCHANGES );
    
    FROM_DEVICE_STATUSCHANGES = new Crestron.Logos.SplusObjects.StringOutput( FROM_DEVICE_STATUSCHANGES__AnalogSerialOutput__, this );
    m_StringOutputList.Add( FROM_DEVICE_STATUSCHANGES__AnalogSerialOutput__, FROM_DEVICE_STATUSCHANGES );
    
    MESSAGE_ALERT_TEXT = new Crestron.Logos.SplusObjects.StringOutput( MESSAGE_ALERT_TEXT__AnalogSerialOutput__, this );
    m_StringOutputList.Add( MESSAGE_ALERT_TEXT__AnalogSerialOutput__, MESSAGE_ALERT_TEXT );
    
    MESSAGE_PROMPT_TITLE = new Crestron.Logos.SplusObjects.StringOutput( MESSAGE_PROMPT_TITLE__AnalogSerialOutput__, this );
    m_StringOutputList.Add( MESSAGE_PROMPT_TITLE__AnalogSerialOutput__, MESSAGE_PROMPT_TITLE );
    
    MESSAGE_PROMPT_TEXT = new Crestron.Logos.SplusObjects.StringOutput( MESSAGE_PROMPT_TEXT__AnalogSerialOutput__, this );
    m_StringOutputList.Add( MESSAGE_PROMPT_TEXT__AnalogSerialOutput__, MESSAGE_PROMPT_TEXT );
    
    MESSAGE_PROMPT_OPTION1_TEXT = new Crestron.Logos.SplusObjects.StringOutput( MESSAGE_PROMPT_OPTION1_TEXT__AnalogSerialOutput__, this );
    m_StringOutputList.Add( MESSAGE_PROMPT_OPTION1_TEXT__AnalogSerialOutput__, MESSAGE_PROMPT_OPTION1_TEXT );
    
    MESSAGE_PROMPT_OPTION2_TEXT = new Crestron.Logos.SplusObjects.StringOutput( MESSAGE_PROMPT_OPTION2_TEXT__AnalogSerialOutput__, this );
    m_StringOutputList.Add( MESSAGE_PROMPT_OPTION2_TEXT__AnalogSerialOutput__, MESSAGE_PROMPT_OPTION2_TEXT );
    
    MESSAGE_PROMPT_OPTION3_TEXT = new Crestron.Logos.SplusObjects.StringOutput( MESSAGE_PROMPT_OPTION3_TEXT__AnalogSerialOutput__, this );
    m_StringOutputList.Add( MESSAGE_PROMPT_OPTION3_TEXT__AnalogSerialOutput__, MESSAGE_PROMPT_OPTION3_TEXT );
    
    MESSAGE_PROMPT_OPTION4_TEXT = new Crestron.Logos.SplusObjects.StringOutput( MESSAGE_PROMPT_OPTION4_TEXT__AnalogSerialOutput__, this );
    m_StringOutputList.Add( MESSAGE_PROMPT_OPTION4_TEXT__AnalogSerialOutput__, MESSAGE_PROMPT_OPTION4_TEXT );
    
    MESSAGE_PROMPT_OPTION5_TEXT = new Crestron.Logos.SplusObjects.StringOutput( MESSAGE_PROMPT_OPTION5_TEXT__AnalogSerialOutput__, this );
    m_StringOutputList.Add( MESSAGE_PROMPT_OPTION5_TEXT__AnalogSerialOutput__, MESSAGE_PROMPT_OPTION5_TEXT );
    
    PHONEBOOK_SELECTED_FOLDER_NAME = new Crestron.Logos.SplusObjects.StringOutput( PHONEBOOK_SELECTED_FOLDER_NAME__AnalogSerialOutput__, this );
    m_StringOutputList.Add( PHONEBOOK_SELECTED_FOLDER_NAME__AnalogSerialOutput__, PHONEBOOK_SELECTED_FOLDER_NAME );
    
    CALLHISTORYNAME = new Crestron.Logos.SplusObjects.StringOutput( CALLHISTORYNAME__AnalogSerialOutput__, this );
    m_StringOutputList.Add( CALLHISTORYNAME__AnalogSerialOutput__, CALLHISTORYNAME );
    
    CALLHISTORYNUMBER = new Crestron.Logos.SplusObjects.StringOutput( CALLHISTORYNUMBER__AnalogSerialOutput__, this );
    m_StringOutputList.Add( CALLHISTORYNUMBER__AnalogSerialOutput__, CALLHISTORYNUMBER );
    
    INCOMINGCALLNUMBER = new Crestron.Logos.SplusObjects.StringOutput( INCOMINGCALLNUMBER__AnalogSerialOutput__, this );
    m_StringOutputList.Add( INCOMINGCALLNUMBER__AnalogSerialOutput__, INCOMINGCALLNUMBER );
    
    INPUT_SOURCE_NAME = new InOutArray<StringOutput>( 5, this );
    for( uint i = 0; i < 5; i++ )
    {
        INPUT_SOURCE_NAME[i+1] = new Crestron.Logos.SplusObjects.StringOutput( INPUT_SOURCE_NAME__AnalogSerialOutput__ + i, this );
        m_StringOutputList.Add( INPUT_SOURCE_NAME__AnalogSerialOutput__ + i, INPUT_SOURCE_NAME[i+1] );
    }
    
    PHONEBOOK_NAME = new InOutArray<StringOutput>( 255, this );
    for( uint i = 0; i < 255; i++ )
    {
        PHONEBOOK_NAME[i+1] = new Crestron.Logos.SplusObjects.StringOutput( PHONEBOOK_NAME__AnalogSerialOutput__ + i, this );
        m_StringOutputList.Add( PHONEBOOK_NAME__AnalogSerialOutput__ + i, PHONEBOOK_NAME[i+1] );
    }
    
    PHONEBOOK_PAGES_NAME = new InOutArray<StringOutput>( 20, this );
    for( uint i = 0; i < 20; i++ )
    {
        PHONEBOOK_PAGES_NAME[i+1] = new Crestron.Logos.SplusObjects.StringOutput( PHONEBOOK_PAGES_NAME__AnalogSerialOutput__ + i, this );
        m_StringOutputList.Add( PHONEBOOK_PAGES_NAME__AnalogSerialOutput__ + i, PHONEBOOK_PAGES_NAME[i+1] );
    }
    
    CALLSTATUS_STATUS = new InOutArray<StringOutput>( 3, this );
    for( uint i = 0; i < 3; i++ )
    {
        CALLSTATUS_STATUS[i+1] = new Crestron.Logos.SplusObjects.StringOutput( CALLSTATUS_STATUS__AnalogSerialOutput__ + i, this );
        m_StringOutputList.Add( CALLSTATUS_STATUS__AnalogSerialOutput__ + i, CALLSTATUS_STATUS[i+1] );
    }
    
    CALLSTATUS_CALLTYPE = new InOutArray<StringOutput>( 3, this );
    for( uint i = 0; i < 3; i++ )
    {
        CALLSTATUS_CALLTYPE[i+1] = new Crestron.Logos.SplusObjects.StringOutput( CALLSTATUS_CALLTYPE__AnalogSerialOutput__ + i, this );
        m_StringOutputList.Add( CALLSTATUS_CALLTYPE__AnalogSerialOutput__ + i, CALLSTATUS_CALLTYPE[i+1] );
    }
    
    CALLSTATUS_REMOTESITE = new InOutArray<StringOutput>( 3, this );
    for( uint i = 0; i < 3; i++ )
    {
        CALLSTATUS_REMOTESITE[i+1] = new Crestron.Logos.SplusObjects.StringOutput( CALLSTATUS_REMOTESITE__AnalogSerialOutput__ + i, this );
        m_StringOutputList.Add( CALLSTATUS_REMOTESITE__AnalogSerialOutput__ + i, CALLSTATUS_REMOTESITE[i+1] );
    }
    
    CALLSTATUS_DURATION = new InOutArray<StringOutput>( 3, this );
    for( uint i = 0; i < 3; i++ )
    {
        CALLSTATUS_DURATION[i+1] = new Crestron.Logos.SplusObjects.StringOutput( CALLSTATUS_DURATION__AnalogSerialOutput__ + i, this );
        m_StringOutputList.Add( CALLSTATUS_DURATION__AnalogSerialOutput__ + i, CALLSTATUS_DURATION[i+1] );
    }
    
    CALLSTATUS_TRANSMITCALLRATE = new InOutArray<StringOutput>( 3, this );
    for( uint i = 0; i < 3; i++ )
    {
        CALLSTATUS_TRANSMITCALLRATE[i+1] = new Crestron.Logos.SplusObjects.StringOutput( CALLSTATUS_TRANSMITCALLRATE__AnalogSerialOutput__ + i, this );
        m_StringOutputList.Add( CALLSTATUS_TRANSMITCALLRATE__AnalogSerialOutput__ + i, CALLSTATUS_TRANSMITCALLRATE[i+1] );
    }
    
    CALLSTATUS_RECEIVECALLRATE = new InOutArray<StringOutput>( 3, this );
    for( uint i = 0; i < 3; i++ )
    {
        CALLSTATUS_RECEIVECALLRATE[i+1] = new Crestron.Logos.SplusObjects.StringOutput( CALLSTATUS_RECEIVECALLRATE__AnalogSerialOutput__ + i, this );
        m_StringOutputList.Add( CALLSTATUS_RECEIVECALLRATE__AnalogSerialOutput__ + i, CALLSTATUS_RECEIVECALLRATE[i+1] );
    }
    
    CALLSTATUS_DIRECTION = new InOutArray<StringOutput>( 3, this );
    for( uint i = 0; i < 3; i++ )
    {
        CALLSTATUS_DIRECTION[i+1] = new Crestron.Logos.SplusObjects.StringOutput( CALLSTATUS_DIRECTION__AnalogSerialOutput__ + i, this );
        m_StringOutputList.Add( CALLSTATUS_DIRECTION__AnalogSerialOutput__ + i, CALLSTATUS_DIRECTION[i+1] );
    }
    
    DEVICE_RX__DOLLAR__ = new Crestron.Logos.SplusObjects.BufferInput( DEVICE_RX__DOLLAR____AnalogSerialInput__, 50000, this );
    m_StringInputList.Add( DEVICE_RX__DOLLAR____AnalogSerialInput__, DEVICE_RX__DOLLAR__ );
    
    COMMTIMEOUT_Callback = new WaitFunction( COMMTIMEOUT_CallbackFn );
    RECEIVEPHONEBOOK_Callback = new WaitFunction( RECEIVEPHONEBOOK_CallbackFn );
    
    RESYNCRONIZECALLSTATUSLIST.OnDigitalPush.Add( new InputChangeHandlerWrapper( RESYNCRONIZECALLSTATUSLIST_OnPush_0, false ) );
    SETVOLUMELEVEL.OnAnalogChange.Add( new InputChangeHandlerWrapper( SETVOLUMELEVEL_OnChange_1, false ) );
    DEVICE_RX__DOLLAR__.OnSerialChange.Add( new InputChangeHandlerWrapper( DEVICE_RX__DOLLAR___OnChange_2, true ) );
    ENTRIESPERPAGE.OnAnalogChange.Add( new InputChangeHandlerWrapper( ENTRIESPERPAGE_OnChange_3, false ) );
    PAGESSELECTEDENTRYNUMBER.OnAnalogChange.Add( new InputChangeHandlerWrapper( PAGESSELECTEDENTRYNUMBER_OnChange_4, false ) );
    SELECTEDENTRYNUMBER.OnAnalogChange.Add( new InputChangeHandlerWrapper( SELECTEDENTRYNUMBER_OnChange_5, false ) );
    CLEARSELECTEDENTRYNUMBER.OnDigitalPush.Add( new InputChangeHandlerWrapper( CLEARSELECTEDENTRYNUMBER_OnPush_6, false ) );
    GETLOCALPHONEBOOK.OnDigitalPush.Add( new InputChangeHandlerWrapper( GETLOCALPHONEBOOK_OnPush_7, false ) );
    GETRECENTCALLS.OnDigitalPush.Add( new InputChangeHandlerWrapper( GETRECENTCALLS_OnPush_8, false ) );
    DIALSELECTEDPHONEBOOKENTRYNUMBER.OnDigitalPush.Add( new InputChangeHandlerWrapper( DIALSELECTEDPHONEBOOKENTRYNUMBER_OnPush_9, false ) );
    SEARCHPHONEBOOK.OnDigitalRelease.Add( new InputChangeHandlerWrapper( SEARCHPHONEBOOK_OnRelease_10, false ) );
    PHONEBOOKSEARCHSTRING.OnSerialChange.Add( new InputChangeHandlerWrapper( PHONEBOOKSEARCHSTRING_OnChange_11, true ) );
    TOPLEVEL.OnDigitalPush.Add( new InputChangeHandlerWrapper( TOPLEVEL_OnPush_12, false ) );
    UPONELEVEL.OnDigitalPush.Add( new InputChangeHandlerWrapper( UPONELEVEL_OnPush_13, false ) );
    PAGESPHONEBOOKFIRSTPAGE.OnDigitalPush.Add( new InputChangeHandlerWrapper( PAGESPHONEBOOKFIRSTPAGE_OnPush_14, false ) );
    PAGESPHONEBOOKNEXTPAGE.OnDigitalPush.Add( new InputChangeHandlerWrapper( PAGESPHONEBOOKNEXTPAGE_OnPush_15, false ) );
    PAGESPHONEBOOKPREVIOUSPAGE.OnDigitalPush.Add( new InputChangeHandlerWrapper( PAGESPHONEBOOKPREVIOUSPAGE_OnPush_16, false ) );
    DIALCALL.OnDigitalPush.Add( new InputChangeHandlerWrapper( DIALCALL_OnPush_17, false ) );
    DISCONNECTCALLWITHID.OnAnalogChange.Add( new InputChangeHandlerWrapper( DISCONNECTCALLWITHID_OnChange_18, false ) );
    CALLCONTROLACCEPTINCOMINGCALL.OnDigitalPush.Add( new InputChangeHandlerWrapper( CALLCONTROLACCEPTINCOMINGCALL_OnPush_19, false ) );
    CALLCONTROLREJECTINCOMINGCALL.OnDigitalPush.Add( new InputChangeHandlerWrapper( CALLCONTROLREJECTINCOMINGCALL_OnPush_20, false ) );
    DISCONNECTALLCALLS.OnDigitalPush.Add( new InputChangeHandlerWrapper( DISCONNECTALLCALLS_OnPush_21, false ) );
    DTMFSEND.OnSerialChange.Add( new InputChangeHandlerWrapper( DTMFSEND_OnChange_22, false ) );
    FARENDCONTROLPRESENTATIONSOURCEID.OnAnalogChange.Add( new InputChangeHandlerWrapper( FARENDCONTROLPRESENTATIONSOURCEID_OnChange_23, false ) );
    FARENDCONTROLPRESETACTIVATEID.OnAnalogChange.Add( new InputChangeHandlerWrapper( FARENDCONTROLPRESETACTIVATEID_OnChange_24, false ) );
    FARENDCONTROLCAMERASTOP.OnDigitalPush.Add( new InputChangeHandlerWrapper( FARENDCONTROLCAMERASTOP_OnPush_25, false ) );
    TSTRING_SEND_TEXT.OnSerialChange.Add( new InputChangeHandlerWrapper( TSTRING_SEND_TEXT_OnChange_26, false ) );
    SSTRING_SEND_TEXT.OnSerialChange.Add( new InputChangeHandlerWrapper( SSTRING_SEND_TEXT_OnChange_27, false ) );
    FARENDCONTROLCAMERAPANLEFT.OnDigitalPush.Add( new InputChangeHandlerWrapper( FARENDCONTROLCAMERAPANLEFT_OnPush_28, false ) );
    FARENDCONTROLCAMERAPANRIGHT.OnDigitalPush.Add( new InputChangeHandlerWrapper( FARENDCONTROLCAMERAPANRIGHT_OnPush_29, false ) );
    FARENDCONTROLCAMERATILTUP.OnDigitalPush.Add( new InputChangeHandlerWrapper( FARENDCONTROLCAMERATILTUP_OnPush_30, false ) );
    FARENDCONTROLCAMERATILTDOWN.OnDigitalPush.Add( new InputChangeHandlerWrapper( FARENDCONTROLCAMERATILTDOWN_OnPush_31, false ) );
    FARENDCONTROLCAMERAZOOMIN.OnDigitalPush.Add( new InputChangeHandlerWrapper( FARENDCONTROLCAMERAZOOMIN_OnPush_32, false ) );
    FARENDCONTROLCAMERAZOOMOUT.OnDigitalPush.Add( new InputChangeHandlerWrapper( FARENDCONTROLCAMERAZOOMOUT_OnPush_33, false ) );
    FARENDCONTROLCAMERAFOCUSIN.OnDigitalPush.Add( new InputChangeHandlerWrapper( FARENDCONTROLCAMERAFOCUSIN_OnPush_34, false ) );
    FARENDCONTROLCAMERAFOCUSOUT.OnDigitalPush.Add( new InputChangeHandlerWrapper( FARENDCONTROLCAMERAFOCUSOUT_OnPush_35, false ) );
    DATATOTRACE.OnDigitalPush.Add( new InputChangeHandlerWrapper( DATATOTRACE_OnPush_36, false ) );
    
    _SplusNVRAM.PopulateCustomAttributeList( true );
    
    NVRAM = _SplusNVRAM;
    
}

public override void LogosSimplSharpInitialize()
{
    
    
}

public UserModuleClass_CISCO_C90___C60_CALL_CONTROL_V2_5 ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}


private WaitFunction COMMTIMEOUT_Callback;
private WaitFunction RECEIVEPHONEBOOK_Callback;


const uint DEVICE_RX__DOLLAR____AnalogSerialInput__ = 0;
const uint DEVICE_TX__DOLLAR____AnalogSerialOutput__ = 0;
const uint ENTRIESPERPAGE__AnalogSerialInput__ = 1;
const uint DTMFSENDCALLSLOT__AnalogSerialInput__ = 2;
const uint DTMFSEND__AnalogSerialInput__ = 3;
const uint GETLOCALPHONEBOOK__DigitalInput__ = 0;
const uint ENABLERECENTCALLS__DigitalInput__ = 1;
const uint GETRECENTCALLS__DigitalInput__ = 2;
const uint SELECTEDENTRYNUMBER__AnalogSerialInput__ = 4;
const uint PAGESSELECTEDENTRYNUMBER__AnalogSerialInput__ = 5;
const uint CLEARSELECTEDENTRYNUMBER__DigitalInput__ = 3;
const uint DIALSELECTEDPHONEBOOKENTRYNUMBER__DigitalInput__ = 4;
const uint PHONEBOOKSOURCEID__AnalogSerialInput__ = 6;
const uint TOPLEVEL__DigitalInput__ = 5;
const uint UPONELEVEL__DigitalInput__ = 6;
const uint PAGESPHONEBOOKFIRSTPAGE__DigitalInput__ = 7;
const uint PAGESPHONEBOOKNEXTPAGE__DigitalInput__ = 8;
const uint PAGESPHONEBOOKPREVIOUSPAGE__DigitalInput__ = 9;
const uint SEARCHPHONEBOOK__DigitalInput__ = 10;
const uint PHONEBOOKSEARCHSTRING__AnalogSerialInput__ = 7;
const uint CALLCONTROLACCEPTINCOMINGCALL__DigitalInput__ = 11;
const uint CALLCONTROLREJECTINCOMINGCALL__DigitalInput__ = 12;
const uint DISCONNECTCALLWITHID__AnalogSerialInput__ = 8;
const uint DISCONNECTALLCALLS__DigitalInput__ = 13;
const uint DIALNUMBER__AnalogSerialInput__ = 9;
const uint DIALCALLRATE__AnalogSerialInput__ = 10;
const uint DIALCALLPROTOCOL__AnalogSerialInput__ = 11;
const uint DIALCALL__DigitalInput__ = 14;
const uint FARENDCONTROLCALLSLOT__AnalogSerialInput__ = 12;
const uint FARENDCONTROLCAMERAPANLEFT__DigitalInput__ = 15;
const uint FARENDCONTROLCAMERAPANRIGHT__DigitalInput__ = 16;
const uint FARENDCONTROLCAMERATILTUP__DigitalInput__ = 17;
const uint FARENDCONTROLCAMERATILTDOWN__DigitalInput__ = 18;
const uint FARENDCONTROLCAMERAZOOMIN__DigitalInput__ = 19;
const uint FARENDCONTROLCAMERAZOOMOUT__DigitalInput__ = 20;
const uint FARENDCONTROLCAMERAFOCUSIN__DigitalInput__ = 21;
const uint FARENDCONTROLCAMERAFOCUSOUT__DigitalInput__ = 22;
const uint FARENDCONTROLCAMERASTOP__DigitalInput__ = 23;
const uint FARENDCONTROLPRESENTATIONSOURCEID__AnalogSerialInput__ = 13;
const uint FARENDCONTROLPRESETACTIVATEID__AnalogSerialInput__ = 14;
const uint SETVOLUMELEVEL__AnalogSerialInput__ = 15;
const uint RESYNCRONIZECALLSTATUSLIST__DigitalInput__ = 24;
const uint TSTRING_RECEIVE_TEXT__AnalogSerialOutput__ = 1;
const uint TSTRING_SEND_TEXT__AnalogSerialInput__ = 16;
const uint SSTRING_RECEIVE_TEXT__AnalogSerialOutput__ = 2;
const uint SSTRING_SEND_TEXT__AnalogSerialInput__ = 17;
const uint DATATOTRACE__DigitalInput__ = 25;
const uint SEARCHDELAYSTART__AnalogSerialInput__ = 18;
const uint SEARCHDELAYMOD__AnalogSerialInput__ = 19;
const uint SEARCHDELAY__AnalogSerialInput__ = 20;
const uint DEFAULTCALLTYPESIP__DigitalInput__ = 26;
const uint SELECTEDPHONEBOOK_NAME__AnalogSerialOutput__ = 3;
const uint SELECTEDPHONEBOOK_NUMBER__AnalogSerialOutput__ = 4;
const uint SELECTEDPHONEBOOK_CALLRATE__AnalogSerialOutput__ = 5;
const uint PHONEBOOK_SEARCH_TEXT__AnalogSerialOutput__ = 6;
const uint GETVOLUMELEVEL__AnalogSerialOutput__ = 7;
const uint GETSYSTEMNAME__AnalogSerialOutput__ = 8;
const uint GETSYSTEMNETWORKADDRESS__AnalogSerialOutput__ = 9;
const uint GETGATEKEEPERSTATUS__AnalogSerialOutput__ = 10;
const uint GETGATEKEEPERREGISTRATIONMODE__AnalogSerialOutput__ = 11;
const uint GETGATEKEEPERNETWORKADDRESS__AnalogSerialOutput__ = 12;
const uint GETSYSTEMH323ID__AnalogSerialOutput__ = 13;
const uint GETSYSTEME164ALIAS__AnalogSerialOutput__ = 14;
const uint GETSYSTEMSIPURI__AnalogSerialOutput__ = 15;
const uint GETSYSTEMSOFTWAREVERSION__AnalogSerialOutput__ = 16;
const uint FROM_DEVICE_CONFIGURATIONCHANGES__AnalogSerialOutput__ = 17;
const uint FROM_DEVICE_STATUSCHANGES__AnalogSerialOutput__ = 18;
const uint MESSAGE_ALERT_TEXT__AnalogSerialOutput__ = 19;
const uint MESSAGE_ALERT_ISACTIVE__DigitalOutput__ = 0;
const uint MESSAGE_PROMPT_ISACTIVE__DigitalOutput__ = 1;
const uint MESSAGE_PROMPT_TITLE__AnalogSerialOutput__ = 20;
const uint MESSAGE_PROMPT_TEXT__AnalogSerialOutput__ = 21;
const uint MESSAGE_PROMPT_OPTION1_TEXT__AnalogSerialOutput__ = 22;
const uint MESSAGE_PROMPT_OPTION2_TEXT__AnalogSerialOutput__ = 23;
const uint MESSAGE_PROMPT_OPTION3_TEXT__AnalogSerialOutput__ = 24;
const uint MESSAGE_PROMPT_OPTION4_TEXT__AnalogSerialOutput__ = 25;
const uint MESSAGE_PROMPT_OPTION5_TEXT__AnalogSerialOutput__ = 26;
const uint MESSAGE_PROMPT_OPTION1_ISACTIVE__DigitalOutput__ = 2;
const uint MESSAGE_PROMPT_OPTION2_ISACTIVE__DigitalOutput__ = 3;
const uint MESSAGE_PROMPT_OPTION3_ISACTIVE__DigitalOutput__ = 4;
const uint MESSAGE_PROMPT_OPTION4_ISACTIVE__DigitalOutput__ = 5;
const uint MESSAGE_PROMPT_OPTION5_ISACTIVE__DigitalOutput__ = 6;
const uint MESSAGE_PROMPT_OPTION1_WASSELECTED__DigitalOutput__ = 7;
const uint MESSAGE_PROMPT_OPTION2_WASSELECTED__DigitalOutput__ = 8;
const uint MESSAGE_PROMPT_OPTION3_WASSELECTED__DigitalOutput__ = 9;
const uint MESSAGE_PROMPT_OPTION4_WASSELECTED__DigitalOutput__ = 10;
const uint MESSAGE_PROMPT_OPTION5_WASSELECTED__DigitalOutput__ = 11;
const uint PHONEBOOK_SEARCHRESULT_IS_FROM_SUBFOLDER__DigitalOutput__ = 12;
const uint CURRENTFIRSTPHONEBOOKITEMNUMBER__AnalogSerialOutput__ = 27;
const uint CURRENTLASTPHONEBOOKITEMNUMBER__AnalogSerialOutput__ = 28;
const uint PHONEBOOK_SELECTED_FOLDER_NAME__AnalogSerialOutput__ = 29;
const uint DIRECTORYDOWNLOADBUSY__DigitalOutput__ = 13;
const uint RECENTCALLSDOWNLOADBUSY__DigitalOutput__ = 14;
const uint TOTALROWSFORSEARCH__AnalogSerialOutput__ = 30;
const uint SELECTEDISFOLDER__DigitalOutput__ = 15;
const uint SELECTEDISCONTACT__DigitalOutput__ = 16;
const uint CALLHISTORYNAME__AnalogSerialOutput__ = 31;
const uint CALLHISTORYNUMBER__AnalogSerialOutput__ = 32;
const uint SEARCH_IS_ACTIVE__DigitalOutput__ = 17;
const uint GETREBOOTCODEC_IS_IN_PROGRESS__DigitalOutput__ = 18;
const uint SIGNALINCOMINGCALL__DigitalOutput__ = 19;
const uint DEVICE_ONLINE__DigitalOutput__ = 20;
const uint INCOMINGCALLNUMBER__AnalogSerialOutput__ = 33;
const uint NUMBEROFACTIVECALLS__AnalogSerialOutput__ = 34;
const uint PHONEBOOKPAGESCURRENTPAGE__AnalogSerialOutput__ = 35;
const uint PHONEBOOKPAGESTOTALPAGES__AnalogSerialOutput__ = 36;
const uint DIRECTORYONTOPLEVEL__DigitalOutput__ = 21;
const uint CALLSTATUS_ISCONNECTED__DigitalOutput__ = 22;
const uint CALLSTATUS_SUPPORTSPRESENTATION__DigitalOutput__ = 25;
const uint CALLSTATUS_ISACTIVE__DigitalOutput__ = 28;
const uint INPUT_SOURCE_NAME__AnalogSerialOutput__ = 37;
const uint PHONEBOOK_NAME__AnalogSerialOutput__ = 42;
const uint PHONEBOOK_PAGES_NAME__AnalogSerialOutput__ = 297;
const uint CALLSTATUS_STATUS__AnalogSerialOutput__ = 317;
const uint CALLSTATUS_CALLTYPE__AnalogSerialOutput__ = 320;
const uint CALLSTATUS_REMOTESITE__AnalogSerialOutput__ = 323;
const uint CALLSTATUS_DURATION__AnalogSerialOutput__ = 326;
const uint CALLSTATUS_TRANSMITCALLRATE__AnalogSerialOutput__ = 329;
const uint CALLSTATUS_RECEIVECALLRATE__AnalogSerialOutput__ = 332;
const uint CALLSTATUS_DIRECTION__AnalogSerialOutput__ = 335;
const uint CALLSTATUS_CALLID__AnalogSerialOutput__ = 338;

[SplusStructAttribute(-1, true, false)]
public class SplusNVRAM : SplusStructureBase
{

    public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
    
    
}

SplusNVRAM _SplusNVRAM = null;

public class __CEvent__ : CEvent
{
    public __CEvent__() {}
    public void Close() { base.Close(); }
    public int Reset() { return base.Reset() ? 1 : 0; }
    public int Set() { return base.Set() ? 1 : 0; }
    public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
}
public class __CMutex__ : CMutex
{
    public __CMutex__() {}
    public void Close() { base.Close(); }
    public void ReleaseMutex() { base.ReleaseMutex(); }
    public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
}
 public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}

[SplusStructAttribute(-1, true, false)]
public class PHONEBOOK : SplusStructureBase
{

    [SplusStructAttribute(0, false, false)]
    public ushort  PARENTFOLDER = 0;
    
    [SplusStructAttribute(1, false, false)]
    public ushort  FIRSTCHILD = 0;
    
    [SplusStructAttribute(2, false, false)]
    public CrestronString  ENTRYNAME;
    
    [SplusStructAttribute(3, false, false)]
    public CrestronString  FOLDERID;
    
    [SplusStructAttribute(4, false, false)]
    public ushort  CHILDCOUNT = 0;
    
    
    public PHONEBOOK( SplusObject __caller__, bool bIsStructureVolatile ) : base ( __caller__, bIsStructureVolatile )
    {
        ENTRYNAME  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, Owner );
        FOLDERID  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, Owner );
        
        
    }
    
}
[SplusStructAttribute(-1, true, false)]
public class CONTACTLIST : SplusStructureBase
{

    [SplusStructAttribute(0, false, false)]
    public CrestronString  CONTACTNUMBER;
    
    [SplusStructAttribute(1, false, false)]
    public CrestronString  CALLRATE;
    
    [SplusStructAttribute(2, false, false)]
    public ushort  CALLTYPE = 0;
    
    
    public CONTACTLIST( SplusObject __caller__, bool bIsStructureVolatile ) : base ( __caller__, bIsStructureVolatile )
    {
        CONTACTNUMBER  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, Owner );
        CALLRATE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 20, Owner );
        
        
    }
    
}

}
