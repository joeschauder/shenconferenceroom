using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;

namespace UserModule_NEC_MULTISYNC_BASIC_V1_1
{
    public class UserModuleClass_NEC_MULTISYNC_BASIC_V1_1 : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        
        Crestron.Logos.SplusObjects.DigitalInput POLL_MAIN_INFO;
        Crestron.Logos.SplusObjects.DigitalInput TURN_POWER_ON;
        Crestron.Logos.SplusObjects.DigitalInput TURN_POWER_OFF;
        Crestron.Logos.SplusObjects.DigitalInput TOGGLE_POWER;
        Crestron.Logos.SplusObjects.AnalogInput CHANGE_SOURCE;
        Crestron.Logos.SplusObjects.BufferInput FROMDEVICE;
        Crestron.Logos.SplusObjects.DigitalOutput POWER_IS_ON;
        Crestron.Logos.SplusObjects.DigitalOutput POWER_IS_OFF;
        Crestron.Logos.SplusObjects.AnalogOutput SELECTED_INPUT_ANALOG;
        Crestron.Logos.SplusObjects.StringOutput TODEVICE;
        Crestron.Logos.SplusObjects.StringOutput MESSAGE;
        UShortParameter MONITOR_ID;
        ushort RXOK = 0;
        ushort SENDING = 0;
        ushort RX_MESSAGELENGTH = 0;
        ushort RX_MESSAGETYPE = 0;
        ushort RX_MARKER1 = 0;
        ushort RX_MARKER2 = 0;
        ushort RX_MARKER3 = 0;
        CrestronString RESPONSESTRING;
        CrestronString COMMANDSTRING;
        CrestronString COMMANDTOBESEND;
        CrestronString RX_HEADER;
        CrestronString RX_MESSAGE;
        CrestronString RX_TRASH;
        private void SEND (  SplusExecutionContext __context__ ) 
            { 
            
            __context__.SourceCodeLine = 133;
            COMMANDTOBESEND  .UpdateValue ( Functions.Remove ( "\u000D\u000A\u000A" , COMMANDSTRING )  ) ; 
            __context__.SourceCodeLine = 134;
            COMMANDTOBESEND  .UpdateValue ( Functions.Left ( COMMANDTOBESEND ,  (int) ( (Functions.Length( COMMANDTOBESEND ) - 3) ) )  ) ; 
            __context__.SourceCodeLine = 136;
            TODEVICE  .UpdateValue ( COMMANDTOBESEND  ) ; 
            __context__.SourceCodeLine = 137;
            CreateWait ( "SENDWAIT" , 500 , SENDWAIT_Callback ) ;
            
            }
            
        public void SENDWAIT_CallbackFn( object stateInfo )
        {
        
            try
            {
                Wait __LocalWait__ = (Wait)stateInfo;
                SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
                __LocalWait__.RemoveFromList();
                
            
            __context__.SourceCodeLine = 139;
            /* Trace( "IO Timeout") */ ; 
            __context__.SourceCodeLine = 140;
            SENDING = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 141;
            COMMANDSTRING  .UpdateValue ( ""  ) ; 
            
        
        
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler(); }
            
        }
        
    private CrestronString CALCULATE_BCC (  SplusExecutionContext __context__, CrestronString CMD ) 
        { 
        ushort CMDLENGTH = 0;
        ushort I = 0;
        ushort RESULT = 0;
        
        
        __context__.SourceCodeLine = 148;
        CMDLENGTH = (ushort) ( Functions.Length( CMD ) ) ; 
        __context__.SourceCodeLine = 149;
        RESULT = (ushort) ( Byte( CMD , (int)( 1 ) ) ) ; 
        __context__.SourceCodeLine = 151;
        ushort __FN_FORSTART_VAL__1 = (ushort) ( 2 ) ;
        ushort __FN_FOREND_VAL__1 = (ushort)CMDLENGTH; 
        int __FN_FORSTEP_VAL__1 = (int)1; 
        for ( I  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (I  >= __FN_FORSTART_VAL__1) && (I  <= __FN_FOREND_VAL__1) ) : ( (I  <= __FN_FORSTART_VAL__1) && (I  >= __FN_FOREND_VAL__1) ) ; I  += (ushort)__FN_FORSTEP_VAL__1) 
            { 
            __context__.SourceCodeLine = 153;
            RESULT = (ushort) ( (RESULT ^ Byte( CMD , (int)( I ) )) ) ; 
            __context__.SourceCodeLine = 151;
            } 
        
        __context__.SourceCodeLine = 156;
        return ( Functions.Chr (  (int) ( Functions.Low( (ushort) RESULT ) ) ) ) ; 
        
        }
        
    private void ADDCOMMAND (  SplusExecutionContext __context__, CrestronString CMD ) 
        { 
        CrestronString TEMP;
        CrestronString RESULT;
        TEMP  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 50, this );
        RESULT  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 5, this );
        
        
        __context__.SourceCodeLine = 163;
        MakeString ( TEMP , "\u0001\u0030{0}{1}", Functions.Chr (  (int) ( Functions.Low( (ushort) (MONITOR_ID  .Value + 64) ) ) ) , CMD ) ; 
        __context__.SourceCodeLine = 165;
        RESULT  .UpdateValue ( CALCULATE_BCC (  __context__ , Functions.Right( TEMP , (int)( (Functions.Length( TEMP ) - 1) ) ))  ) ; 
        __context__.SourceCodeLine = 166;
        TEMP  .UpdateValue ( TEMP + RESULT + "\u000D\u000D\u000A\u000A"  ) ; 
        __context__.SourceCodeLine = 167;
        COMMANDSTRING  .UpdateValue ( COMMANDSTRING + TEMP  ) ; 
        __context__.SourceCodeLine = 169;
        if ( Functions.TestForTrue  ( ( Functions.Not( SENDING ))  ) ) 
            { 
            __context__.SourceCodeLine = 171;
            SENDING = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 172;
            SEND (  __context__  ) ; 
            } 
        
        
        }
        
    private void OUTPUTSOURCERESULT (  SplusExecutionContext __context__, ushort I ) 
        { 
        
        __context__.SourceCodeLine = 178;
        
            {
            int __SPLS_TMPVAR__SWTCH_1__ = ((int)I);
            
                { 
                if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 0) ) ) ) 
                    { 
                    __context__.SourceCodeLine = 182;
                    SELECTED_INPUT_ANALOG  .Value = (ushort) ( 0 ) ; 
                    } 
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 1) ) ) ) 
                    { 
                    __context__.SourceCodeLine = 186;
                    SELECTED_INPUT_ANALOG  .Value = (ushort) ( 1 ) ; 
                    } 
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 2) ) ) ) 
                    { 
                    __context__.SourceCodeLine = 190;
                    SELECTED_INPUT_ANALOG  .Value = (ushort) ( 2 ) ; 
                    } 
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 3) ) ) ) 
                    { 
                    __context__.SourceCodeLine = 194;
                    SELECTED_INPUT_ANALOG  .Value = (ushort) ( 3 ) ; 
                    } 
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 5) ) ) ) 
                    { 
                    __context__.SourceCodeLine = 198;
                    SELECTED_INPUT_ANALOG  .Value = (ushort) ( 4 ) ; 
                    } 
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 6) ) ) ) 
                    { 
                    __context__.SourceCodeLine = 202;
                    SELECTED_INPUT_ANALOG  .Value = (ushort) ( 5 ) ; 
                    } 
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 7) ) ) ) 
                    { 
                    __context__.SourceCodeLine = 206;
                    SELECTED_INPUT_ANALOG  .Value = (ushort) ( 6 ) ; 
                    } 
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 12) ) ) ) 
                    { 
                    __context__.SourceCodeLine = 210;
                    SELECTED_INPUT_ANALOG  .Value = (ushort) ( 7 ) ; 
                    } 
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 4) ) ) ) 
                    { 
                    __context__.SourceCodeLine = 214;
                    SELECTED_INPUT_ANALOG  .Value = (ushort) ( 8 ) ; 
                    } 
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 10) ) ) ) 
                    { 
                    __context__.SourceCodeLine = 218;
                    SELECTED_INPUT_ANALOG  .Value = (ushort) ( 9 ) ; 
                    } 
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 17) ) ) ) 
                    { 
                    __context__.SourceCodeLine = 222;
                    SELECTED_INPUT_ANALOG  .Value = (ushort) ( 10 ) ; 
                    } 
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 18) ) ) ) 
                    { 
                    __context__.SourceCodeLine = 226;
                    SELECTED_INPUT_ANALOG  .Value = (ushort) ( 11 ) ; 
                    } 
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 19) ) ) ) 
                    { 
                    __context__.SourceCodeLine = 230;
                    SELECTED_INPUT_ANALOG  .Value = (ushort) ( 12 ) ; 
                    } 
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 15) ) ) ) 
                    { 
                    __context__.SourceCodeLine = 234;
                    SELECTED_INPUT_ANALOG  .Value = (ushort) ( 13 ) ; 
                    } 
                
                } 
                
            }
            
        
        
        }
        
    private void PROCESSRESPONSE (  SplusExecutionContext __context__, CrestronString CMD ) 
        { 
        ushort RESULT = 0;
        
        
        __context__.SourceCodeLine = 244;
        RX_MESSAGETYPE = (ushort) ( Byte( CMD , (int)( 5 ) ) ) ; 
        __context__.SourceCodeLine = 246;
        RX_MESSAGELENGTH = (ushort) ( ((16 * Functions.HextoI( Functions.Mid( CMD , (int)( 6 ) , (int)( 1 ) ) )) + Functions.HextoI( Functions.Mid( CMD , (int)( 7 ) , (int)( 1 ) ) )) ) ; 
        __context__.SourceCodeLine = 247;
        MakeString ( MESSAGE , "RX Found Message Length: {0:d}", (ushort)RX_MESSAGELENGTH) ; 
        __context__.SourceCodeLine = 248;
        
            {
            int __SPLS_TMPVAR__SWTCH_2__ = ((int)RX_MESSAGETYPE);
            
                { 
                if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_2__ == ( 66) ) ) ) 
                    { 
                    __context__.SourceCodeLine = 252;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.Mid( CMD , (int)( 9 ) , (int)( 2 ) ) == "02"))  ) ) 
                        { 
                        __context__.SourceCodeLine = 254;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.Mid( CMD , (int)( 11 ) , (int)( 2 ) ) == "00"))  ) ) 
                            { 
                            __context__.SourceCodeLine = 256;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.Mid( CMD , (int)( 13 ) , (int)( 4 ) ) == "D600"))  ) ) 
                                { 
                                __context__.SourceCodeLine = 258;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.Mid( CMD , (int)( 24 ) , (int)( 1 ) ) == "1"))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 260;
                                    Functions.Pulse ( 10, POWER_IS_ON ) ; 
                                    } 
                                
                                else 
                                    { 
                                    __context__.SourceCodeLine = 264;
                                    Functions.Pulse ( 10, POWER_IS_OFF ) ; 
                                    } 
                                
                                } 
                            
                            } 
                        
                        else 
                            { 
                            __context__.SourceCodeLine = 270;
                            /* Trace( "Error received while executing command") */ ; 
                            } 
                        
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 273;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.Mid( CMD , (int)( 9 ) , (int)( 2 ) ) == "00"))  ) ) 
                            { 
                            __context__.SourceCodeLine = 275;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.Mid( CMD , (int)( 11 ) , (int)( 6 ) ) == "C203D6"))  ) ) 
                                { 
                                __context__.SourceCodeLine = 277;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.Mid( CMD , (int)( 20 ) , (int)( 1 ) ) == "1"))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 279;
                                    Functions.Pulse ( 10, POWER_IS_ON ) ; 
                                    } 
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 281;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.Mid( CMD , (int)( 20 ) , (int)( 1 ) ) == "4"))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 283;
                                        Functions.Pulse ( 10, POWER_IS_OFF ) ; 
                                        } 
                                    
                                    }
                                
                                } 
                            
                            } 
                        
                        else 
                            { 
                            __context__.SourceCodeLine = 289;
                            /* Trace( "Error received while executing command") */ ; 
                            } 
                        
                        }
                    
                    } 
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_2__ == ( 68) ) ) ) 
                    { 
                    __context__.SourceCodeLine = 294;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.Mid( CMD , (int)( 9 ) , (int)( 2 ) ) == "00"))  ) ) 
                        { 
                        __context__.SourceCodeLine = 296;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.Mid( CMD , (int)( 11 ) , (int)( 4 ) ) == "02BE"))  ) ) 
                            { 
                            __context__.SourceCodeLine = 297;
                            ; 
                            __context__.SourceCodeLine = 298;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.Mid( CMD , (int)( 18 ) , (int)( 1 ) ) == "1"))  ) ) 
                                { 
                                __context__.SourceCodeLine = 300;
                                Functions.Pulse ( 10, POWER_IS_ON ) ; 
                                } 
                            
                            else 
                                {
                                __context__.SourceCodeLine = 302;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.Mid( CMD , (int)( 18 ) , (int)( 1 ) ) == "2"))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 304;
                                    Functions.Pulse ( 10, POWER_IS_OFF ) ; 
                                    } 
                                
                                }
                            
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 307;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.Mid( CMD , (int)( 11 ) , (int)( 4 ) ) == "0060"))  ) ) 
                                { 
                                __context__.SourceCodeLine = 309;
                                OUTPUTSOURCERESULT (  __context__ , (ushort)( Functions.HextoI( Functions.Mid( CMD , (int)( 23 ) , (int)( 2 ) ) ) )) ; 
                                } 
                            
                            }
                        
                        } 
                    
                    else 
                        { 
                        __context__.SourceCodeLine = 314;
                        /* Trace( "Error received while retrieving value") */ ; 
                        } 
                    
                    } 
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_2__ == ( 70) ) ) ) 
                    { 
                    __context__.SourceCodeLine = 319;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.Mid( CMD , (int)( 9 ) , (int)( 2 ) ) == "00"))  ) ) 
                        { 
                        __context__.SourceCodeLine = 321;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.Mid( CMD , (int)( 11 ) , (int)( 4 ) ) == "0060"))  ) ) 
                            { 
                            __context__.SourceCodeLine = 323;
                            RESULT = (ushort) ( Functions.HextoI( Functions.Mid( CMD , (int)( 23 ) , (int)( 2 ) ) ) ) ; 
                            __context__.SourceCodeLine = 324;
                            OUTPUTSOURCERESULT (  __context__ , (ushort)( RESULT )) ; 
                            } 
                        
                        } 
                    
                    else 
                        { 
                        __context__.SourceCodeLine = 329;
                        /* Trace( "Error received while Setting value") */ ; 
                        } 
                    
                    } 
                
                } 
                
            }
            
        
        
        }
        
    object POLL_MAIN_INFO_OnPush_0 ( Object __EventInfo__ )
    
        { 
        Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
        try
        {
            SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
            
            __context__.SourceCodeLine = 342;
            ADDCOMMAND (  __context__ , "\u0030\u0041\u0030\u0036\u0002\u0030\u0031\u0044\u0036\u0003") ; 
            __context__.SourceCodeLine = 343;
            ADDCOMMAND (  __context__ , "\u0030\u0043\u0030\u0036\u0002\u0030\u0030\u0036\u0032\u0003") ; 
            __context__.SourceCodeLine = 344;
            ADDCOMMAND (  __context__ , "\u0030\u0043\u0030\u0036\u0002\u0030\u0030\u0038\u0044\u0003") ; 
            __context__.SourceCodeLine = 345;
            ADDCOMMAND (  __context__ , "\u0030\u0043\u0030\u0036\u0002\u0030\u0030\u0036\u0030\u0003") ; 
            __context__.SourceCodeLine = 346;
            ADDCOMMAND (  __context__ , "\u0030\u0043\u0030\u0036\u0002\u0031\u0030\u0038\u0034\u0003") ; 
            __context__.SourceCodeLine = 347;
            ADDCOMMAND (  __context__ , "\u0030\u0043\u0030\u0036\u0002\u0031\u0030\u0041\u0031\u0003") ; 
            __context__.SourceCodeLine = 348;
            ADDCOMMAND (  __context__ , "\u0030\u0043\u0030\u0036\u0002\u0030\u0030\u0046\u0042\u0003") ; 
            
            
        }
        catch(Exception e) { ObjectCatchHandler(e); }
        finally { ObjectFinallyHandler( __SignalEventArg__ ); }
        return this;
        
    }
    
object TURN_POWER_ON_OnPush_1 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 353;
        ADDCOMMAND (  __context__ , "\u0030\u0041\u0030\u0043\u0002\u0043\u0032\u0030\u0033\u0044\u0036\u0030\u0030\u0030\u0031\u0003") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object TURN_POWER_OFF_OnPush_2 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 358;
        ADDCOMMAND (  __context__ , "\u0030\u0041\u0030\u0043\u0002\u0043\u0032\u0030\u0033\u0044\u0036\u0030\u0030\u0030\u0034\u0003") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object TOGGLE_POWER_OnPush_3 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 364;
        ADDCOMMAND (  __context__ , "\u0030\u0041\u0030\u0043\u0002\u0043\u0032\u0031\u0030\u0030\u0030\u0030\u0033\u0030\u0033\u0003") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object CHANGE_SOURCE_OnChange_4 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString TEMP;
        CrestronString RESULT;
        TEMP  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 50, this );
        RESULT  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 5, this );
        
        
        __context__.SourceCodeLine = 371;
        
            {
            int __SPLS_TMPVAR__SWTCH_3__ = ((int)CHANGE_SOURCE  .UshortValue);
            
                { 
                if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_3__ == ( 0) ) ) ) 
                    { 
                    __context__.SourceCodeLine = 375;
                    ADDCOMMAND (  __context__ , "\u0030\u0045\u0030\u0041\u0002\u0030\u0030\u0036\u0030\u0030\u0030\u0030\u0030\u0003") ; 
                    } 
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_3__ == ( 1) ) ) ) 
                    { 
                    __context__.SourceCodeLine = 379;
                    ADDCOMMAND (  __context__ , "\u0030\u0045\u0030\u0041\u0002\u0030\u0030\u0036\u0030\u0030\u0030\u0030\u0031\u0003") ; 
                    } 
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_3__ == ( 2) ) ) ) 
                    { 
                    __context__.SourceCodeLine = 383;
                    ADDCOMMAND (  __context__ , "\u0030\u0045\u0030\u0041\u0002\u0030\u0030\u0036\u0030\u0030\u0030\u0030\u0032\u0003") ; 
                    } 
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_3__ == ( 3) ) ) ) 
                    { 
                    __context__.SourceCodeLine = 387;
                    ADDCOMMAND (  __context__ , "\u0030\u0045\u0030\u0041\u0002\u0030\u0030\u0036\u0030\u0030\u0030\u0030\u0033\u0003") ; 
                    } 
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_3__ == ( 4) ) ) ) 
                    { 
                    __context__.SourceCodeLine = 391;
                    ADDCOMMAND (  __context__ , "\u0030\u0045\u0030\u0041\u0002\u0030\u0030\u0036\u0030\u0030\u0030\u0030\u0035\u0003") ; 
                    } 
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_3__ == ( 5) ) ) ) 
                    { 
                    __context__.SourceCodeLine = 395;
                    ADDCOMMAND (  __context__ , "\u0030\u0045\u0030\u0041\u0002\u0030\u0030\u0036\u0030\u0030\u0030\u0030\u0036\u0003") ; 
                    } 
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_3__ == ( 6) ) ) ) 
                    { 
                    __context__.SourceCodeLine = 399;
                    ADDCOMMAND (  __context__ , "\u0030\u0045\u0030\u0041\u0002\u0030\u0030\u0036\u0030\u0030\u0030\u0030\u0037\u0003") ; 
                    } 
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_3__ == ( 7) ) ) ) 
                    { 
                    __context__.SourceCodeLine = 403;
                    ADDCOMMAND (  __context__ , "\u0030\u0045\u0030\u0041\u0002\u0030\u0030\u0036\u0030\u0030\u0030\u0030\u0043\u0003") ; 
                    } 
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_3__ == ( 8) ) ) ) 
                    { 
                    __context__.SourceCodeLine = 407;
                    ADDCOMMAND (  __context__ , "\u0030\u0045\u0030\u0041\u0002\u0030\u0030\u0036\u0030\u0030\u0030\u0030\u0034\u0003") ; 
                    } 
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_3__ == ( 9) ) ) ) 
                    { 
                    __context__.SourceCodeLine = 411;
                    ADDCOMMAND (  __context__ , "\u0030\u0045\u0030\u0041\u0002\u0030\u0030\u0036\u0030\u0030\u0030\u0030\u0041\u0003") ; 
                    } 
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_3__ == ( 10) ) ) ) 
                    { 
                    __context__.SourceCodeLine = 415;
                    ADDCOMMAND (  __context__ , "\u0030\u0045\u0030\u0041\u0002\u0030\u0030\u0036\u0030\u0030\u0030\u0031\u0031\u0003") ; 
                    } 
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_3__ == ( 11) ) ) ) 
                    { 
                    __context__.SourceCodeLine = 419;
                    ADDCOMMAND (  __context__ , "\u0030\u0045\u0030\u0041\u0002\u0030\u0030\u0036\u0030\u0030\u0030\u0031\u0032\u0003") ; 
                    } 
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_3__ == ( 12) ) ) ) 
                    { 
                    __context__.SourceCodeLine = 423;
                    ADDCOMMAND (  __context__ , "\u0030\u0045\u0030\u0041\u0002\u0030\u0030\u0036\u0030\u0030\u0030\u0031\u0033\u0003") ; 
                    } 
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_3__ == ( 13) ) ) ) 
                    { 
                    __context__.SourceCodeLine = 427;
                    ADDCOMMAND (  __context__ , "\u0030\u0045\u0030\u0041\u0002\u0030\u0030\u0036\u0030\u0030\u0030\u0030\u0046\u0003") ; 
                    } 
                
                } 
                
            }
            
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object FROMDEVICE_OnChange_5 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 434;
        RESPONSESTRING  .UpdateValue ( RESPONSESTRING + FROMDEVICE  ) ; 
        __context__.SourceCodeLine = 435;
        Functions.ClearBuffer ( FROMDEVICE ) ; 
        __context__.SourceCodeLine = 437;
        if ( Functions.TestForTrue  ( ( RXOK)  ) ) 
            { 
            __context__.SourceCodeLine = 439;
            RXOK = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 440;
            while ( Functions.TestForTrue  ( ( Functions.Find( "\u000D" , RESPONSESTRING ))  ) ) 
                { 
                __context__.SourceCodeLine = 442;
                RX_MARKER3 = (ushort) ( Functions.Find( "\u000D" , RESPONSESTRING ) ) ; 
                __context__.SourceCodeLine = 443;
                if ( Functions.TestForTrue  ( ( RX_MARKER3)  ) ) 
                    { 
                    __context__.SourceCodeLine = 445;
                    RX_MESSAGE  .UpdateValue ( Functions.Remove ( RX_MARKER3, RESPONSESTRING )  ) ; 
                    __context__.SourceCodeLine = 446;
                    CancelWait ( "SENDWAIT" ) ; 
                    __context__.SourceCodeLine = 447;
                    PROCESSRESPONSE (  __context__ , RX_MESSAGE) ; 
                    __context__.SourceCodeLine = 448;
                    if ( Functions.TestForTrue  ( ( Functions.Length( COMMANDSTRING ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 450;
                        SEND (  __context__  ) ; 
                        } 
                    
                    else 
                        { 
                        __context__.SourceCodeLine = 454;
                        SENDING = (ushort) ( 0 ) ; 
                        } 
                    
                    } 
                
                __context__.SourceCodeLine = 440;
                } 
            
            __context__.SourceCodeLine = 458;
            RXOK = (ushort) ( 1 ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public override object FunctionMain (  object __obj__ ) 
    { 
    try
    {
        SplusExecutionContext __context__ = SplusFunctionMainStartCode();
        
        __context__.SourceCodeLine = 471;
        RXOK = (ushort) ( 1 ) ; 
        __context__.SourceCodeLine = 472;
        RX_MESSAGELENGTH = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 473;
        RX_MARKER1 = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 474;
        RX_MARKER2 = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 475;
        SENDING = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 476;
        RESPONSESTRING  .UpdateValue ( ""  ) ; 
        __context__.SourceCodeLine = 477;
        COMMANDSTRING  .UpdateValue ( ""  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    return __obj__;
    }
    

public override void LogosSplusInitialize()
{
    _SplusNVRAM = new SplusNVRAM( this );
    RESPONSESTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 3000, this );
    COMMANDSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 3000, this );
    COMMANDTOBESEND  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 50, this );
    RX_HEADER  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 7, this );
    RX_MESSAGE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 50, this );
    RX_TRASH  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
    
    POLL_MAIN_INFO = new Crestron.Logos.SplusObjects.DigitalInput( POLL_MAIN_INFO__DigitalInput__, this );
    m_DigitalInputList.Add( POLL_MAIN_INFO__DigitalInput__, POLL_MAIN_INFO );
    
    TURN_POWER_ON = new Crestron.Logos.SplusObjects.DigitalInput( TURN_POWER_ON__DigitalInput__, this );
    m_DigitalInputList.Add( TURN_POWER_ON__DigitalInput__, TURN_POWER_ON );
    
    TURN_POWER_OFF = new Crestron.Logos.SplusObjects.DigitalInput( TURN_POWER_OFF__DigitalInput__, this );
    m_DigitalInputList.Add( TURN_POWER_OFF__DigitalInput__, TURN_POWER_OFF );
    
    TOGGLE_POWER = new Crestron.Logos.SplusObjects.DigitalInput( TOGGLE_POWER__DigitalInput__, this );
    m_DigitalInputList.Add( TOGGLE_POWER__DigitalInput__, TOGGLE_POWER );
    
    POWER_IS_ON = new Crestron.Logos.SplusObjects.DigitalOutput( POWER_IS_ON__DigitalOutput__, this );
    m_DigitalOutputList.Add( POWER_IS_ON__DigitalOutput__, POWER_IS_ON );
    
    POWER_IS_OFF = new Crestron.Logos.SplusObjects.DigitalOutput( POWER_IS_OFF__DigitalOutput__, this );
    m_DigitalOutputList.Add( POWER_IS_OFF__DigitalOutput__, POWER_IS_OFF );
    
    CHANGE_SOURCE = new Crestron.Logos.SplusObjects.AnalogInput( CHANGE_SOURCE__AnalogSerialInput__, this );
    m_AnalogInputList.Add( CHANGE_SOURCE__AnalogSerialInput__, CHANGE_SOURCE );
    
    SELECTED_INPUT_ANALOG = new Crestron.Logos.SplusObjects.AnalogOutput( SELECTED_INPUT_ANALOG__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( SELECTED_INPUT_ANALOG__AnalogSerialOutput__, SELECTED_INPUT_ANALOG );
    
    TODEVICE = new Crestron.Logos.SplusObjects.StringOutput( TODEVICE__AnalogSerialOutput__, this );
    m_StringOutputList.Add( TODEVICE__AnalogSerialOutput__, TODEVICE );
    
    MESSAGE = new Crestron.Logos.SplusObjects.StringOutput( MESSAGE__AnalogSerialOutput__, this );
    m_StringOutputList.Add( MESSAGE__AnalogSerialOutput__, MESSAGE );
    
    FROMDEVICE = new Crestron.Logos.SplusObjects.BufferInput( FROMDEVICE__AnalogSerialInput__, 500, this );
    m_StringInputList.Add( FROMDEVICE__AnalogSerialInput__, FROMDEVICE );
    
    MONITOR_ID = new UShortParameter( MONITOR_ID__Parameter__, this );
    m_ParameterList.Add( MONITOR_ID__Parameter__, MONITOR_ID );
    
    SENDWAIT_Callback = new WaitFunction( SENDWAIT_CallbackFn );
    
    POLL_MAIN_INFO.OnDigitalPush.Add( new InputChangeHandlerWrapper( POLL_MAIN_INFO_OnPush_0, false ) );
    TURN_POWER_ON.OnDigitalPush.Add( new InputChangeHandlerWrapper( TURN_POWER_ON_OnPush_1, false ) );
    TURN_POWER_OFF.OnDigitalPush.Add( new InputChangeHandlerWrapper( TURN_POWER_OFF_OnPush_2, false ) );
    TOGGLE_POWER.OnDigitalPush.Add( new InputChangeHandlerWrapper( TOGGLE_POWER_OnPush_3, false ) );
    CHANGE_SOURCE.OnAnalogChange.Add( new InputChangeHandlerWrapper( CHANGE_SOURCE_OnChange_4, true ) );
    FROMDEVICE.OnSerialChange.Add( new InputChangeHandlerWrapper( FROMDEVICE_OnChange_5, true ) );
    
    _SplusNVRAM.PopulateCustomAttributeList( true );
    
    NVRAM = _SplusNVRAM;
    
}

public override void LogosSimplSharpInitialize()
{
    
    
}

public UserModuleClass_NEC_MULTISYNC_BASIC_V1_1 ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}


private WaitFunction SENDWAIT_Callback;


const uint POLL_MAIN_INFO__DigitalInput__ = 0;
const uint TURN_POWER_ON__DigitalInput__ = 1;
const uint TURN_POWER_OFF__DigitalInput__ = 2;
const uint TOGGLE_POWER__DigitalInput__ = 3;
const uint CHANGE_SOURCE__AnalogSerialInput__ = 0;
const uint FROMDEVICE__AnalogSerialInput__ = 1;
const uint POWER_IS_ON__DigitalOutput__ = 0;
const uint POWER_IS_OFF__DigitalOutput__ = 1;
const uint SELECTED_INPUT_ANALOG__AnalogSerialOutput__ = 0;
const uint TODEVICE__AnalogSerialOutput__ = 1;
const uint MESSAGE__AnalogSerialOutput__ = 2;
const uint MONITOR_ID__Parameter__ = 10;

[SplusStructAttribute(-1, true, false)]
public class SplusNVRAM : SplusStructureBase
{

    public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
    
    
}

SplusNVRAM _SplusNVRAM = null;

public class __CEvent__ : CEvent
{
    public __CEvent__() {}
    public void Close() { base.Close(); }
    public int Reset() { return base.Reset() ? 1 : 0; }
    public int Set() { return base.Set() ? 1 : 0; }
    public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
}
public class __CMutex__ : CMutex
{
    public __CMutex__() {}
    public void Close() { base.Close(); }
    public void ReleaseMutex() { base.ReleaseMutex(); }
    public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
}
 public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}


}
